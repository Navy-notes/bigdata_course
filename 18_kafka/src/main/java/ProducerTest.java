import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.util.Properties;

public class ProducerTest {
    public static void main(String[] args) throws Exception {

        Properties props = new Properties();
        // 这里配置几台broker都可以，Kafka Controller会从zk去拉取元数据进行缓存，其他broker节点会自动从controller同步元数据信息
        props.put("bootstrap.servers", "hadoop1:9092,hadoop2:9092,hadoop3:9092");
        // 这个就是负责把发送的key从字符串序列化为字节数组
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        // 把发送的Message从字符串序列化为字节数组
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");



        // 生产者参数调优-提高吞吐量
        props.put("buffer.memory", 33554432);
        props.put("compression.type", "lz4");

        props.put("batch.size", 32768);   // 攒批的大小
        props.put("linger.ms", 100);      // 批处理的最大等待时间

        // 生产者参数调优-自定义分区器
        props.put("partitioner.class", "com.nx.HotDataPartitioner");

        // 重试机制参数
        props.put("retries", 10);               // 重试次数
        props.put("retry.backoff.ms", 300);     // 重试时间间隔

        // 由于重试机制导致的消息乱序可以通过设置参数:
        props.put("max.in.flight.requests.per.connection", 1);

        // 设置ack参数，默认是1
        props.put("request.required.acks", -1);





        KafkaProducer<String, String> producer = new KafkaProducer<String, String>(props);
        ProducerRecord<String, String> record = new ProducerRecord<>(
                "test8", "message1");

        /**
         *
         * 如果发送消息，消息不指定key，那么我们发送的这些消息，会被轮询的发送到不同的分区。
         * 如果指定了key。发送消息的时候，客户端会根据这个key计算出来一个hash值，
         * 根据这个hash值会把消息发送到对应的分区里面。
         */

        //kafka发送数据有两种方式：
        //1:异步的方式。
        // 这是异步发送的模式（可带回调函数也可不带回调函数）
        producer.send(record, new Callback() {
            @Override
            public void onCompletion(RecordMetadata metadata, Exception exception) {
                if (exception == null) {
                    // 消息发送成功
                    System.out.println("消息发送成功");
                } else {
                    // 消息发送失败，需要重新发送
                }
            }

        });

        Thread.sleep(10 * 1000);

        //第二种方式：这是同步发送的模式
//		producer.send(record).get();
        // 你要一直等待人家后续一系列的步骤都做完，发送消息之后
        // 有了消息的回应返回给你，你这个方法才会退出来

        producer.close();
    }
}
