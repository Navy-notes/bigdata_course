import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.util.Arrays;
import java.util.Properties;

public class ConsumerTest {
    public static void main(String[] args) {

        String topicName = "test8";
        String groupId = "testtest";
        Properties props = new Properties();
        props.put("bootstrap.servers", "hadoop1:9092");
        // 指定消费组的id（subscribe方式必须设置消费组id，assign方式则可设置可不设置消费组id）
        props.put("group.id", groupId);
        // 反序列化
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");




        /**
         * 消费者参数调优
         */
        // 获取一条消息最大的字节数，一般建议设置大一些, 默认是1M
        props.put("fetch.max.bytes", 10 * 1024 * 1024);
        // 一次poll返回消息的最大条数，默认是500条
        props.put("max.poll.records", 1000);
        // consumer跟broker的socket连接如果空闲超过了一定的时间，此时就会自动回收连接，但是下次消费就要重新建立socket连接，这个建议设置为-1，不要去回收，因为在实时场景中使用网络会很频繁，没必要回收
        props.put("connection.max.idle.ms", -1);

        /**
         * offset相关设置
         */
        // 开启自动提交偏移量offset，默认是true
        props.put("enable.auto.commit", true);
        // 每隔多久提交一次偏移量，默认值5000毫秒
        props.put("auto.commit.interval.ms", 5 * 1000);
        // 设置消费策略，生产一般用latest
        props.put("auto.offset.reset", "latest");

        /**
         * consumer线程心跳异常感知
         */
        // 消费者线程每隔3秒给coordinator发送一次心跳信息
        props.put("heartbeat.interval.ms", 3000);
        // coordinator多长时间感知不到一个consumer的心跳就认为它故障了，默认是10秒
        props.put("session.timeout.ms",10 * 1000);
        // 如果某个消费线程在两次poll操作之间，超过了设置的时间，会被踢出消费组，然后组内进行重平衡
        props.put("max.poll.interval.ms",5 * 1000);


        /**
         * 分区策略、重平衡策略的设置
         */
        props.put(ConsumerConfig.PARTITION_ASSIGNMENT_STRATEGY_CONFIG, "org.apache.kafka.clients.consumer.RoundRobinAssignor");
//        props.put("partition.assignment.strategy", "org.apache.kafka.clients.consumer.RangeAssignor");    // 默认使用range分区策略
//        props.put("partition.assignment.strategy", "org.apache.kafka.clients.consumer.StickyAssignor");     // 从kafka_0.11.x开始才有

        KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(props);


        // 指定要消费的主题，一个消费者可以消费多个主题
        //   subscribe: 这种方式使用kafka自己内部的分区和重平衡策略进行消费（range、round-robin、sticky）
        //   assign: 用户手动设置消费topic的指定的某个分区数据，这种方式注册的消费者不会进行重平衡。
        consumer.subscribe(Arrays.asList(topicName));


        try {
            while (true) {
                /**
                 * 这里的超时时间，可以理解成batch攒批的超时时间，同时也是一种重试机制。
                 * 就是说，当我到了这个超时时间就必须要返回这次poll的结果了，不管你能消费到多少数据，可能是0条也可能是max.poll.records条。
                 * 如果这次没有消费到数据，那么下次继续poll就行了，持续poll不到数据的话其实就是消费线程发生故障了，这自然会通过心跳机制感知到异常，然后就会进行组内消费线程和分区的重分配。
                 */
                // 去服务端消费(拉取)数据
                ConsumerRecords<String, String> records = consumer.poll(1000);  // 超时时间
                // 处理数据
                for (ConsumerRecord<String, String> record : records) {
                    System.out.println(record.offset() + ", " + record.key() + ", " + record.value());
                }
            }
        } catch (Exception e) {

        }


    }

}
