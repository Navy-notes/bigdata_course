import org.apache.spark.{SparkConf, SparkContext}

/**
  * @author Yuliang.Lee
  * @date 2021/10/8 22:21
  * @version 1.0
  */
object WebPV {
  def main(args: Array[String]): Unit = {
    // 创建SparkContext对象
    val conf = new SparkConf().setAppName("WebPv").setMaster("local")
    val sc = new SparkContext(conf)

    // 读取数据
    val rdd1 = sc.textFile("F:\\IDEAProjects\\bigdata_course\\22_spark\\src\\main\\document\\测试数据\\tomcat_access.log").map(line => {
      val resource = line.substring(line.indexOf("\"") + 1, line.lastIndexOf("\""))
      val url = resource.substring(resource.indexOf(" ") + 1, resource.lastIndexOf(" "))
      val jspName = url.substring(url.lastIndexOf("/") + 1)
      (jspName, 1)
    })

    // 对每个jspName访问量求和
    val rdd2 = rdd1.reduceByKey(_+_)

    // 排序：根据元组的第二个元素进行排序即访问量，默认true为升序排序
    val rdd3 = rdd2.sortBy(_._2, false)

    // 取出访问量最高的两个网页
    rdd3.take(2).foreach(println)

    // 释放资源
    sc.stop()
  }

}
