import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}

/**
  * @author Yuliang.Lee
  * @date 2021/10/11 15:42
  * @version 1.0
  */
object SortTask {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.WARN); // 屏蔽掉多余的日志

    val conf = new SparkConf().setAppName("sort_task").setMaster("local")
    val sc = new SparkContext(conf)

    val input = sc.textFile("22_spark/src/main/document/测试数据/task2.txt")

    // 注：sortByKey() 等价于 sortBy(x => x._1)，默认ascending = true为升序排序。
    //     sortBy和sortByKey会触发shuffle操作，分区数如果不设置默认跟随上游。
    //     sort的流程会先根据分区数执行rangePartition分发策略，然后再在各分区内保证有序，最终实现全局有序。
    // reduceByKey必须要传func作为参数，groupByKey可以不传func；
    // reduceByKey性能要优于groupByKey，相当于会在Map阶段做combine，减少磁盘IO和网络IO
    // reduce和reduceByKey的区别，可以理解为reduceByKey = keyBy + reduce；
    // reduceByKey要求数据类型为二元组，reduce则非常自由，数据类型可以是任意元组甚至任意类型
    val res = input.map(line => {
      val arr = line.split(",")
      (arr(0), arr(1).toInt)
    }).sortBy(x => x._2)
      .groupByKey()
      .sortByKey()

    res.foreach(println)
//    res.saveAsTextFile("22_spark/src/main/document/结果文件/res2")

    sc.stop()
  }
}
