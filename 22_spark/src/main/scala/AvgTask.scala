import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}

/**
  * @author Yuliang.Lee
  * @date 2021/10/10 1:48
  * @version 1.0
  */
object AvgTask {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.WARN); // 屏蔽掉多余的日志

    // 创建上下文环境
    val conf = new SparkConf().setAppName("avg_task").setMaster("local")
    val sc = new SparkContext(conf)

    // 初始化数据
    val input = List(("spark", 2), ("hadoop", 6), ("hadoop", 4), ("spark", 6))

    // 计算图书每天的平均销量
    // 注：reduceByKey只能处理K-V二元组，不能处理三元组，但是可以构造嵌套的伪多元组
    val res = sc.parallelize(input, 1).map(x => (x._1, (x._2, 1)))
      .reduceByKey((a,b) => (a._1 + b._1, a._2 + b._2))
      .map(x => (x._1, Math.ceil(x._2._1/x._2._2)))

    // 输出结果
    res.foreach(println)

    // 释放资源
    sc.stop()
  }
}
