import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}

/**
  * @author Yuliang.Lee
  * @date 2021/10/11 22:44
  * @version 1.0
  */
object TopNTask {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.WARN); // 屏蔽掉多余的日志

    val conf = new SparkConf().setAppName("top_n_task").setMaster("local")
    val sc = new SparkContext(conf)

    // sc读取多个文件作为输入源可以用逗号分隔路径
    val rdd1 = sc.textFile("22_spark/src/main/document/测试数据/task3.txt")
    // 数据初始化
    val rdd2 = rdd1.map(line => {
        val arr = line.split(",")
        (arr(0), (arr(0), arr(1), arr(2), arr(3).toInt, arr(4)))
    })
    // 根据日期分组并取组内成交金额top3的记录，组之间按照日期升序排序
    val rdd3 = rdd2.sortBy(x => x._2._4, false).groupByKey().sortByKey().flatMap(x => x._2.take(3))

    rdd3.foreach(println)

    sc.stop()
  }
}
