
1、数组
create database db;
use db;
create table person(name string,location array<string>) row format delimited fields terminated by "\t" collection items terminated by ",";
vim array.txt

Huangbo	beijing,shanghai,tianjin,Hangzhou
Xuzheng	tianjin,chengdu,wuhan 
Wangbaoqiang	wuhan,shenyang,jilin

load data local inpath '/home/data/array.txt' into table person;

select * from person;


select location from person;
select location[0] from person;

2、map
vim map.txt
huangbo	yuwen:80,shuxue:89,yingyu:95
xuzheng	yuwen:70,shuxue:65,yingyu:81
wangbaoqiang	yuwen:75,shuxue:100,yingyu:75

接下来创建一个新的表，可以解析这个三个数据：
create table score(name string, scores map<string,int>) row format delimited fields terminated by '\t' collection items terminated by ',' map keys terminated by ':';
desc formatted score;

load data local inpath '/home/data/map.txt' into table score;

select * from score;
select name from score; 
select scores from score; 
select s.scores['yuwen'] from score s;



3、struct结构

建表语句： 
create table structtable(id int,course struct<name:string,score:int>) row format delimited fields terminated by '\t' collection items terminated by ','; 
 
数据： 
vim structtable.txt
1	english,80
2	math,89
3	chinese,95
 
导入数据： 
load data local inpath '/home/data/structtable.txt' into table structtable; 
 
查询语句： 
select * from structtable;
select id from structtable;
select course from structtable;
select t.course.name from structtable t;
select t.course.score from structtable t;

4、视图
use myhive;
查询下面的语句
select department, count(*) as total from student group by department;

select a.department,a.total from (select department, count(*) as total from student group by department) a;

创建一个视图：
create view groupby_dpt_count as select department, count(*) as total from student group by department;

select a.department,a.total from groupby_dpt_count a;//虚表
select department,total from groupby_dpt_count;//虚表

show views;////1.x不对，2.x对
show tables;//对的

删除视图：
drop table groupby_dpt_count;//这是不可以的。
drop view groupby_dpt_count;//可以的



