package com.nx.hive;

import org.apache.hadoop.hive.ql.exec.UDF;

/**
 * @author LIAO
 * create  2020-12-30 22:31
 * 将大写转化为小写
 */
public class ToLowerCase extends UDF {
    // 必须是 public，并且 evaluate 方法可以重载
    public String evaluate(String field)
    {
        String result = field.toLowerCase();
        return result;
    }
}