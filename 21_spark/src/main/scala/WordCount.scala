import org.apache.spark.{SparkConf, SparkContext}

object WordCount {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("WordCount").setMaster("local")
    //创建一个SparkContext对象
    val sc = new SparkContext(conf)

    //执行WordCount
    val result = sc.textFile("F:\\IDEAProjects\\bigdata_course\\20_spark\\src\\main\\resources\\test.txt")
      .flatMap(_.split(" "))
      .map((_,1))
      .reduceByKey(_+_)

    //打印
    result.foreach(println)
    //释放资源
    sc.stop()

  }
}
