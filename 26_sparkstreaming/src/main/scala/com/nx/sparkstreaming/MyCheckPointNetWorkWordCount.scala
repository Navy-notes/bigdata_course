package com.nx.sparkstreaming

import org.apache.log4j.{Level, Logger}
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.Seconds
import org.apache.spark.SparkConf
import org.apache.spark.storage.StorageLevel

object MyCheckPointNetWorkWordCount {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org.apache.spark").setLevel(Level.ERROR)
    // 通过StreamingContext直接创建一个StreamingContext对象 ----> 直接指定检查点
    val ssc = StreamingContext.getOrCreate("hdfs://192.168.85.111:9000/sparkckpt0", creatingFunc)

    // 创建一个DStream,从netcat服务器接收数据
    val lines = ssc.socketTextStream("192.168.85.111", 5678, StorageLevel.MEMORY_ONLY)

    // 进行单词计数,分词
    val words = lines.flatMap(_.split(" "))
    // 计数
    val wordCount = words.map((_, 1)).reduceByKey(_ + _)
    // 使用transform完成生成一个数组,完成跟map一样的作用
    // val wordPair = words.transform(x => x.map(x => (x, 1)))


    // 打印结果
    wordCount.print()

    // 启动StreamingContext,进行计算
    ssc.start()

    // 等待任务的结束
    ssc.awaitTermination()
  }

  def creatingFunc():StreamingContext = {
    val sparkConf = new SparkConf().setAppName("MyCheckPointNetWorkWordCount").setMaster("local[2]")
    val ssc = new StreamingContext(sparkConf,Seconds(3))

    // 设置检查点目录
    ssc.checkpoint("hdfs://192.168.85.111:9000/sparkckpt0")

    // 返回对象
    ssc
  }
}