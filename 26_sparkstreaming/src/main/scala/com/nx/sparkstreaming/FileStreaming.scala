package com.nx.sparkstreaming

import org.apache.spark.SparkConf
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.Seconds
import org.apache.spark.storage.StorageLevel
import org.apache.log4j.Logger
import org.apache.log4j.Level

object FileStreaming {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org.apache.spark").setLevel(Level.ERROR)

    // 创建一个StreamingContext对象,以local模式为例
    // 注意:保证cpu核数大于等于2
    val conf = new SparkConf().setAppName("MyNetworkWordCount").setMaster("local[2]")
    // 两个参数:1.conf参数  2.采样时间间隔:每隔3s
    val ssc = new StreamingContext(conf, Seconds(3))

    // 直接监控本地的某个目录，如果有新的文件产生，就读取进来-----> 直接打印
    // （只能采集新增的文件，原文件的内容变更是采集不到的；
    // 如果是单单拷贝监控前的原文件然后改个文件名而里面的内容不做变更的话，哪怕属于新增的文件也是采集不到的）
    val lines = ssc.textFileStream("26_sparkstreaming/src/main/document/configure")
    // 直接打印
    lines.print()

    ssc.start()
    ssc.awaitTermination()
  }
}