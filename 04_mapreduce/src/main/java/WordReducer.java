import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * @author LIAO
 * create  2021-06-18 22:11
 * Reducer<KEYIN,VALUEIN,KEYOUT,VALUEOUT>
 *     KEYIN： map阶段输出过来的key  类型
 *     VALUEIN： map阶段输出过来的value的 类型
 *     KEYOUT：最终要输出的key的类型   单词的类型
 *     VALUEOUT： 最终要输出的value的类型  次数的类型
 */
public class WordReducer extends Reducer<Text,LongWritable,Text,LongWritable> {
    /**
     *
     * @param key 单词
     * @param values 相同单词的次数
     * @param context 上下问
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    protected void reduce(Text key, Iterable<LongWritable> values, Context context) throws IOException, InterruptedException {
        //1、定义一个统计的变量
        long count = 0;

        //2、迭代
        for (LongWritable value : values) {
            count += value.get();
        }

        //3、写入到上下文中
        context.write(key,new LongWritable(count));
    }
}
