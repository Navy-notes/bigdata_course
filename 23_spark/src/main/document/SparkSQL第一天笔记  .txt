﻿
SparkSQL核心原理剖析

导师：王端阳 
内容大纲： 

（1）SparkSQL概述 

（2）使用caseclass定义表
    DataFrame 就是表 SparkSQL对结构化数据的一个抽象 类似于 RDD
    SparkCOre -->RDD
    SparkSQL-->DataFrame

    DataFrame表现形式就是RDD  --RDD 分区

    7369	SMITH	CLERK	7902	1980/12/17	800	0	20


    a. 定义一个case class 代表schema
        case class Emp(
            empno:Int,
            ename:String,
            job:String,
            mgr:String,
            hiredate:String,
            sal:Int,
            comm:String,
            deptno:Int
        )

    b. 导入数据
        val lines = sc.textFile("F:\\IDEAProjects\\bigdata_course\\23_spark\\src\\main\\document\\emp.csv").map(_.split(","))


    c. 生成DataFrame
        val allEmp =lines.map(x=>Emp(x(0).toInt,x(1),x(2),x(3),x(4),x(5).toInt,x(6),x(7).toInt))
        val empDF=allEmp.toDF


    DSL语句操作DateFrame：
        empDF.show --->select * from emp
        empDF.printSchema -->desc emp


（3）使用SparkSession对象创建表
    a.什么是SparkSession对象：
        Spark context available as 'sc' (master = spark://bigdata111:7077, app id = app-20210123042807-0000).
        Spark session available as 'spark'.

    spark-2.0 以后  spark提供的新的访问接口  提供统一的访问方式

    SparkCore   SparkSQL    SparkStreaming

    def createDataFrame(rowRDD: RDD[Row], schema: StructType): DataFrame


    b. 代码实例：
    -------------------------------------------------------------------------------------------------------------
    import org.apache.spark.sql._
    import org.apache.spark.sql.types._

    // 构造表结构StructType
    val myschema = StructType(List(StructField("empno", DataTypes.IntegerType), StructField("ename", DataTypes.StringType),StructField("job", DataTypes.StringType),StructField("mgr", DataTypes.StringType),StructField("hiredate", DataTypes.StringType),StructField("sal", DataTypes.IntegerType),StructField("comm", DataTypes.StringType),StructField("deptno", DataTypes.IntegerType)))

    // 从文件中读取数据
    val lines=sc.textFile("F:\\IDEAProjects\\bigdata_course\\23_spark\\src\\main\\document\\emp.csv").map(_.split(","))

    // 把读入的数据每一行映射成一个个Row
    val rowRDD=lines.map(x=>Row(x(0).toInt,x(1),x(2),x(3),x(4),x(5).toInt,x(6),x(7).toInt))

    //使用SparkSession创建DateFrame
    val df=spark.createDataFrame(rowRDD,myschema)
    -------------------------------------------------------------------------------------------------------------


（4）读取带格式的文件创建表(最简单的方式)
    比如：JSON文件

    前提:数据本身一定要具有格式

    {"name":"Michael"}
    {"name":"Andy", "age":30}
    {"name":"Justin", "age":19}

    val peopleDF=spark.read.json("F:\\IDEAProjects\\bigdata_course\\23_spark\\src\\main\\document\\emp.json")


（5）操作DataFrame
    【DSL语句示例】

    empDF
    查看所有员工信息: empDF.show

    查看所有员工的姓名
    empDF.select("ename").show
    empDF.select($"ename").show

    查询所有的员工姓名和薪水，并给薪水加100块钱
    sal sal+100
    empDF.select($"ename",$"sal",$"sal"+100).show

    where 查询工资大于2000的员
    empDF.filter($"sal">2000).show

    分组
    empDF.groupBy("deptno").count.show




    【SQL语句】
    前提：需要把DataFrame注册成table或者一个view

    createOrReplaceTempView

    empDF.createOrReplaceTempView("emp")

    SparkSession    ---spark

    spark.sql("select * from emp").show


    查询一下10号部门员
    spark.sql("select * from emp where deptno=10").show

    分组
    求每个部门的工资总和
    要求展示部门号,工资总额
    spark.sql("select deptno,sum(sal) from emp group by deptno").show


（6）使用load和save函数
    数据源的数据格式：parquet json csv jdbc

    a. load函数（加载数据）默认文件格式parquet
        自动生成表 DataFrame

        // 使用load直接加载parquet数据格式
        val userDF =spark.read.load("/root/tools/temp/users.parquet")

        // 两种方式加载json数据格式
        val peopleDF=spark.read.json("F:\\IDEAProjects\\bigdata_course\\23_spark\\src\\main\\document\\emp.json")
        val peopleDF=spark.read.format("json").load("F:\\IDEAProjects\\bigdata_course\\23_spark\\src\\main\\document\\emp.json")

        jdbc (oracle/mysql )


    b. save函数（保存结果）默认存储的文件格式是parquet

        只查询用户的名字和喜欢的颜色,并保存起来

        // save直接保存parquet格式的数据
        userDF.select($"name",$"favorite_color").write.save("/root/tools/temp/result666")

        // 两种方式保存csv格式的数据
        userDF.select($"name",$"favorite_color").write.format("csv").save("/root/tools/temp/result777")
        userDF.select($"name",$"favorite_color").write.csv("/root/tools/temp/result777")














1.Hive   hadoop SQL-->Mapreduce
SparkSQL  SQL-->Spark任务 -->提交到Spark集群上运行
2.底层依赖的是RDD  
SQL-->转换成对RDD操作   


一.Spark SQL基础
1.什么是SparkSQL
2. 核心概念 DataFrame --->表 :Schema data
    Hive create table

    Schema :表结构
    data:数据

    spark2.3版本 DataSet

3.如何创建表

4.操作表
    两种语言:
    SQL,DSL

5.支持视图  

6.DataSet 



二.在Spark SQL中使用数据源
1. load,save函数
    load    加载数据源
    save函数 保存结果
2. 数据源
    a.默认Parquet文件   （列式存储文件）
    b.JSON文件    （逗号分割 花括号括起来）
    c.JDBC Mysql oracle -->加载到SparkSQL里面 -->DataFrame
    d.hive table
 

三.开发Spark SQL程序
    1.指定schema的方式创建DataFrame
    2.使用case class创建DataFrame
    3.自定义函数



四.性能优化
缓存





作业:
1.创建DataFrame的三种方式
case class / SparkSession /带格式的文件(截图)

2.讲义上的dataset练习一下
    Dataset是一个分布式的数据收集器。这是在Spark1.6之后新加的一个接口，兼顾了RDD的优点（强类型）
    可以把DataFrames看成是一种特殊的（无类型的）Datasets，即：Dataset(Row)

3.Load/Save函数 加载并存储(练习,后面项目会用到)




复习&总结：
1. 构建一个DataFrame的三种方式：
    a、使用case class定义表，然后通过RDD对象调用toDF()转换  （需要导入隐式转换import spark.sqlContext.implicits._）
    b、使用SparkSession的createDateFrame(rowRDD, StructType)
    c、使用saprkSession.read读取带格式的文件创建表



