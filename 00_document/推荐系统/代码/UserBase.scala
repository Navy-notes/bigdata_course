package mllib

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.mllib.linalg.distributed.MatrixEntry
import org.apache.spark.mllib.linalg.distributed.CoordinateMatrix
import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.linalg.distributed.RowMatrix

object UserBase {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.OFF)
    System.setProperty("spark.ui.showConsoleProgress", "false")
    //读入数据
    val conf = new SparkConf().setAppName("UserBaseModel").setMaster("local")
    val sc = new SparkContext(conf)
    val data = sc.textFile("D:\\temp\\ratingdata.txt")

    /*MatrixEntry代表一个分布式矩阵中的每一行(Entry)
     * 这里的每一项都是一个(i: Long, j: Long, value: Double) 指示行列值的元组tuple。
     * 其中i是行坐标，j是列坐标，value是值。*/
    val parseData: RDD[MatrixEntry] = data.map(_.split(",")
    match { case Array(user, item, rate) => MatrixEntry(user.toLong, item.toLong, rate.toDouble)
    })

    //CoordinateMatrix是Spark MLLib中专门保存user_item_rating这种数据样本的
    val ratings = new CoordinateMatrix(parseData)

    /* 由于CoordinateMatrix没有columnSimilarities方法，所以我们需要将其转换成RowMatrix矩阵，调用他的columnSimilarities计算其相似性
     * RowMatrix的方法columnSimilarities是计算，列与列的相似度，现在是user_item_rating，需要转置(transpose)成item_user_rating,这样才是用户的相似*/
    val matrix: RowMatrix = ratings.transpose().toRowMatrix()
    //计算用户的相似性，并输出
    val similarities = matrix.columnSimilarities()
    println("用户相似性矩阵")
    similarities.entries.collect().map(x => {
      println(x.i + "->" + x.j + "->" + x.value)
    })

    //得到用户1对所有物品的评分
    val ratingOfUser1 = ratings.entries.filter(_.i == 1).map(x => {
      (x.j, x.value)
    }).sortBy(_._1).collect().map(_._2).toList.toArray
    println("得到用户1对每种物品的评分")
    for (s <- ratingOfUser1) println(s)

    //得到用户1相对于其他用户的相似性（即：权重），降序排列：value越大，表示相似度越高
    val weights = similarities.entries.filter(_.i == 1).sortBy(_.value, false).map(_.value).collect()
    println("用户1相对于其他用户的相似性")
    for (s <- weights) println(s)

    //用户1对所有物品的平均评分
    val avgRatingOfUser1 = ratingOfUser1.sum / ratingOfUser1.size
    println("用户1对所有物品的平均评分：" + avgRatingOfUser1)

    //其他用户对物品1的评分，drop(1)表示除去用户1的评分
    val otherRatingsToItem1 = matrix.rows.collect()(0).toArray.drop(1)
    println("其他用户对物品1的评分")
    for (s <- otherRatingsToItem1) println(s)


    sc.stop()
  }
}












