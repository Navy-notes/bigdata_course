package mydemo;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.common.LongPrimitiveIterator;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.recommender.GenericItemBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.EuclideanDistanceSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.similarity.ItemSimilarity;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class ItemBased {
	final static int RECOMMENDER_NUM = 3;// 推荐物品的最大个数

	public static void main(String[] args) throws IOException, TasteException {
		//根据数据源建立数据模型，即：打分矩阵
		String file = "D:\\temp\\ratingdata.txt";
		DataModel model = new FileDataModel(new File(file));// 数据模型

		//计算物品的相似度
		ItemSimilarity itemSimilarity = new EuclideanDistanceSimilarity(model);
		//ItemSimilarity itemSimilarity = new UncenteredCosineSimilarity(model);

		// 物品推荐算法 其中model为数据模型，usersimilarity为相似度模型
		Recommender r = new GenericItemBasedRecommender(model, itemSimilarity);

		//输出结果：为每个用户推荐的物品ID
		LongPrimitiveIterator iter = model.getUserIDs();
		while (iter.hasNext()) {
			long uid = iter.nextLong();
			List<RecommendedItem> list = r.recommend(uid, RECOMMENDER_NUM);
			System.out.println("用户ID: " + uid);
			for (RecommendedItem ritem : list) {
				System.out.println("给用户推荐的物品的ID: "+ ritem.getItemID()+ "\t 评分: "+ ritem.getValue());
			}
			System.out.println("************************");
		}
	}
}
