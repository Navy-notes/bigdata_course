package mllib

import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.mllib.recommendation.Rating
import scala.io.Source
import org.apache.spark.mllib.recommendation.MatrixFactorizationModel
import org.apache.spark.mllib.recommendation.ALS
import org.apache.spark.mllib.recommendation.Rating
import org.apache.spark.rdd.RDD

object ALSDemo {
  def main(args: Array[String]): Unit = {

    Logger.getLogger("org").setLevel(Level.OFF)
    System.setProperty("spark.ui.showConsoleProgress", "false")

    val sparkConf = new SparkConf().setAppName("ALSDemo").setMaster("local[2]")
    val sc = new SparkContext(sparkConf)

    //读入打分数据，并转成RDD
    val productRatings = loadRatingData("D:\\temp\\ratingdata.txt")
    val productRatingRDD = sc.parallelize(productRatings, 1)
    //计算：打分的个数、用户的个数、物品的个数
    val numRatings = productRatingRDD.count()
    val numUsers = productRatingRDD.map(x => x.user).distinct().count()
    val numProducts = productRatingRDD.map(x => x.product).distinct().count()
    println("评分数:" + numRatings + " 用户总数:" + numUsers + " 物品总数:" + numProducts)

    /*
            最小二乘法的模型需要以下三个参数：
            1、rank
            		对应ALS模型中的因子个数，也就是在低阶近似矩阵中的隐含特征个数。因子个数一般越多越好。
            		但它也会接影响模型训练和保存时所需的内存开销，尤其是在用户和物品很多的时候。
            		因此实践中该参数常作为训练效果与系统开销之间的调节参数。通常，其合理取值为10到200。
            		可以简单理解为：模型因子的列的数量
            2、iterations
            		对应运行时的迭代次数。ALS能确保每次迭代都能降低评级矩阵的重建误差，但一般经少数次迭代后ALS模型便已能收敛为一个比较合理的好模型。
            		这样，大部分情况下都没必要迭代太多次（10次左右一般就挺好）。

            3、lambda
            		该参数控制模型的正则化过程，从而控制模型的过拟合情况。其值越高，正则化越严厉。
            		该参数的赋值与实际数据的大小、特征和稀疏程度有关。和其他的机器学习模型一样，正则参数应该通过用非样本的测试数据进行交叉验证来调整。
               这里将使用的 rank、iterations 和 lambda 参数的值分别为50、10和0.01
               可以调整这些参数，不断优化结果，使均方差变小。比如iterations越多，lambda较小，均方差会较小，推荐结果较优
     */
    //注意：实际工作中，可以通过多次测试，找到最佳的 rank、iterations 和 lambda的值
    val model = ALS.train(productRatingRDD, 50, 10, 0.01)

    //val model = ALS.train(productRatingRDD, 10, 5, 0.5)

    val rmse = computeRMSE(model, productRatingRDD, numRatings)

    println("误差" + rmse )

    val recomm =model.recommendProducts(1,2)

    recomm.foreach {
      r => {
        println("用户：" + r.user.toString() + " 物品：" + r.product.toString() + "  评分：" + r.rating.toString())
      }
    }



    sc.stop()
  }

  //计算RMSE
  def computeRMSE(model: MatrixFactorizationModel, data: RDD[Rating], n: Long): Double = {
    val predictions: RDD[Rating] = model.predict((data.map(x => (x.user, x.product))))
    val predictionsAndRating = predictions.map {
      x => ((x.user, x.product), x.rating)
    }.join(data.map(x => ((x.user, x.product), x.rating))).values

    math.sqrt(predictionsAndRating.map(x => (x._1 - x._2) * (x._1 - x._2)).reduce(_ + _) / n)
  }

  //加载评分数据
  def loadRatingData(path: String): Seq[Rating] = {
    val lines = Source.fromFile(path).getLines()
    val ratings = lines.map {
      line =>
        val fields = line.split(",")
        Rating(fields(0).toInt, fields(1).toInt, fields(2).toDouble)
    }.filter { x => x.rating > 0.0 }

    if (ratings.isEmpty) {
      sys.error("No ratings provided")
    } else {
      ratings.toSeq
    }
  }
}








