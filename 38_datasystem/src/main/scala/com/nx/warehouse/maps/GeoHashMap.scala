package com.nx.warehouse.maps

import java.util.Properties

import ch.hsr.geohash.GeoHash
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession


/**
  * 构建地图位置字典
  */
object GeoHashMap {
  def main(args: Array[String]): Unit = {
    // 日志设置
    Logger.getLogger("org").setLevel(Level.WARN)
    System.setProperty("spark.ui.showConsoleProgress","false")

    // 构建SparkSession环境
    val spark = SparkSession.builder().appName(this.getClass.getSimpleName).master("local").getOrCreate()
    import spark.implicits._

    // 从Mysql中读取表数据 t_maps_mid
    val properties = new Properties()
    properties.put("user", "root")
    properties.put("password", "root")
    val df = spark.read.jdbc("jdbc:mysql://localhost:3306/datasystem","t_maps_mid", properties)
      .map(row => {
        val lng = row.getAs[Double]("lng")
        val lat = row.getAs[Double]("lat")
        val town = row.getAs[String]("town")
        val district = row.getAs[String]("district")
        val city = row.getAs[String]("city")
        val province = row.getAs[String]("province")

        // 将坐标信息通过GeoHash算法转化成GeoHash编码
        val geoHash = GeoHash.geoHashStringWithCharacterPrecision(lat, lng, 4)
        (geoHash, town, district, city, province)
      }).toDF("geo", "town", "district", "city", "province")


//    df.show(20, false)    // numRows:显示条数，truncate：是否每列最多只显示20个字符，默认为true
//    df.take(20).foreach(println(_))

    // 存储为parquet格式的文件
    df.write.parquet("F:\\IDEAProjects\\bigdata_course\\38_datasystem\\src\\main\\resources\\预处理数据字典\\maps")

    // 释放资源
    spark.stop()
  }

}
