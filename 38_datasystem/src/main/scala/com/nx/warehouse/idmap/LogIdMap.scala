package com.nx.warehouse.idmap

import java.util.Properties

import com.alibaba.fastjson.JSON
import com.nx.common.SparkUtils
import org.apache.commons.lang.StringUtils
import org.apache.log4j.{Level, Logger}
import org.apache.spark.graphx.{Edge, Graph}
import org.apache.spark.sql.{Dataset, SaveMode}

import scala.collection.mutable.ArrayBuffer

/**
  * 预处理日志数据，生成IdMapping
  */
object LogIdMap {
  val props = new Properties()
  val pathStream = this.getClass.getClassLoader.getResourceAsStream("path.properties")    //这里必须用项目根路径下的相对路径而不是文件的绝对路径
  props.load(pathStream)

  def main(args: Array[String]): Unit = {
    // 日志设置
//    Logger.getLogger("org").setLevel(Level.WARN)
//    System.setProperty("spark.ui.showConsoleProgress", "false")

    // 构建环境
    val spark = SparkUtils.getSparkSession(this.getClass.getSimpleName, "local[*]", Map[String,String]())

    // 读取数据并处理
    val logDS = logProcess(spark.read.textFile(props.getProperty("applog"))).cache()


   /* //读取原始的数据文件
    val applog: Dataset[String] = sparkSession.read.textFile(prop.getProperty("applog"))
    val weblog: Dataset[String] = sparkSession.read.textFile(prop.getProperty("weblog"))
    val wxlog: Dataset[String] = sparkSession.read.textFile(prop.getProperty("wxlog"))
    //抽取出来每一种日志可能的标识字段
    val applogArr: RDD[Array[String]] = logProcess(applog)
    val weblogArr: RDD[Array[String]] = logProcess(weblog)
    val wxlogArr: RDD[Array[String]] = logProcess(wxlog)
    //
    val logArr: RDD[Ar多端数据做整合ray[String]] = applogArr.union(weblogArr).union(wxlogArr)*/


    // 构建点集合
    val vertices = logDS.flatMap(_.map(element => (element.hashCode.toLong, element)))

    // 构建边集合
    val edges = logDS.flatMap(arr => {
      val list = ArrayBuffer[Edge[String]]()
      for (i <- 0 until(arr.length - 1)) {
        list.append(Edge(arr(i).hashCode.toLong, arr(i+1).hashCode.toLong))
      }
      list
    })
      // 过滤掉出现的边小于经验阈值的数据
      .map((_,1))
      .reduceByKey(_+_)
      .filter(_._2 > 2)
      .map(_._1)

    // 绘图，找到连通域的点
    val graph = Graph(vertices, edges)
    val connectedVert = graph.connectedComponents().vertices

    // 保存结果或者打印输出查看
    import spark.implicits._
//    connectedVert.toDF("ids_hashcode","guid").show(50, false)
    connectedVert.coalesce(6,true).toDF("ids_hashcode","guid").write.mode(SaveMode.Overwrite).parquet(props.getProperty("resPath"))

    // 释放资源
    spark.stop()
  }


  /**
    * 公共的处理方法，用来处理applog日志、web日志、微信小程序日志 数据的。抽取出来每一种日志可能的用户标识字段
    * @param logdata
    * @return
    */
  private def logProcess(logdata: Dataset[String]) = {
    logdata.rdd.map(line => {
      val lineObj = JSON.parseObject(line)
      val userObj = lineObj.getJSONObject("user")
      val uid = userObj.getString("uid")
      val phoneObj = userObj.getJSONObject("phone")
      val imei = phoneObj.getString("imei")
      val mac = phoneObj.getString("mac")
      val imsi = phoneObj.getString("imsi")
      val androidId = phoneObj.getString("androidId")
      val deviceId = phoneObj.getString("deviceId")
      val uuid = phoneObj.getString("uuid")

      Array(uid, imei, mac, imsi, androidId, deviceId, uuid).filter(StringUtils.isNotBlank(_))
    })
  }

}
