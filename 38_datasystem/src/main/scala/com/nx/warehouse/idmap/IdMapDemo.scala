package com.nx.warehouse.idmap

import com.nx.common.SparkUtils
import org.apache.log4j.{Level, Logger}
import org.apache.spark.graphx.{Edge, Graph}

/**
  * 图计算IDMapping映射的案例
  * 使用测试数据：F:\IDEAProjects\bigdata_course\38_datasystem\src\main\resources\图计算测试数据\graph1.txt
  */
object IdMapDemo {
  def main(args: Array[String]): Unit = {
    // 日志设置
    Logger.getLogger("org").setLevel(Level.WARN)
    System.setProperty("spark.ui.showConsoleProgress","false")

    // 构建环境
    val spark = SparkUtils.getSparkSession(this.getClass.getSimpleName, "local", Map[String,String]())

    // 读取数据
    val sourceRDD = spark.read.textFile("F:\\IDEAProjects\\bigdata_course\\38_datasystem\\src\\main\\resources\\图计算测试数据\\graph1.txt").rdd.cache()

    // 选取能够识别用户标识的字段，构建顶点
    val vertices = sourceRDD.flatMap(line => {
      val set = Set[(Long, String)]()
      val fields = line.split(",")
      set.apply((Math.abs(fields(0).hashCode.toLong)), fields(0))
      set.apply((Math.abs(fields(1).hashCode.toLong)), fields(1))
      set.apply((Math.abs(fields(2).hashCode.toLong)), fields(2))
      set
    })

    // 构建边，同一行数据的顶点即为边关系
    val edges = sourceRDD.flatMap(line => {
      val fields = line.split(",")
      var list = List[Edge[String]]()
      for(index <- 0 to fields.length - 3) {
        /*
        本案例中之所以减少3，是因为我们的数据最后一列没有用。要是最后一列也需要放到点集合中，那么这里就减少2
        在实际的工作中，只需要给你自己需要连接的那些点拿过来就可以了。
        例如：长度是4，那么减少3就会出现 0 1 。 那么对应的边 （0,1） (1,2)
        如果减少2 ，那么就会出现 0 1 2 ，那么对应的边就是 (0,1)  (1,2)  (2,3)
         */
        list ++= List(Edge(Math.abs(fields(index).hashCode.toLong), Math.abs(fields(index+1).hashCode.toLong), ""))
      }
      list
    })

    // 绘图，提取连通域的点关系
    val graph = Graph(vertices, edges)
    val connectedVertices = graph.connectedComponents().vertices

    // 根据连通图提取IdMap，并在上下文中广播
    val idMap = connectedVertices.collectAsMap()
    val idMapBroadcast = spark.sparkContext.broadcast(idMap)

    // 通过idMapBroadcast生成全局用户标识guid，并把映射关系保存文件/打印
    sourceRDD.map(line => {
      val key = Math.abs(line.split(",")(0).hashCode.toLong)
      val guid = idMapBroadcast.value.get(key).getOrElse("")
      ("guid_" + guid, line)
    }).foreach(println)

//    res.write.mode(SaveMode.Overwrite).text("output/graph/demo_res")

    // 释放资源
    spark.stop()
  }
}
