package com.nx.warehouse.beans


case class AppLogBean(
                       var guid:Long,
                       eventid:String,
                       event:Map[String,String],
                       uid:String,
                       imei:String,
                       mac:String,
                       imsi:String,
                       osName:String,
                       osVer:String,
                       androidId:String,
                       resolution:String,
                       deviceType:String,
                       deviceId:String,
                       uuid:String,
                       appid:String,
                       appVer:String,
                       release_ch:String,
                       promotion_ch:String,
                       longtitude:Double,
                       latitude:Double,
                       carrier:String,
                       netType:String,
                       cid_sn:String,
                       ip:String,
                       sessionId:String,
                       timestamp:Long,
                       var district:String= "weizhi",//未知的情况，就是没有找到对应的省市区
                       var city:String= "weizhi",//未知的情况
                       var province:String= "weizhi"//未知的情况
                     )

