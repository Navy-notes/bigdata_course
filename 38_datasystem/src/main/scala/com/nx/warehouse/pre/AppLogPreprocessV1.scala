package com.nx.warehouse.pre

import ch.hsr.geohash.GeoHash
import com.alibaba.fastjson.{JSON, JSONObject}
import com.nx.common.SparkUtils
import com.nx.warehouse.beans.AppLogBean
import org.apache.commons.lang.StringUtils
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.{Row, SaveMode}

/**
  * 企业级数据预处理
  *
  * F:\IDEAProjects\bigdata_course\38_datasystem\src\main\resources\预处理数据字典\maps
  * F:\IDEAProjects\bigdata_course\38_datasystem\src\main\resources\预处理数据字典\idmap
  * F:\IDEAProjects\bigdata_course\38_datasystem\src\main\resources\日志数据\aa.access.log.app
  * F:\IDEAProjects\bigdata_course\38_datasystem\src\main\resources\result_preprocess
  */
object AppLogPreprocessV1 {
  Logger.getLogger("org").setLevel(Level.WARN)

  def main(args: Array[String]): Unit = {
    // 构建环境
    val spark = SparkUtils.getSparkSession(this.getClass.getSimpleName, "local", Map[String, String]())
    import spark.implicits._

    // 接受参数
    if (args.length < 4) {
      println(
        """
          |com.nx.warehouse.pre.AppLogPreprocessV1 <areaMapFilePath><idMapFilePath><applogFilePath><resOutputPath>
          |<areaMapFilePath>  地理位置映射文件路径
          |<idMapFilePath>  全局用户id映射文件路径
          |<applogFilePath> 日志文件路径
          |<resOutputPath>  结果保存路径
        """.stripMargin)
      System.exit(0)
    }
    val Array(areaMapFilePath, idMapFilePath, applogFilePath, resOutputPath) = args

    // 1、读取maps地理位置映射文件构造地理位置字典
    val areaMapDict = spark.read.parquet(areaMapFilePath).rdd.map({
      // 读取带格式的文件，需要声明一个带schema的Row
      case Row(geo:String, town:String, district:String, city:String, province:String) => {
        (geo, (province, city, district))   // 根据需求只要精确到区级的地理位置
      }
    }).collectAsMap()   // 转换成map会对Key去重
    // 广播地理位置字典
    val areaMapDictBd = spark.sparkContext.broadcast(areaMapDict)


    // 2、读取idmap全局用户id映射文件构造全局用户id字典
    val guidMapDict = spark.read.parquet(idMapFilePath).rdd.map(row => (row.getLong(0), row.getLong(1))).collectAsMap()
    // 广播guid字典
    val guidMapDictBd = spark.sparkContext.broadcast(guidMapDict)


    // 3、读取applog日志数据，封装为AppLogBean对象
    spark.read.textFile(applogFilePath).map(line => {
      try {
        val jsonobj = JSON.parseObject(line)
        //解析，抽取各个字段
        val eventid = jsonobj.getString("eventid")

        // 这里必须这么写
        import scala.collection.JavaConversions._
        val event = jsonobj.getJSONObject("event").getInnerMap.asInstanceOf[java.util.Map[String, String]].toMap


        val userobj = jsonobj.getJSONObject("user")
        val uid = userobj.getString("uid")

        val phoneobj = userobj.getJSONObject("phone")
        val imei = phoneobj.getString("imei")
        val imsi = phoneobj.getString("imsi")
        val mac = phoneobj.getString("mac")
        val osName = phoneobj.getString("osName")
        val osVer = phoneobj.getString("osVer")
        val androidId = phoneobj.getString("androidId")
        val resolution = phoneobj.getString("resolution")
        val deviceType = phoneobj.getString("deviceType")
        val deviceId = phoneobj.getString("deviceId")
        val uuid = phoneobj.getString("uuid")

        val appobj = userobj.getJSONObject("app")
        val appid = appobj.getString("appid")
        val appVer = appobj.getString("appVer")
        val release_ch = appobj.getString("release_ch")
        val promotion_ch = appobj.getString("promotion_ch")

        val locobj = userobj.getJSONObject("loc")
        val longtitude = locobj.getDouble("longtitude")
        val latitude = locobj.getDouble("latitude")
        val carrier = locobj.getString("carrier")
        val ip = locobj.getString("ip")
        val cid_sn = locobj.getString("cid_sn")
        val netType = locobj.getString("netType")

        val sessionId = userobj.getString("sessionId")
        val timestamp = jsonobj.getString("timestamp").toLong

        val flagFields = uid.concat(imei).concat(uuid).concat(mac).concat(androidId).concat(imsi).replaceAll("null", "")
        // 判断数据是否符合规则
        var bean: AppLogBean = null
        if (StringUtils.isNotBlank(flagFields) && event != null && StringUtils.isNotBlank(eventid) && StringUtils.isNotBlank(sessionId)) {
          bean = AppLogBean(
            Long.MinValue,
            eventid,
            event,
            uid,
            imei,
            mac,
            imsi,
            osName,
            osVer,
            androidId,
            resolution,
            deviceType,
            deviceId,
            uuid,
            appid,
            appVer,
            release_ch,
            promotion_ch,
            longtitude,
            latitude,
            carrier,
            netType,
            cid_sn,
            ip,
            sessionId,
            timestamp
          )
        }
        bean
      } catch {
        case e: Exception => null
      }
    })
      .filter(_ != null)
      // 根据广播的字典进行数据集成
      .map(bean => {
        // 集成省、市、区
        val lat = bean.latitude
        val lnt = bean.longtitude
        val geohash = GeoHash.geoHashStringWithCharacterPrecision(lat, lnt, 4)
        val maybeTuple = areaMapDictBd.value.get(geohash)
        if (maybeTuple.isDefined) {
          val tuple = maybeTuple.get
          bean.province = tuple._1
          bean.city = tuple._2
          bean.district = tuple._3
        }
        // 集成guid
        val ids = Array(bean.imei,bean.imsi,bean.mac,bean.uid,bean.androidId,bean.uuid,bean.deviceId)
        var isBreak = false
        for (id <- ids if !isBreak) {
          val maybeLong = guidMapDictBd.value.get(id.hashCode.toLong)
          if (maybeLong.isDefined) {
            bean.guid = maybeLong.get
            isBreak = true
          }
        }
        bean
    })
      .filter(_.guid != Long.MinValue)
      // 保存结果
      .repartition(2)
      .write
      .mode(SaveMode.Overwrite)
      .parquet(resOutputPath)

    // 释放资源
    spark.stop()
  }
}
