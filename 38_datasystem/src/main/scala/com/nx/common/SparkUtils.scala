package com.nx.common

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

/**
  * 工具类
  */
object SparkUtils {
  def getSparkSession(appName:String="util_app", master:String="local[*]", props:Map[String, String]=Map.empty) = {
    val conf = new SparkConf()
    conf.setAll(props)
    SparkSession.builder().config(conf).appName(appName).master(master).getOrCreate()
  }
}
