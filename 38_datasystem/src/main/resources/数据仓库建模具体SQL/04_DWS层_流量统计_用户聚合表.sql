-- DWS层建表：流量统计_用户聚合表 DWS_APP_TFC_AGG_USER
-- 这个时候就是  一个用户是一条数据了 
-- 日期  uid 访问次数 访问总时长  pv总数
-- date1 u01 20 	  2000        200
-- 访问次数：就是会话个数
-- 访问总时长：每个会话的时长之和。
 
USE DATASYS;
DROP TABLE IF EXISTS DATASYS.DWS_APP_TFC_AGG_USER;
CREATE TABLE DATASYS.DWS_APP_TFC_AGG_USER(
guid           bigint,
se_cnts        int,
ac_time        bigint,
pv_cnts        int,
province       string,
city           string,
district       string,
devicetype     string,
osname         string,
osver          string,
release_ch     string,
promotion_ch   string
)
PARTITIONED BY (dt string)
stored as parquet;


/*
计算逻辑：
	分组聚合
		uid ： 作为分组 key
		访问次数： count
		访问总时长 ：sum (end_time-start_time)
		pv总数： sum (pv_cnts)
		省、市、区、手机型号、操作系统、系统版本都是维度，在每组中取一个值 max
*/
-- SRC：流量统计_会话聚合表 DWS_APP_TFC_AGG_SESSION
-- DST: 流量统计_用户聚合表 DWS_APP_TFC_AGG_USER
INSERT INTO TABLE DATASYS.DWS_APP_TFC_AGG_USER PARTITION (dt='2021-10-02')
SELECT
guid,
count(1) as se_cnts,
sum(end_time-start_time) as ac_time,
sum(pv_cnts) as pv_cnts,
province ,
city,
district,
devicetype,
osname,
osver,
release_ch,
promotion_ch
FROM DATASYS.DWS_APP_TFC_AGG_SESSION WHERE dt='2021-10-02'
GROUP BY guid,province,city,district,devicetype,osname,osver,release_ch,promotion_ch
;
