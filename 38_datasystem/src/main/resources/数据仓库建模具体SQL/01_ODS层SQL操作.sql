-- 建库datasys
CREATE DATABASE datasys;

-- 建表 datasys.ods_app_log
use datasys;
CREATE EXTERNAL TABLE datasys.ods_app_log(
guid bigint,
eventid  String,
event  Map<String, String>,
uid  String,
imei  String,
mac  String,
imsi  String,
osName  String,
osVer  String,
androidId  String,
resolution  String,
deviceType  String,
deviceId  String,
uuid  String,
appid  String,
appVer  String,
release_ch  String,
promotion_ch  String,
longtitude  Double,
latitude  Double,
carrier  String,
netType  String,
cid_sn  String,
ip  String,
sessionId  String,
`timestamp`  bigint,
province String,
city String,
district String
)
partitioned by (dt string)
stored as parquet
;

-- 加载预处理后数据到ods_app_log表2021-10-02分区中
load data local inpath '/home/data/output/app/2021-10-02/' into table datasys.ods_app_log partition(dt='2021-10-02');

-- 查看10条数据。数据太多了，不要一次性全部查看。否则需要打印输出很久。
hive> select * from ods_app_log limit 10;



-- 同理其他的 web日志 以及 wx日志 也是同样的上面的建表语句。但是我们数据是一样的。测试一个app的就可以了。

-- 建表 datasys.ods_web_log
use datasys;
CREATE EXTERNAL TABLE datasys.ods_web_log(
guid bigint,
eventid  String,
event  Map<String, String>,
uid  String,
imei  String,
mac  String,
imsi  String,
osName  String,
osVer  String,
androidId  String,
resolution  String,
deviceType  String,
deviceId  String,
uuid  String,
appid  String,
appVer  String,
release_ch  String,
promotion_ch  String,
longtitude  Double,
latitude  Double,
carrier  String,
netType  String,
cid_sn  String,
ip  String,
sessionId  String,
`timestamp`  bigint,
province String,
city String,
district String
)
partitioned by (dt string)
stored as parquet
;

-- 建表 datasys.ods_wx_log
use datasys;
CREATE EXTERNAL TABLE datasys.ods_wx_log(
guid bigint,
eventid  String,
event  Map<String, String>,
uid  String,
imei  String,
mac  String,
imsi  String,
osName  String,
osVer  String,
androidId  String,
resolution  String,
deviceType  String,
deviceId  String,
uuid  String,
appid  String,
appVer  String,
release_ch  String,
promotion_ch  String,
longtitude  Double,
latitude  Double,
carrier  String,
netType  String,
cid_sn  String,
ip  String,
sessionId  String,
`timestamp`  bigint,
province String,
city String,
district String
)
partitioned by (dt string)
stored as parquet
;