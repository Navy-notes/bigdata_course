--01 DWD层建表：app日志事件全局明细表DWD_APP_GLB_DTL
USE DATASYS;
CREATE TABLE DATASYS.DWD_APP_GLB_DTL(
guid bigint COMMENT '用户全局统一标识',
eventid  String COMMENT '事件ID',
event  Map<String, String> COMMENT '事件详情信息',
uid  String COMMENT '用户登录账号id',
imei  String,
mac  String,
imsi  String,
osName  String,
osVer  String,
androidId  String,
resolution  String,
deviceType  String,
deviceId  String,
uuid  String,
appid  String,
appVer  String,
release_ch  String,
promotion_ch  String,
longtitude  Double,
latitude  Double,
carrier  String,
netType  String,
cid_sn  String,
ip  String,
sessionId  String,
`timestamp`  bigint,
province String,
city String,
district String,
year  string,
month string,
day string,
datestr string
)
COMMENT 'app日志全局明细表'
partitioned by (dt string)
stored as parquet
;

-- 上表计算逻辑
-- src：ODS_APP_LOG
-- dst：DWD_APP_GLB_DTL
-- 相对 ODS 来说，主要增加了时间维度信息等维度信息
INSERT INTO TABLE DATASYS.DWD_APP_GLB_DTL PARTITION(dt='2021-10-02')
SELECT
guid,
eventid,
event,
uid,
imei,
mac,
imsi,
osName,
osVer,
androidId,
resolution,
deviceType,
deviceId,
uuid,
appid,
appVer,
release_ch,
promotion_ch,
longtitude,
latitude,
carrier,
netType,
cid_sn,
ip,
sessionId,
`timestamp`,
province,
city,
district,
year(from_unixtime(cast(`timestamp`/1000 as bigint)))  as year,
month(from_unixtime(cast(`timestamp`/1000 as bigint))) as month,
day(from_unixtime(cast(`timestamp`/1000 as bigint))) as day,
from_unixtime(cast(`timestamp`/1000 as bigint),'yyyy-MM-dd') AS datestr
FROM DATASYS.ODS_APP_LOG WHERE dt='2021-10-02'
;

-- 02 DWD层建表：页面描述信息维表创建
USE DATASYS;
CREATE TABLE DATASYS.DIM_PAGE_INFO(
pgid STRING,
channel STRING,
category STRING,
url STRING
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
;

-- 上表导入数据
load data local inpath '/home/data/input/dim/dim_page.txt' into table DATASYS.DIM_PAGE_INFO;
-- 清空表 
-- hive> truncate table dim_page_info;  



--03 DWD层建表：app日志流量事件明细表 DWD_APP_TFC_DTL
-- 相对于 ODS 来说，只包含页面浏览事件数据，增加页面描述相关维度信息，比如 频道，类别。
-- 主题抽取 ，所有 appviewEvent、pgviewEvent 事件
USE DATASYS;
CREATE TABLE DATASYS.DWD_APP_TFC_DTL(
guid bigint COMMENT '用户全局统一标识',
eventid  String COMMENT '事件ID',
event  Map<String, String> COMMENT '事件详情信息',
uid  String COMMENT '用户登录账号的id',
imei  String,
mac  String,
imsi  String,
osName  String,
osVer  String,
androidId  String,
resolution  String,
deviceType  String,
deviceId  String,
uuid  String,
appid  String,
appVer  String,
release_ch  String,
promotion_ch  String,
longtitude  Double,
latitude  Double,
carrier  String,
netType  String,
cid_sn  String,
ip  String,
sessionId  String,
`timestamp`  bigint,
province String,
city String,
district String,
year  string,
month string,
day string,
datestr string,
channel STRING ,  -- 频道
category STRING,  -- 页面类别
url STRING        -- 页面具体地址
)
COMMENT 'APP端埋点日志全局明细表'
partitioned by (dt string)
stored as parquet
;

-- 计算逻辑
-- SRC：app日志全局明细表- DWD_APP_GLB_DTL  和 页面信息维表- DIM_PAGE_INFO
-- DST：app日志流量事件明细表-DWD_APP_TFC_DTL
USE DATASYS;
INSERT INTO TABLE DATASYS.DWD_APP_TFC_DTL PARTITION(dt='2021-10-02')
SELECT 
a.guid,
a.eventid,
a.event,
a.uid,
a.imei,
a.mac,
a.imsi,
a.osName,
a.osVer,
a.androidId,
a.resolution,
a.deviceType,
a.deviceId,
a.uuid,
a.appid,
a.appVer,
a.release_ch,
a.promotion_ch,
a.longtitude,
a.latitude,
a.carrier,
a.netType,
a.cid_sn,
a.ip,
a.sessionId,
a.`timestamp`,
a.province,
a.city,
a.district,
a.year,
a.month,
a.day,
a.datestr,
b.channel,  
b.category,  
b.url
FROM 
(
SELECT
*
FROM DWD_APP_GLB_DTL
WHERE dt='2021-10-02'  and eventid='pgviewEvent'
) a
JOIN DIM_PAGE_INFO  b  ON a.event['pgid'] = b.pgid
;


--04 DWD层建表：app日志交互事件明细表 DWD_APP_ITR_DTL
-- 相对于 ODS 来说，只包含各类交互事件数据，比如 点赞、收藏、分享、评论...
-- 主题抽取 ，所有点赞、收藏、评论、加入购物车 等交互行为事件
USE DATASYS;
CREATE TABLE DATASYS.DWD_APP_ITR_DTL(
guid bigint COMMENT '用户全局统一标识',
eventid  String COMMENT '事件ID',
event  Map<String, String> COMMENT '事件详情信息',
uid  String COMMENT '用户登录账号id',
imei  String,
mac  String,
imsi  String,
osName  String,
osVer  String,
androidId  String,
resolution  String,
deviceType  String,
deviceId  String,
uuid  String,
appid  String,
appVer  String,
release_ch  String,
promotion_ch  String,
longtitude  Double,
latitude  Double,
carrier  String,
netType  String,
cid_sn  String,
ip  String,
sessionId  String,
`timestamp`  bigint,
province String,
city String,
district String,
year  string,
month string,
day string,
datestr string
)
partitioned by (dt string)
stored as parquet
;

-- 计算逻辑
-- SRC： app埋点日志数据全局明细表DWD_APP_GLB_DTL
-- DST： DWD_APP_ITR_DTL
-- 动态分区语法要求设置一个参数： set hive.exec.dynamic.partition.mode=nonstrict; 没有设置输入的时候，页面也会给出来详细的提示。
set hive.exec.dynamic.partition.mode=nonstrict;
INSERT INTO TABLE DATASYS.DWD_APP_ITR_DTL PARTITION(dt)
SELECT * FROM DATASYS.DWD_APP_GLB_DTL WHERE dt='2021-10-02' AND eventid not in ('pgviewEvent','adClickEvent','adShowEvent');


-- 05 DWD层建表：广告描述维表 DIM_AD_INFO
USE DATASYS;
CREATE TABLE DATASYS.DIM_AD_INFO(
adid STRING,    -- 广告ID
medium STRING,  -- 媒体具体形式
campaign STRING -- 广告系列名称
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
;


-- 加载数据
load data local inpath '/home/data/input/dim/dim_ad.txt' into table DATASYS.DIM_AD_INFO;



--06 DWD层建表：app日志广告事件明细表 DWD_APP_ADV_DTL
-- 相对于 ODS 来说，只包含了各类广告事件，增加页面维度信息、广告维度
-- 主题抽取 ，所有ad_show广告曝光、ad_click广告点击事件

USE DATASYS;
CREATE TABLE DATASYS.DWD_APP_ADV_DTL(
guid bigint COMMENT '用户全局统一标识',
eventid  String COMMENT '事件ID',
event  Map<String, String> COMMENT '事件详情信息',
uid  String COMMENT '用户登录账号id',
imei  String,
mac  String,
imsi  String,
osName  String,
osVer  String,
androidId  String,
resolution  String,
deviceType  String,
deviceId  String,
uuid  String,
appid  String,
appVer  String,
release_ch  String,
promotion_ch  String,
longtitude  Double,
latitude  Double,
carrier  String,
netType  String,
cid_sn  String,
ip  String,
sessionId  String,
`timestamp`  bigint,
province String,
city String,
district String,
year  string,
month string,
day string,
datestr string,
channel STRING ,  -- 频道
category STRING,  -- 页面类别
url STRING,       -- 页面地址
medium STRING,    -- 广告媒体形式
campaign STRING   -- 广告系列名称
)
COMMENT 'APP端埋点日志全局明细表'
partitioned by (dt string)
stored as parquet
;


-- 计算逻辑
-- SRC： dwd全局明细表  和  页面信息维表  和  广告信息维表
USE DATASYS;
with a as 
(
SELECT
*
FROM DATASYS.DWD_APP_GLB_DTL
WHERE dt='2021-10-02'  and (eventid='adShowEvent' or eventid='adClickEvent')
)

INSERT INTO TABLE DATASYS.DWD_APP_ADV_DTL PARTITION(dt='2021-10-02')
SELECT
a.guid,
a.eventid,
a.event,
a.uid,
a.imei,
a.mac,
a.imsi,
a.osName,
a.osVer,
a.androidId,
a.resolution,
a.deviceType,
a.deviceId,
a.uuid,
a.appid,
a.appVer,
a.release_ch,
a.promotion_ch,
a.longtitude,
a.latitude,
a.carrier,
a.netType,
a.cid_sn,
a.ip,
a.sessionId,
a.`timestamp`,
a.province,
a.city,
a.district,
a.year,
a.month,
a.day,
a.datestr,
b.channel,  -- 频道
b.category,  -- 页面类别
b.url,       -- 页面地址
c.medium,    -- 广告媒体形式
c.campaign     -- 广告系列名称
FROM a 
JOIN dim_page_info b ON a.event['pgId']=b.pgid 
JOIN dim_ad_info c ON a.event['adId']=c.adid
;

-- 可以做一些查询查看
