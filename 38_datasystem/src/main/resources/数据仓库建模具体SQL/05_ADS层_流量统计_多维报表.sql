--01 建表：流量概况报表  ADS_APP_TFC_OVW
USE DATASYS;
DROP TABLE IF EXISTS ADS_APP_TFC_OVW;
CREATE TABLE ADS_APP_TFC_OVW(
pv_amt          int,          -- pv数
uv_amt          int,          -- uv数
se_amt          int,          -- 会话数
time_avg_se     double,       -- 次均时长
time_avg_u      double,       -- 人均时长
se_avg_u        double,       -- 人均次数
pv_avg_u        double,       -- 人均pv数
rbu_ratio       double,       -- 回头客占比
province        string,
city            string,
district        string,
devicetype      string,
osname          string,
osver           string,
release_ch      string,
promotion_ch    string
)
partitioned by (dt string)
stored as parquet
;



-- 02 计算 组合1： 全局
-- 源表：流量用户聚合表 DWS_APP_TFC_AGG_USER
INSERT INTO TABLE ADS_APP_TFC_OVW PARTITION(dt='2021-10-02')
SELECT
sum(pv_cnts) as pv_amt,
count(distinct guid) as uv_amt,
sum(se_cnts) as se_amt,
sum(ac_time)/sum(se_cnts) as time_avg_se,
sum(ac_time)/count(1)     as time_avg_u,
sum(se_cnts)/count(1)     as se_avg_u,
sum(pv_cnts)/count(1)     as pv_avg_u,
count(if(se_cnts>=2,"ok",null))/count(1)  as rbu_ratio,
null,
null,
null,
null,
null,
null,
null,
null
FROM DWS_APP_TFC_AGG_USER WHERE dt='2021-10-02'
;

-- 03 计算 组合2： 省省份
INSERT INTO TABLE ADS_APP_TFC_OVW PARTITION(dt='2021-10-02')
SELECT
sum(pv_cnts) as pv_amt,
count(distinct guid) as uv_amt,
sum(se_cnts) as se_amt,
sum(ac_time)/sum(se_cnts) as time_avg_se,
sum(ac_time)/count(distinct guid)     as time_avg_u,
sum(se_cnts)/count(distinct guid)     as se_avg_u,
sum(pv_cnts)/count(distinct guid)    as pv_avg_u,
count(if(se_cnts>=2,"ok",null))/count(distinct guid)  as rbu_ratio,
province,
null,
null,
null,
null,
null,
null,
null
FROM DWS_APP_TFC_AGG_USER WHERE dt='2021-10-02'
GROUP BY province
;


-- 04 计算 组合3： 省 + 市
INSERT INTO TABLE ADS_APP_TFC_OVW  PARTITION(dt='2021-10-02')
SELECT
sum(pv_cnts) as pv_amt,
count(distinct guid) as uv_amt,
sum(se_cnts) as se_amt,
sum(ac_time)/sum(se_cnts) as time_avg_se,
sum(ac_time)/count(distinct guid)     as time_avg_u,
sum(se_cnts)/count(distinct guid)     as se_avg_u,
sum(pv_cnts)/count(distinct guid)    as pv_avg_u,
count(if(se_cnts>=2,"ok",null))/count(distinct guid)  as rbu_ratio,
province,
city,
null,
null,
null,
null,
null,
null
FROM DWS_APP_TFC_AGG_USER WHERE dt='2021-10-02'
GROUP BY province,city
;

-- 可以自己查看一下结果