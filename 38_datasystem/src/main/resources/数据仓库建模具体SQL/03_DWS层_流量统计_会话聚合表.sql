-- DWS 建表：流量会话聚合表 DWS_APP_TFC_AGG_SESSION  
-- 一条数据就是一次会话 
-- 日期 uid sessionid 起始时间 结束时间  pv数
USE DATASYS;
DROP TABLE IF EXISTS DWS_APP_TFC_AGG_SESSION;
CREATE TABLE DATASYS.DWS_APP_TFC_AGG_SESSION(
guid           bigint,
sessionid      string,
start_time     bigint,
end_time       bigint,
pv_cnts        int,
province       string,
city           string,
district       string,
devicetype     string,
osname         string,
osver          string,
release_ch     string,
promotion_ch   string
)
PARTITIONED BY (dt string)
STORED AS PARQUET;

-- 计算逻辑
-- 分组聚合！
--		uid ： 分组 key
--		sessionid： 分组 key
--		起始时间： min (commit_time)
--		结束时间 ： max (commit_time)
--		pv 个数： count (1)
-- SRC：PV事件明细表 DWD_APP_TFC_DTL
-- DSR: 流量会话聚合表 DWS_APP_TFC_AGG_SESSION
USE DATASYS;
INSERT INTO TABLE DATASYS.DWS_APP_TFC_AGG_SESSION PARTITION(dt='2021-10-02')
SELECT  
guid,
sessionid,
min(`timestamp`) as start_time,
max(`timestamp`) as end_time,
count(1) as pv_cnts,
province ,
city,
district,
devicetype,
osname,
osver,
release_ch,
promotion_ch
FROM DATASYS.DWD_APP_TFC_DTL WHERE dt='2021-10-02'
GROUP BY guid,sessionid,province,city,district,devicetype,osname,osver,release_ch,promotion_ch
;
