大数据八期第八次课程笔记_Hive


零、回顾
1、hive的概念
2、hive的特点和优缺点
3、hive的架构
	用户接口
	内部组件
	底层支撑
	元数据
4、hive的数据存储
	库
	表
	分区+分桶
	表文件

	分桶的特性：
	1、如果一个key的某个值在某个分桶中，那么所有为该key的值的记录都在这个分桶中。
	2、某些桶中可以有多个该key的值，也可以一个都没有。
5、hive的安装
	重装
	1、保证之前的hive的数据仓库的默认的hdfs目录不存在。
	2、保证元数据库不存在。或者换一个。
	
	基于同一个hadoop集群，想搭建多个不同的hive数据仓库
	1、默认的仓库的路径不一致
	2、元数据库不一致
	
	
一、DDL
官网参考路径：  https://cwiki.apache.org/confluence/display/Hive/LanguageManual+DDL  

DDL： Data Definition Language   数据定义语言
DML:  Data Manipulation Language 数据操作语言
DQL 数据查询语言

（一）针对库 ： 创建库、查看库、删除库、修改库、切换库....
创建库
	hive> create database myhive;
	hive> create database if not exists myhive;
	hive> create database myhive_1 comment "myhive_1 comment" location "/myhive_1";
查看库
	hive> show databases;
	查看当前正在使用的库
	hive> select current_database();
	
删除表
	hive> drop database myhive_1;
	hive> drop database myhive_1 RESTRICT; // 默认的方式，判断若数据库非空不允许删除
	hive> drop database myhive_1 CASCADE;  // 强制删除
修改库 
	基本不常用
切换库
	hive> use myhive;
	OK
	Time taken: 0.015 seconds
	hive> use default;
	OK
	Time taken: 0.014 seconds

	
（二）针对表 ： 创建表、查看表、删除表、修改表、查看表的详细信息....

查看表：
	在当前正在使用的库中查看有哪些表
	hive> show tables;

	在当前的库中查看其它库的表的信息
	hive> show tables in default;

	使用正则
	hive> show tables "stu*";
	hive> show tables "stu";

	查看表的详细的信息
	hive> desc stu; 
	hive> desc extended stu;
	hive> desc formatted stu; //格式化输出

创建表
	1、创建内部表
	create table student(id int, name string, sex string, age int, department string) row format delimited fields terminated by ",";
	内部表： 类型是  MANAGED_TABLE
	show create table xxx;      // 查看建表语句
		
	2、创建外部表
	内部表和外部表的区别：
		删除表的时候，内部表会删除描述数据和真实数据，外部表只会删除描述数据。
	内部表和外部表选择问题：
		（1）如果数据存储在hdfs上，然后要使用hive去分析，并且这份数据还有可能被其他的计算引擎使用到，使用外部表。
		（2）如果这份数据仅仅是供hive做数据分析使用，可以使用内部表。
	
	// 没有指定外部路径, 表的数据目录存储在默认的仓库路径中
	create external table student_ext_1(id int, name string, sex string, age int, department string) row format delimited fields terminated by ",";

	// 指定一个不存在的外部路径: 创建表的时候，会自动给你创建表目录
	create external table student_ext_2(id int, name string, sex string, age int, department string) row format delimited fields terminated by "," location "/student_ext_2";

	// 指定一个已经存在的目录: 并且有数据
	//在linux中执行
	hadoop fs -mkdir -p /student_ext_3
	hadoop fs -put /home/data/student.txt /student_ext_3
	//在hive命令行中执行
	create external table student_ext_3(id int, name string, sex string, age int, department string) row format delimited fields terminated by "," location "/student_ext_3";

	
	3、创建分区表
	// 创建只有一个分区字段的分区表：
	create table student_ptn(id int, name string, sex string, age int, department string) partitioned by (city string comment "partitioned field") row format delimited fields terminated by ",";

	load data local inpath "/home/data/student.txt" into table student_ptn;  XXXXXXX
	分区字段是一个虚拟列
	

	// 把数据导入到一个不存在的分区，它会自动创建该分区
	load data local inpath "/home/data/student.txt" into table student_ptn partition(city="beijing");  √√√√

	// 把数据导入到一个已经存在的分区
	alter table student_ptn add partition (city="chongqing");
	load data local inpath "/home/data/student.txt" into table student_ptn partition(city="chongqing");


	// 创建有多个分区字段的分区表：
	create table student_ptn_date(id int, name string, sex string, age int, department string)
	partitioned by (city string comment "partitioned field", dt string)
	row format delimited fields terminated by ",";
	
	两个分区city、dt，请问一下city和dt两个分区是递进关系还是并列关系
	递进关系，在评论区扣1
	并列关系，在评论区扣2
	答案：city与dt是递进关系

	// 往分区中导入数据:
	load data local inpath "/home/data/student.txt" into table student_ptn_date partition(city="beijing");  //报错

	load data local inpath "/home/data/student.txt" into table student_ptn_date partition(city="beijing", dt='2012-12-12');    //正确

	// 不能在导入数据的时候指定多个分区定义，但是是允许一个分区多个列的，不要混淆了
	load data local inpath "/home/data/student.txt" into table student_ptn_date partition(city="beijing", dt='2012-12-14') partition (city="beijing" , dt='2012-12-13');   XXXXXX

	// 添加分区
	alter table student_ptn_date add partition(city="beijing", dt='2012-12-14') partition (city="beijing" , dt='2012-12-13');    √√√√√√√√
	alter table student_ptn_date add partition(city="chongqing", dt='2012-12-14') partition (city="chongqing" , dt='2012-12-13'); 

	// 查询一个分区表有那些分区
	show partitions student_ptn;
	show partitions student_ptn_date;
	show partitions student;

	
	4、创建分桶表
	分桶字段必须是表字段中的任意的一个或者多个
	// 创建一个分桶表
	create table student_bucket (id int, name string, sex string, age int, department string) clustered by (department) sorted by (age desc, id asc) into 3 buckets row format delimited fields terminated by ",";

	clustered by department [sorted by age desc,id asc] into 3 buckets;
	desc formatted table 0表示降序，1表示升序

	分桶表不能通过load文件的方式导入数据
	
	5、从查询语句的结果来创建新表
	通过下面的命令：
	create table ... as  select ....
	查询例子：
		select department, count(*) as total from student group by department;
	完整的CTAS语句：
		create table dpt_count as select department, count(*) as total from student group by department;
		
	6、通过like复制已有表的结构创建新表
	create table student_like like student;
	
	
	id	info				city
	1	"zhangsan",22,AA	beijing
	2	22,33,44			shanghai

	22,33,44 : array<int>
	"zhangsan",22,AA : struct<name:string,age:int,department:string>
	

删除表  
	hive> drop table student_ext_3;
	OK
	Time taken: 0.11 seconds
	hive> drop table if exists student_ext_3;
	OK
	Time taken: 0.012 seconds

清空表
	truncate table student;
	等价于下面的命令：
	hadoop fs -rm -r /user/hive/warehouse2/myhive.db/student/*

	hive> truncate table student_ext_1;
	FAILED: SemanticException [Error 10146]: Cannot truncate non-managed table student_ext_1.
	不能truncate非管理表，但是可以drop非管理表。
	这是因为需要遵守删除外部表仅删除表的元数据而保留真实数据的这一准则。
	
修改表 
	
	1、修改表名
	alter table student rename to studentss;

	2、修改字段

		添加字段： 
		alter table student add columns (city string, dt string);
		删除字段： 
		alter table student drop columns (city);
		替换字段：
		alter table student replace columns (id int, name string, sex string, age int);
		改变列的定义：
		alter table student change id newid string comment "new id";
		改变列的顺序：
		alter table student change sex sex string first;
		alter table student change name name string after sex;

	3、修改分区

		添加分区：
		alter table student_ptn add partition(city='tiajin') partition(city='shanghai');

		删除分区：
		alter table student_ptn drop partition(city='tiajin');  
		alter table student_ptn drop partition(city='tiajin'),partition(city='shanghai'); 

		修改分区的数据目录：
		alter table student_ptn partition(city="beijing") set location "/stu_beijing";   XXXX  1.x中不ok，需要写全路径
		alter table student_ptn partition(city="beijing") set location "/stu_beijing";	ok  2.3.8版本ok
		

二、DML

DDL： Data Definition Language   数据定义语言
DML:  Data Manipulation Language 数据操作语言
DQL 数据查询语言


1、导入数据
	load data local inpath "/home/data/student.txt" into table student;     //本地磁盘复制到hdfs上
	load data inpath "/home/data/student.txt" into table student;   //hdfs的文件之间的移动
	
	
	创建分区表：
	create table student_ptn (id int, name string, sex string, age int)
	partitioned by (department string)
	row format delimited fields terminated by ",";


	单重插入：
    insert into table student_ptn partition (department = 'IS') select id,sex,name,age from student where department  = 'IS';
    insert into table student_ptn partition (department = 'CS') select id,sex,name,age from student where department  = 'CS';
    insert into table student_ptn partition (department = 'MA') select id,sex,name,age from student where department  = 'MA';


	多重插入：
    from student
    insert into table student_ptn partition (department = 'IS') select id,sex,name,age where department = 'IS'
    insert into table student_ptn partition (department = 'CS') select id,sex,name,age where department = 'CS'
    insert into table student_ptn partition (department = 'MA') select id,sex,name,age where department = 'MA';


2、导出数据
	insert overwrite local directory "/home/data/cs_student" select * from student where department = 'CS';
	insert overwrite directory "/home/data/cs_student" select * from student where department = 'CS';

    单模式导出数据到本地：
    insert overwrite local directory '/root/outputdata' select id,name,sex,age,department from mingxing;

    多模式导出数据到本地：
    from mingxing
    insert overwrite local directory '/root/outputdata1' select id, name
    insert overwrite local directory '/root/outputdata2' select id, name,age


三、文件格式
参考博客：https://zhuanlan.zhihu.com/p/103740807
Hive主要有四种文件格式：
    行式存储
        SEQUENCEFILE    //二进制，（比源文件格式占用磁盘更多，生产上基本不用）支持三种压缩格式：none、record、block
        TEXTFILE        //纯文本格式，可通过文件直接导入数据然后作为其他文件格式表的跳板，可结合Gzip、Bzip2等压缩方式使用
    列式存储
        ORC             //生产中最常用，默认的压缩格式是zlib
        PARQUET 二进制  //压缩比没有ORC大，支持Impala，默认压缩格式是Snappy

    ORC和RCFile
        ORC是RCFile的升级版，RCFile属于行列混合存储，生产用的很少，压缩比也不大。
    生产上多常用列式存储，TEXTFILE用的也不少。
    TEXTFILE是Hive的默认文件格式，若要修改的话可通过set hive.default.fileformat配置。
    压缩格式来说，公认的Snappy压缩综合性能最佳，生产上较常见。
        create table log_orc_snappy(
        track_time string,
        url string,
        session_id string,
        referer string,
        ip string,
        end_user_id string,
        city_id string
        )
        ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
        STORED AS orc tblproperties ("orc.compress"="SNAPPY");


四、事务表的创建
事务表在生产上几乎不用，创建事务表也需要满足诸多要求：
    1、必须为内部表
    2、必须是分桶表
    3、文件格式必须是ORC
    4、开启事务配置：
        a、命令执行方式：
            set hive.support.concurrency=true;
            set hive.enforce.bucketing=true;
            set hive.exec.dynamic.partition.mode=nonstrict;
            set hive.txn.manager=org.apache.hadoop.hive.ql.lockmgr.DbTxnManager;
            set hive.compactor.initiator.on=true;
            set hive.compactor.worker.threads=1;
        b、JDBC访问url方式：
            url='jdbc:hive2://hnedadn118:10000/default;principal=hive/hnedadn118@NBDP.COM?hive.support.concurrency=true;hive.enforce.bucketing=true;hive.exec.dynamic.partition.mode=nonstrict;hive.txn.manager=org.apache.hadoop.hive.ql.lockmgr.DbTxnManager;hive.compactor.initiator.on=true;hive.compactor.worker.threads=1'
        c、修改hive-site.xml（hiveserver2需要重启）
创建事务表DDL语句：
    // Hive执行ACID增删改的建表语句
    create table if not exists testToHive3 (
      money String,
      orderTime bigint,
      packCode String,
      packContent String,
      packId String,
      packLevel String,
      packName String,
      userCode String,
      table String,
      op_type String
    )
    partitioned by (column1 String)
    clustered by (packId) into 2 buckets
    row format delimited fields terminated by '\t'
    stored as orc TBLPROPERTIES('transactional'='true');


五、补充
【Hive1.2.1参数调优】
hive并行度调优：
set hive.exec.parallel=true;
set hive.exec.parallel.thread.number=16;

hive job本地模式自动运行（更适合在控制台，jdbc代码开发不可行）：
set hive.exec.mode.local.auto=true;
https://www.cnblogs.com/frankdeng/p/9463897.html

hive job强制设置为本地模式（代码jdbc开发可行）：
set mapreduce.framework.name=local;
本地模式设置后（相当于不走MapReduce），原来插入一条语句平均是五六十秒，现在是10秒左右，建分区表性能应该更佳，并且可考虑假批量插入。

【Hive3.0+tez+llap】
前提是在HDP环境下，一条DML语句基本上执行2秒左右





