package com.nx.sort;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/8/24 11:47
 */
public class MyPartitioner extends Partitioner<FlowBean, Text> {
    @Override
    public int getPartition(FlowBean key, Text value, int numReduceTasks) {
//        System.out.println("--------------------------------");
//        System.out.println((key.getUpFlow().hashCode() & 2147483647) % numReduceTasks);
        return (key.getUpFlow().hashCode() & 2147483647) % numReduceTasks;
    }
}
