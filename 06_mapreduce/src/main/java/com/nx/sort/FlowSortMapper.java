package com.nx.sort;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @author LIAO
 * create  2020-09-12 22:47
 * Mapper<KEYIN, VALUEIN, KEYOUT, VALUEOUT>
 *     KEYIN:偏移量
 *     VALUEIN：文本：13480253104	41580	41580	2494800	2494800
 *     KEYOUT：FlowBean
 *     VALUEOUT：手机号
 */
public class FlowSortMapper extends Mapper<LongWritable,Text,FlowBean,Text> {
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        //1、拆分 一行文本  13480253104	41580	41580	2494800	2494800
        String[] split = value.toString().split("\t");

        //2、写入到实体类
        FlowBean flowBean = new FlowBean();
        flowBean.setUpFlow(Integer.parseInt(split[1]));
        flowBean.setDownFlow(Integer.parseInt(split[2]));
        flowBean.setUpCountFlow(Integer.parseInt(split[3]));
        flowBean.setDownCountFlow(Integer.parseInt(split[4]));

        String phoneNum = split[0];

        //3、写入到上下文
        context.write(flowBean,new Text(phoneNum));
    }
}
