package com.nx.sort;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import java.io.IOException;

/**
 * @author LIAO
 * create  2020-09-12 22:56
 * 需求二：将需求一中结果按照upFlow流量倒排
 */
public class JobMain {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration configuration = new Configuration();
        //一、初始化一个Job
        Job job = Job.getInstance(configuration, "flowsort");

        //二、设置Job的相关信息，配置   8个小步骤
        //1、设置输入的路径，让程序找到源数据的位置
        job.setInputFormatClass(TextInputFormat.class);
        //设置成本地的输入路径
        TextInputFormat.addInputPath(job,new Path("06_mapreduce/src/main/document/input/flowsort"));

        //2、设置mapper类，并设置k2 v2
        job.setMapperClass(FlowSortMapper.class);
        job.setMapOutputKeyClass(FlowBean.class);
        job.setMapOutputValueClass(Text.class);

        // 3 4 5 6 四个步骤，shuffle阶段，使用默认的配置就可以了
        job.setPartitionerClass(MyPartitioner.class);
        job.setNumReduceTasks(3);

        //7、设置reducer类，并设置k3 v3的类型
        job.setReducerClass(FlowSortReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(FlowBean.class);

        //8、设置输出的路径
        job.setOutputFormatClass(TextOutputFormat.class);
        //设置成本地目的地路径，下面的代码可以通过。
        TextOutputFormat.setOutputPath(job,new Path("06_mapreduce/src/main/document/output/flowsort"));

        //三、等待完成
        boolean b = job.waitForCompletion(true);
        System.out.println(b);
        System.exit(b ? 0:1);
    }
}
