package com.nx.sort;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * @author LIAO
 * create  2020-09-12 22:53
 * Reducer<KEYIN,VALUEIN,KEYOUT,VALUEOUT>
 */
public class FlowSortReducer extends Reducer<FlowBean,Text,Text,FlowBean> {
    @Override
    protected void reduce(FlowBean key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        //1、遍历集合，将k3 v3 写入到上下文
        for (Text value : values) {
//            System.out.println(key + "\t" + value);
            context.write(value, key);
        }
    }
}
