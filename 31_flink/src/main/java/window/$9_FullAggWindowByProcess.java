package window;

import org.apache.flink.streaming.api.datastream.AllWindowedStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.ProcessAllWindowFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.util.Iterator;

/**
 * 全量聚合： process算子
 *
 * 实现累加求和的效果 sum
 */
public class $9_FullAggWindowByProcess {
    public static void main(String[] args) throws  Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();


        DataStreamSource<String> dataStream = env.socketTextStream("localhost", 1234);

        SingleOutputStreamOperator<Integer> intDStream = dataStream.map(number -> Integer.valueOf(number));

        AllWindowedStream<Integer, TimeWindow> windowResult = intDStream.timeWindowAll(Time.seconds(5));

        /**
         * IN       输入类型
         * OUT      输出类型
         * WINDOW   窗口类型
         */
        windowResult.process(new ProcessAllWindowFunction<Integer, Integer, TimeWindow>() {
            @Override
            public void process(Context context, Iterable<Integer> iterable,
                                Collector<Integer> out) throws Exception {
                System.out.println("执行计算逻辑...（每个窗口只输出一个）");
                int count = 0;
                Iterator<Integer> numberIterator = iterable.iterator();
                while (numberIterator.hasNext()){
                    Integer number = numberIterator.next();
                    count += number;
                }
                out.collect(count);
            }
        }).print();


        /**
         * apply 实现的 windowFunction 也能获取到 window 信息，而 process 实现的方法能获取到更多的上下文信息。
         * 除此之外，两者无其他区别。
         */
//        windowResult.apply(new AllWindowFunction<Integer, Integer, TimeWindow>() {
//            @Override
//            public void apply(TimeWindow timeWindow, Iterable<Integer> iterable, Collector<Integer> out) throws Exception {
//
//            }
//        });

        env.execute("FullAgg Window By Process");
    }
}
