package window;

import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks;
import org.apache.flink.streaming.api.functions.co.ProcessJoinFunction;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.util.Collector;

import javax.annotation.Nullable;

/**
 * 输出二维坐标系上属于 y=x 的点
 * Interval Join 只支持事件时间语义。
 * 且不需要 watermark 来作为触发条件，只要在时间间隔内的，来一条数据就join一条并输出。
 */
public class $10_IntervalJoinTest {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        // 阿这...那就整嘛！
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);


        SingleOutputStreamOperator<String> orangeStream = env.socketTextStream("localhost", 1234)
                .assignTimestampsAndWatermarks(new EventTimeExtractor());

        SingleOutputStreamOperator<String> greenStream = env.socketTextStream("localhost", 5678)
                .assignTimestampsAndWatermarks(new EventTimeExtractor());


        orangeStream
                .keyBy(s1 -> s1)
                .intervalJoin(greenStream.keyBy(s2 -> s2))
                .between(Time.seconds(-3), Time.seconds(2))
                .process(new ProcessJoinFunction<String, String, String>() {
                    @Override
                    public void processElement(String s1, String s2, Context ctx, Collector<String> out) throws Exception {
                        out.collect("(" + s1 + "," + s2 + ")");     // 以坐标的形式输出
                    }
                })
                .print();


        env.execute("Interval Join Test");
    }


    public static class EventTimeExtractor implements AssignerWithPeriodicWatermarks<String> {
        @Nullable
        @Override
        public Watermark getCurrentWatermark() {
//            return new Watermark(System.currentTimeMillis());
            return null;
        }

        @Override
        public long extractTimestamp(String element, long preTimeStamp) {
            return System.currentTimeMillis();
        }
    }
}
