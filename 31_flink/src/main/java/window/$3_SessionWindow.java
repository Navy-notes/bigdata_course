package window;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.assigners.ProcessingTimeSessionWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.util.Collector;

/**
 * 会话时间窗口：5秒过去以后，该单词不出现就打印出来该单词
 */
public class $3_SessionWindow {

    public static void main(String[] args) throws  Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        DataStreamSource<String> dataStream = env.socketTextStream("127.0.0.1", 1234);

        SingleOutputStreamOperator<Tuple2<String, Integer>> stream = dataStream.flatMap(new FlatMapFunction<String, Tuple2<String, Integer>>() {
            @Override
            public void flatMap(String line, Collector<Tuple2<String, Integer>> collector) throws Exception {
                String[] fields = line.split(",");
                for (String word : fields) {
                    collector.collect(Tuple2.of(word, 1));
                }
            }
        });



        stream.keyBy(0)
               // 会话窗口
                .window(ProcessingTimeSessionWindows.withGap(Time.seconds(5)))
                // 滚动窗口，3秒运行一次
               // .window(TumblingProcessingTimeWindows.of(Time.seconds(3)))
                //.timeWindow(Time.seconds(3))
                // 滑动窗口，3s  1s单词次数
                //.window(SlidingProcessingTimeWindows.of(Time.seconds(3),Time.seconds(1)))
               //.timeWindow(Time.seconds(3), Time.seconds(1))
                .sum(1)
                .print();



        env.execute("SessionWindowTest");
    }
}
