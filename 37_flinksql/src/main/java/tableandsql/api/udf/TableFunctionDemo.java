package tableandsql.api.udf;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.java.StreamTableEnvironment;
import org.apache.flink.table.functions.TableFunction;
import org.apache.flink.types.Row;
import tableandsql.beans.SensorReading;

/**
 * 一对多   自定义表值函数，相当于输出一个表
 */
public class TableFunctionDemo {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        SingleOutputStreamOperator<SensorReading> dataStream = env.readTextFile("F:\\IDEAProjects\\bigdata_course\\37_flinksql\\src\\main\\resources\\sensor.txt")
                .map(line -> {
                    String[] fields = line.split(",");
                    return new SensorReading(fields[0], new Long(fields[1]), new Float(fields[2]));
                });

        // 流转表，这里别名必须显式用as
        Table sensorTable = tableEnv.fromDataStream(dataStream, "id, timestamp as ts, temperature as temp");

        // 注册自定义表函数
        tableEnv.registerFunction("split", new Split("_"));

        // Table API
        Table resultTable = sensorTable.joinLateral("id.split as (word, length)")
                .select("id, ts, word, length");

        // SQL
        tableEnv.createTemporaryView("sensor", sensorTable);
        Table sqlResultTable = tableEnv.sqlQuery("select id,ts,word,length from sensor, lateral table(split(id)) tmp(word, length)");   // 这里的tmp(word, length)前省略了as，相当于是lateral join的表的别名


        // 打印输出
        tableEnv.toAppendStream(resultTable, Row.class).print("tableapi");
        tableEnv.toAppendStream(sqlResultTable, Row.class).print("sql");

        env.execute("TableFunctionDemo");
    }


    /**
     * OUT  输出的数据类型
     */
    // 实现自定义表函数
    public static class Split extends TableFunction<Tuple2<String, Integer>> {
        // 分隔符
        private String separator;

        public Split(String separator) {
            this.separator = separator;
        }

        public void eval(String column) {
            for (String s : column.split(separator)) {
                collect(new Tuple2<>(s, s.length()));
            }
        }
    }
}
