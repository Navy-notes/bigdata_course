package tableandsql.api.udf;


import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.java.StreamTableEnvironment;
import org.apache.flink.table.functions.ScalarFunction;
import org.apache.flink.types.Row;
import tableandsql.beans.SensorReading;

/**
 * 一对一  自定义标量函数
 */
public class ScalarFunctionDemo {
    public static void main(String[] args) throws Exception {
        // 创建环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);
        env.setParallelism(1);

        // 读取数据
        DataStreamSource<String> source = env.readTextFile("F:\\IDEAProjects\\bigdata_course\\37_flinksql\\src\\main\\resources\\sensor.txt");

        // 转换成SensorReading数据流
        SingleOutputStreamOperator<SensorReading> dataStream = source.map(line -> {
            String[] fields = line.split(",");
            return new SensorReading(fields[0], Long.valueOf(fields[1]), Float.valueOf(fields[2]));
        });

        // 流转换为表
        Table table = tableEnv.fromDataStream(dataStream, "id, timestamp as ts");

        // 在表环境中注册UDF
        tableEnv.registerFunction("hashCode", new HashCode(13));

        // Table API
        Table resultTable = table.select("id, ts, id.hashCode");

        // SQL操作
        tableEnv.createTemporaryView("sensor", table);
        Table sqlResultTable = tableEnv.sqlQuery("select id, ts, hashCode(id) from sensor");


        // 分别打印两种方式的执行结果
        tableEnv.toAppendStream(resultTable, Row.class).print("tableapi");
        tableEnv.toAppendStream(sqlResultTable, Row.class).print("sql");


        env.execute("ScalarFunctionDemo");
    }



    // 实现自定义的ScalarFunction
    public static class HashCode extends ScalarFunction {
        private int factor = 1;

        public HashCode() {
        }

        public HashCode(int factor) {
            this.factor = factor;
        }

        public int eval(String id) {
            return id.hashCode() * factor;
        }
    }
}
