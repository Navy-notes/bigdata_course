package tableandsql.api.udf;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.java.StreamTableEnvironment;
import org.apache.flink.table.functions.AggregateFunction;
import org.apache.flink.types.Row;
import tableandsql.beans.SensorReading;

/**
 * 多对一   自定义聚合函数
 */
public class AggregateFunctionDemo {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        SingleOutputStreamOperator<SensorReading> dataStream = env.readTextFile("F:\\IDEAProjects\\bigdata_course\\37_flinksql\\src\\main\\resources\\sensor.txt")
                .map(line -> {
                    String[] fields = line.split(",");
                    return new SensorReading(fields[0], new Long(fields[1]), new Float(fields[2]));
                });

        // 流转表，这里别名必须显式用as
        Table sensorTable = tableEnv.fromDataStream(dataStream, "id, timestamp as ts, temperature as temp");

        // 注册UDAF
        tableEnv.registerFunction("avgTemp", new AvgTemp());

        // Table API
        Table resultTable = sensorTable.groupBy("id")
                .aggregate("temp.avgTemp as avgTemp")
                .select("id, avgTemp");

        // SQL
        tableEnv.createTemporaryView("sensor", sensorTable);
        Table sqlResultTable = tableEnv.sqlQuery("select id, avgTemp(temp) from sensor group by id");

        // 动态表转实时流  撤回模式
        tableEnv.toRetractStream(resultTable, Row.class).print("tableapi");
        tableEnv.toRetractStream(sqlResultTable, Row.class).print("sql");


        env.execute("AggregateFunctionDemo");
    }


    /**
     * OUT  输出的数据类型
     * ACC  累加器的数据类型
     */
    public static class AvgTemp extends AggregateFunction<Float, Tuple2<Float, Integer>> {

        @Override
        public Float getValue(Tuple2<Float, Integer> accumulator) {
            return accumulator.f0 / accumulator.f1;
        }

        @Override
        public Tuple2<Float, Integer> createAccumulator() {
            return new Tuple2<>(0.0F, 0);
        }

        // 固定实现的方法名
        public void accumulate(Tuple2<Float, Integer> accumulator, Float temp) {
            accumulator.f0 += temp;
            accumulator.f1 += 1;
        }

        // 多并行度下需要实现merge方法....
    }
}
