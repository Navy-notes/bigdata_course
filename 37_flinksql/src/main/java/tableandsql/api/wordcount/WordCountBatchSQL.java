package tableandsql.api.wordcount;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.DataSource;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.java.BatchTableEnvironment;
import org.apache.flink.util.Collector;
import tableandsql.beans.WordCount;

/**
 * 使用SQL的方式实现批环境下的单词计数
 */
public class WordCountBatchSQL {
    public static void main(String[] args) throws Exception {
        // 创建批处理执行环境
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        // 创建Table执行环境。这里的tEnv可以等同于spark的ssc
        BatchTableEnvironment tEnv = BatchTableEnvironment.create(env);

        // 接受原始数据
        DataSource<String> dataSource = env.fromElements("I love China", "I love Beijing", "Beijing is the capital of China");

        // 数据预处理得到 input DataSet
        DataSet<WordCount> input = dataSource.flatMap((String line, Collector<WordCount> out) -> {
            String[] words = line.split(" ");
            for (String word : words) {
                out.collect(new WordCount(word, 1));
            }
        }).returns(WordCount.class);


        /**
         * 方法一：注册DataSet生成表。
         *  参数1：表名
         *  参数2：数据集
         *  参数3：列名
         */
//        tEnv.registerDataSet("word_count", input, "word, frequency");

        /**
         * 方法二：先将DataSet转换成表，然后再创建临时视图。
         *  参数1：视图名
         *  参数2：Table对象
         *
         */
        Table table = tEnv.fromDataSet(input);
        tEnv.createTemporaryView("word_count", table);


        // 执行sql逻辑
        Table result = tEnv.sqlQuery("select word, sum(frequency) frequency from word_count group by word ");


        // 将 sql 的查询结果转换成 DataSet 打印输出
        tEnv.toDataSet(result, WordCount.class).print();

    }
}
