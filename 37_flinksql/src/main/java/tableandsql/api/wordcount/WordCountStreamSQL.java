package tableandsql.api.wordcount;

import org.apache.flink.api.scala.typeutils.Types;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.java.StreamTableEnvironment;
import org.apache.flink.util.Collector;
import tableandsql.beans.WordCount;

/**
 * 使用 SQL 实现流环境下的单词计数
 */
public class WordCountStreamSQL {
    public static void main(String[] args) throws Exception {
        // 创建流执行环境
        StreamExecutionEnvironment sEnv = StreamExecutionEnvironment.getExecutionEnvironment();
        // 创建Table的流执行环境
        StreamTableEnvironment stEnv = StreamTableEnvironment.create(sEnv);

        // 实时接受流式数据
        DataStreamSource<String> source = sEnv.socketTextStream("localhost", 1234);

        // 数据预处理
        SingleOutputStreamOperator<WordCount> input = source.flatMap((String line, Collector<WordCount> out) -> {
            String[] words = line.split(" ");
            for (String word : words) {
                out.collect(new WordCount(word, 1));
            }
        }).returns(Types.POJO(WordCount.class));

        /**
         * 方式一：在表环境中注册实时流
         */
//        stEnv.registerDataStream("word_count", input);
//        Table table = stEnv.sqlQuery("select word, sum(frequency) frequency from word_count group by word");

        /**
         * 方式二：将实时流转换为表环境中的临时视图
         *  参数1： 视图名
         *  参数2： stream对象
         *  参数3： columnNames，默认不指定列名就使用 POJO 的所有fields
         */
//        stEnv.createTemporaryView("word_count", input, "word, frequency");
//        // 执行sql逻辑
//        Table table = stEnv.sqlQuery("select word, sum(frequency) frequency from word_count group by word");


        /**
         * 方式三：先将实时流 -> 动态表，然后直接使用表对象作为参数执行sql语句，持续查询
         */
        Table wordCount = stEnv.fromDataStream(input);
        Table table = stEnv.sqlQuery("select word, sum(frequency) frequency from " + wordCount + " group by word");


        // 动态表 -> 实时流， 输出
        stEnv.toRetractStream(table, WordCount.class).print();

        sEnv.execute("WordCountStreamSQL");
    }
}
