package tableandsql.api.wordcount;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.scala.typeutils.Types;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.java.StreamTableEnvironment;
import org.apache.flink.util.Collector;
import tableandsql.beans.WordCount;

/**
 * 使用 Table API 实现流环境下的单词计数
 */
public class WordCountStreamTableAPI {
    public static void main(String[] args) throws Exception {
        // 创建流执行环境
        StreamExecutionEnvironment sEnv = StreamExecutionEnvironment.getExecutionEnvironment();
        // 创建Table的流执行环境
        StreamTableEnvironment stEnv = StreamTableEnvironment.create(sEnv);

        // 实时接受流式数据
        DataStreamSource<String> source = sEnv.socketTextStream("localhost", 1234);

        // 数据预处理
        SingleOutputStreamOperator<WordCount> input = source.flatMap((String line, Collector<WordCount> out) -> {
            String[] words = line.split(" ");
            for (String word : words) {
                out.collect(new WordCount(word, 1));
            }
        }).returns(Types.POJO(WordCount.class));

        // 实时流 -> 动态表
        Table table = stEnv.fromDataStream(input, "word, frequency");

        // 动态表进行持续查询
        Table result = table.groupBy("word").select("word, frequency.sum as frequency");

        // 动态表 -> 实时流
        // flink支持两种转换模式的流：追加流和回撤流。对于有聚合操作的计算只能使用撤回模式。
        DataStream<Tuple2<Boolean, WordCount>> stream = stEnv.toRetractStream(result, WordCount.class);


        // 流输出
        stream.print();

        sEnv.execute("WordCountStreamTableAPI");
    }
}
