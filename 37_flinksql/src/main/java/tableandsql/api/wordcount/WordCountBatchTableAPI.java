package tableandsql.api.wordcount;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.DataSource;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.java.BatchTableEnvironment;
import org.apache.flink.util.Collector;
import tableandsql.beans.WordCount;

/**
 * 使用 Table API 实现批环境下的单词计数
 */
public class WordCountBatchTableAPI {
    public static void main(String[] args) throws Exception {
        // 创建批处理执行环境
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        // 接受原始数据
        DataSource<String> dataSource = env.fromElements("I love China", "I love Beijing", "Beijing is the capital of China");

        // 数据预处理得到 input DataSet
        DataSet<WordCount> input = dataSource.flatMap((String line, Collector<WordCount> out) -> {
            String[] words = line.split(" ");
            for (String word : words) {
                out.collect(new WordCount(word, 1));
            }
        }).returns(WordCount.class);


        // 创建Table的执行环境
        BatchTableEnvironment tEnv = BatchTableEnvironment.create(env);

        // 将DataSet转换成Table，如果不显式声明表的列名则默认使用 POJO 的所有fields作为字段名
        Table table = tEnv.fromDataSet(input);

        // 执行计算逻辑。这里并不完成等同于sql语句，起别名 as 不能省略
        Table result = table.groupBy("word").select("word, frequency.sum as frequency");


        // Table转成DataSet并打印输出
        tEnv.toDataSet(result, WordCount.class).print();


        // 流处理环境才需要用到下面这一步，批处理不需要如此启动执行。
//        env.execute("WordCountBatchTableAPI");
    }
}
