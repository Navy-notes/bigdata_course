package tableandsql.api.inoutput;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.DataTypes;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.java.StreamTableEnvironment;
import org.apache.flink.table.descriptors.Csv;
import org.apache.flink.table.descriptors.FileSystem;
import org.apache.flink.table.descriptors.Schema;

/**
 * 使用SQL/Table API直接获取数据源file source和file sink
 */
public class FilePipeLine {
    public static void main(String[] args) throws Exception {
        // 创建流环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        // 创建表环境
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        env.setParallelism(1);


        // 创建inputTable，从文件获取输入数据
        String inputPath = "F:\\IDEAProjects\\bigdata_course\\37_flinksql\\src\\main\\resources\\sensor.txt";
        tableEnv.connect(new FileSystem().path(inputPath))
                .withFormat(new Csv())
                .withSchema(new Schema()
                        .field("id", DataTypes.STRING())
                        .field("timestamp", DataTypes.BIGINT())
                        .field("temperature", DataTypes.FLOAT()))
                .createTemporaryTable("inputTable");


        // 创建outputTable，将输出数据写入文件
        String outputPath = "F:\\IDEAProjects\\bigdata_course\\37_flinksql\\src\\main\\resources\\out.txt";
        tableEnv.connect(new FileSystem().path(outputPath))
                .withFormat(new Csv())
                .withSchema(new Schema()
                        .field("id", DataTypes.STRING())
                        .field("temp", DataTypes.FLOAT()))
                .createTemporaryTable("outputTable");


        // sql操作，方式一 （推荐）
//        String sql = "insert into outputTable select id, temperature from inputTable where id = 'sensor_6'";
//        tableEnv.sqlUpdate(sql);

        // sql操作，方式二
//        Table sqlResultTable = tableEnv.sqlQuery("select id, temperature from inputTable where id = 'sensor_6'");
//        sqlResultTable.insertInto("outputTable");

        // table api操作
        Table inputTable = tableEnv.from("inputTable");
        Table resultTable = inputTable.filter("id === 'sensor_6'").select("id, temperature");
        resultTable.insertInto("outputTable");


        env.execute("FilePipeLine With SQL/Table API");
    }
}
