package tableandsql.api.inoutput;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.DataTypes;
import org.apache.flink.table.api.java.StreamTableEnvironment;
import org.apache.flink.table.descriptors.Csv;
import org.apache.flink.table.descriptors.Kafka;
import org.apache.flink.table.descriptors.Schema;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/11/11 16:44
 */
public class KafkaPipeLine {
    public static void main(String[] args) throws Exception {
        // 创建环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);


        // 连接kafka的sensor主题，创建来源表（使用DDL的方式）
        tableEnv.sqlUpdate("CREATE TABLE inputTable(\n" +
                "    id  STRING,\n" +
                "    ts  BIGINT,\n" +
                "    temperature  FLOAT\n" +
                ") WITH (\n" +
                "    'connector.type' = 'kafka',       \n" +
                "    'connector.version' = '0.10',\n" +
                "    'connector.topic' = 'sensor',\n" +
                "    'connector.properties.zookeeper.connect' = 'localhost:2181',\n" +
                "    'connector.properties.bootstrap.servers' = 'localhost:9092',\n" +
                "    'connector.properties.group.id' = 'testGroup1',\n" +
                "    'connector.startup-mode' = 'latest-offset',\n" +
                "    'format.type' = 'csv'\n" +
                ")");
//        tableEnv.connect(new Kafka()
//                .version("0.10")
//                .topic("sensor")
//                .property("group.id", "testGroup1")
//                .property("bootstrap.servers", "localhost:9092")
//                .property("zookeeper.connect", "localhost:2181")
//        )
//                .withFormat(new Csv())
//                .withSchema(new Schema()
//                        .field("id", DataTypes.STRING())
//                        .field("timestamp", DataTypes.BIGINT())
//                        .field("temperature", DataTypes.FLOAT())
//                )
//                .createTemporaryTable("inputTable");


        // 连接kafa的sinktest主题，创建目标表（使用Java的API）
        tableEnv.connect(new Kafka()
                .version("0.10")
                .topic("sinktest")
                .property("bootstrap.servers", "localhost:9092")
                .property("zookeeper.connect", "localhost:2181")
        )
                .withFormat(new Csv())
                .withSchema(new Schema()
                        .field("id", DataTypes.STRING())
                        .field("temperature", DataTypes.FLOAT())
                )
                .createTemporaryTable("outputTable");


        // 执行计算逻辑
        tableEnv.sqlUpdate("insert into outputTable select id, temperature from inputTable where id = 'sensor_6'");


        env.execute("KafkaPipeLine With Sql");
    }
}
