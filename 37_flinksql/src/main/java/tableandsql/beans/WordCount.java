package tableandsql.beans;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/11/9 22:38
 */
public class WordCount {
    private String word;
    private Integer frequency;

    public WordCount() {
    }

    public WordCount(String word, Integer frequency) {
        this.word = word;
        this.frequency = frequency;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    @Override
    public String toString() {
        return "WordCount{" +
                "word='" + word + '\'' +
                ", frequency=" + frequency +
                '}';
    }
}
