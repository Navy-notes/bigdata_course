import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.table.api.scala.StreamTableEnvironment

object WordCountStreamSQL {
  def main(args: Array[String]): Unit = {
    //获取运行环境
    val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    
   val tEnv = StreamTableEnvironment.create(env)

    //链接socket获取输入数据
    val source = env.socketTextStream("localhost",1234)

    //注意：必须要添加这一行隐式转行，否则下面的flatmap方法执行会报错
    import org.apache.flink.api.scala._
    //生成DataStream，并注册DataStream
    val dataStream = source.flatMap(line => line.split(" ")).map(w => WordCount(w,1))
    tEnv.registerDataStream("mytable", dataStream)
    
    //执行查询，并输出
    val data = tEnv.sqlQuery("select word,sum(frequency) as frequency from mytable group by word")
                     
    //执行查询，并输出
    val result = tEnv.toRetractStream[WordCount](data)
    
    result.print
    
    env.execute()
  }
  case class WordCount(word: String, frequency: Integer)
}