import org.apache.flink.api.scala._
import org.apache.flink.table.api.scala.BatchTableEnvironment

object WordCountBatchTable {
  def main(args: Array[String]): Unit = {
    val env = ExecutionEnvironment.getExecutionEnvironment
    
    val tEnv = BatchTableEnvironment.create(env)
    
    val text = env.fromElements("I love Beijing", "I love China", "Beijing is the capital of China")
    val input = text.flatMap(_.split(" ")).map(word => WordCount(word,1))
    
    //使用隐式转换将DataSet转为Table
    val table = tEnv.fromDataSet(input)
    
    val data = table.groupBy("word").select("word, frequency.sum as frequency");

    tEnv.toDataSet[WordCount](data).print()
  }
}

case class WordCount(word:String,frequency:Integer)

