import org.apache.flink.api.scala._
import org.apache.flink.table.api.scala.BatchTableEnvironment

object WordCountBatchSQL {
  def main(args: Array[String]): Unit = {
    val env = ExecutionEnvironment.getExecutionEnvironment
    
    val tEnv = BatchTableEnvironment.create(env)

    val text = env.fromElements("I love Beijing", "I love China", "Beijing is the capital of China")
    //这里的case class：WordCount在前面已经定义了
    val input = text.flatMap(_.split(" ")).map(word => WordCount(word, 1))
    
    val table = tEnv.fromDataSet(input)
    
    //注册表
    tEnv.registerTable("mytable", table)
    
    val result = tEnv.sqlQuery("select word, sum(frequency) from mytable group by word")

    tEnv.toDataSet[WordCount](result).print()
  }
}