package user

import utils.Utils
import org.apache.flink.api.common.state.{ListState, ListStateDescriptor}
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.functions.{AssignerWithPeriodicWatermarks, KeyedProcessFunction}
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.watermark.Watermark
import org.apache.flink.util.Collector


// 输入的登录事件样例类
case class LoginEvent( userId: Long, ip: String, eventType: String, eventTime: Long )
// 输出的异常报警信息样例类
case class Warning( userId: Long, firstFailTime: Long, lastFailTime: Long, warningMsg: String)


/**
 * 用户登陆检测
 * 3s内连续登陆2次，就告警
 */
object LoginCheck {
  def main(args: Array[String]): Unit = {

    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
    env.setParallelism(1)

    env.readTextFile("F:\\IDEAProjects\\bigdata_course\\35_flinkproject\\src\\main\\resources\\data4.csv") //读取数据
      .map(Utils.string2UserLoginEvent(_)) //解析日志
      .assignTimestampsAndWatermarks(new LoginCheckEventTimeExtractor()) //添加水位
      .keyBy(_.userId) // 以用户id做分组
      .process( new LoginWarning(2) ) //实现业务逻辑
      .print()

    env.execute("login fail detect job")


  }

}

class LoginWarning(maxFailTimes: Int) extends KeyedProcessFunction[Long, LoginEvent, Warning] {
  // 定义状态，保存3秒内的所有登录失败事件
  lazy val loginFailState: ListState[LoginEvent] =
                    getRuntimeContext.getListState(
      new ListStateDescriptor[LoginEvent]("login-fail-state", classOf[LoginEvent]))

  override def processElement(value: LoginEvent, ctx: KeyedProcessFunction[Long, LoginEvent, Warning]#Context,
                              out: Collector[Warning]): Unit = {

    if (value.eventType == "fail") {
      // 如果是失败，判断之前是否有登录失败事件
      val iter = loginFailState.get().iterator()
      if (iter.hasNext) {
        // 如果已经有登录失败事件，就比较事件时间
        val firstFail = iter.next()
        if (value.eventTime < firstFail.eventTime + 3) {
          // 如果两次间隔小于3秒，输出报警
          out.collect(Warning(value.userId, firstFail.eventTime, value.eventTime, "3秒内连续两次登录失败."))
        }
        // 更新最近一次的登录失败事件，保存在状态里
        loginFailState.clear()
        loginFailState.add(value)
      } else {
        // 如果是第一次登录失败，直接添加到状态
        loginFailState.add(value)
      }
    } else {
      // 如果是成功，清空状态
      loginFailState.clear()
    }
  }
}



class LoginCheckEventTimeExtractor extends AssignerWithPeriodicWatermarks[LoginEvent]{

  var currentMaxEventTime = 0L
  val maxOufOfOrderness = 10 //最大乱序时间

  override def getCurrentWatermark: Watermark = {
    new Watermark((currentMaxEventTime - maxOufOfOrderness)* 1000)
  }

  override def extractTimestamp(element: LoginEvent, previousElementTimestamp: Long): Long = {
    //时间字段
    val timestamp = element.eventTime * 1000
    currentMaxEventTime = Math.max(currentMaxEventTime, currentMaxEventTime)
    timestamp;
  }
}
