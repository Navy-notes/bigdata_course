package user

import java.lang
import java.sql.Timestamp

import product.HotProduct.{EventTimeExtractor, UserBehavior}
import utils.Utils
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.scala.function.AllWindowFunction
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.util.Collector


//展示UV信息
case class UvInfo(windowEnd:String,uvCount:Long)

object UVCount {
  def main(args: Array[String]): Unit = {
    //获取执行环境
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    //设置参数
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
    env.setParallelism(1)

    env.readTextFile(Utils.userBehaviorLogPath) //读取数据
      .map(Utils.string2UserBehavior(_)) //解析数据
      .assignTimestampsAndWatermarks(new EventTimeExtractor) //添加水位
      .filter(_.behavior == "P") //过滤用户行为
      .timeWindowAll(Time.hours(1)) //滚动的窗口
      .apply(new UvCountByWindow) //统计UV
      .print()

    env.execute("uv count....")
  }
}

class UvCountByWindow() extends
  AllWindowFunction[UserBehavior,UvInfo,TimeWindow]{
  override def apply(window: TimeWindow,
                     values: Iterable[UserBehavior],
                     out: Collector[UvInfo]): Unit = {
    //定一个SET集合，天然的实现去重的效果
    var userIdSet = Set[Long]()

    for(user <- values){
      userIdSet += user.userId
    }
    //如果我们想要看有多少UV，那么我们直接看这个集合的大小就行了。
    out.collect( UvInfo(new Timestamp(window.getEnd).toString,userIdSet.size))
  }
}
