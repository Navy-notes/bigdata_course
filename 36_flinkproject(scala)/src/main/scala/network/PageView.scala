package network

import product.HotProduct.UserBehavior
import utils.Utils
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.watermark.Watermark
import org.apache.flink.streaming.api.windowing.time.Time

/**
 * 统计的PV
 */
object PageView {
  def main(args: Array[String]): Unit = {
    //获取程序入口
    val env = StreamExecutionEnvironment.getExecutionEnvironment
     //设置参数
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
    env.setParallelism(1)

    env.readTextFile(Utils.userBehaviorLogPath) //读数据
      .map(Utils.string2UserBehavior(_)) //解析数据
      .assignTimestampsAndWatermarks(new PageViewEventTimeExtractor) //设置水位
      .filter(_.behavior == "P") //过滤出来page view的行为数据
      .map( data => ("P",1)) //单词计数，让每个pv出现一次
      .timeWindowAll(Time.hours(1)) //滚动窗口，统计每小时的数据。
      .sum(1) //计算结果
      .print()

    env.execute("PageView")
  }


  /**
   * 指定水位
   */
  class PageViewEventTimeExtractor extends AssignerWithPeriodicWatermarks[UserBehavior]{
    var currentMaxEventTime = 0L;
    val maxOutofOrderness = 10;//最大乱序时间
    override def getCurrentWatermark: Watermark = {
      new Watermark((currentMaxEventTime - maxOutofOrderness) * 1000)
    }

    override def extractTimestamp(userBehavior: UserBehavior, l: Long): Long = {
      //指定时间字段
      val timeStamp = userBehavior.timeStamp * 1000
      currentMaxEventTime = Math.max(userBehavior.timeStamp, currentMaxEventTime)
      timeStamp
    }
  }


}

