package order

import java.text.SimpleDateFormat

import org.apache.flink.api.common.state.{ValueState, ValueStateDescriptor}
import org.apache.flink.api.java.tuple.Tuple
import org.apache.flink.configuration.Configuration
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.scala.function.ProcessWindowFunction
import org.apache.flink.streaming.api.watermark.Watermark
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows
import org.apache.flink.streaming.api.windowing.evictors.TimeEvictor
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.triggers.ContinuousEventTimeTrigger
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.util.Collector


object OrderInfoCount {
  def main(args: Array[String]): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
    env.setParallelism(1)

    /**
      * 运行结果：
      * (2020-09-18,7)
        (2020-09-18,9)
        (2020-09-18,10)
        (2020-09-19,2)
      */
    env.socketTextStream("127.0.0.1",1234)

    /**
      * 运行结果：
      * (2020-09-18,10)
        (2020-09-19,2)
      */
//      env.readTextFile("F:\\IDEAProjects\\bigdata_course\\35_flinkproject\\src\\main\\resources\\test")
      .map( line =>{
        val fields = line.split("\t")
        val date = fields(6).trim
        val day = date.split(" ")(0)
        (day,date)
      })
      .assignTimestampsAndWatermarks(new PageViewEventTimeExtractor)
      .keyBy(0)                                                           // 这里是因为要使用KeyState + Evictor所以才需要用keyBy，否则不用keyBy也能完成基础的需求计算
      .window(TumblingEventTimeWindows.of(Time.days(1),Time.hours(16)))   // 统计一天00:00-24:00的整点时间(由于中国是东八区默认一天的整点时间是8:00-24:00)
      .trigger(ContinuousEventTimeTrigger.of(Time.seconds(10)))           // trigger触发不会清除窗口数据，因为它的触发策略是FIRE，但它触发后会清除自己用来保存上一个触发节点时间的ReducingState
      .evictor(TimeEvictor.of(Time.seconds(0),true))                     // 在触发计算的0s后，删除窗口中现存的数据，true表示是否doEvictAfter
      //（这里使用驱逐器，在每次触发计算后删除全量聚合窗口中的数据，能够避免由于ContinuousTimeTrigger周期性触发但实际数据来源并无事件更新的空转行为）
      // 窗口触发计算输出需要满足两个条件：一是watermark大于等于待触发的时间戳，二是窗口中得有数据。
      .process(new myProcess())
      .print()

    env.execute("OrderInfoCount")
  }


  /**
   * 指定水位
   */
  class PageViewEventTimeExtractor extends AssignerWithPeriodicWatermarks[(String,String)]{
    var currentMaxEventTime = 0L;
    val maxOutofOrderness = 10000;//最大乱序时间
    override def getCurrentWatermark: Watermark = {
      new Watermark(currentMaxEventTime - maxOutofOrderness)
    }

    override def extractTimestamp(value: (String,String), l: Long): Long = {
      //指定时间字段
      val TIME_FORMAT= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
      val date: Long = TIME_FORMAT.parse(value._2).getTime

      currentMaxEventTime = Math.max(date, currentMaxEventTime)

      date
    }
  }


  //IN, OUT, KEY, W <: Window
  class myProcess extends ProcessWindowFunction[(String,String),(String,Long),Tuple,TimeWindow] {
    // 这里定义state是为了存储在窗口的全量聚合过程中，由于中途不断地有触发输出，所以为了避免冗余计算，节省内存空间和提高效率，
    // 就把中间的迭代结果保存了下来，并且在每一次触发计算后清除窗口的数据，配合evictor使用。
    private var productState:ValueState[Long]=_

    override def open(parameters: Configuration): Unit = {
      productState = getRuntimeContext.getState(
        new ValueStateDescriptor[Long]("product-state",
          classOf[Long])
      )

    }
    override def process(key: Tuple, context: Context,
                         elements: Iterable[(String, String)],
                         out: Collector[(String,Long)]): Unit = {
      val iterator = elements.iterator
      var count: Long = 0L
      count = productState.value()
      if(count == null){
        count = 0L
      }
     while (iterator.hasNext) {
       iterator.next()
       count += 1L
     }

      productState.update(count)
      out.collect((key.getField(0), count))

    }
  }

}

// 窗口实用触发器:ContinuousEventTimeTrigger
// https://blog.csdn.net/u013516966/article/details/103105377


//case class AreaOrder(areaId: String, amount: Double)
//case class Order(orderId: String, orderTime: Long, gdsId: String, amount: Double, areaId: String)
//
//object ContinuousEventTimeTriggerDemo { def main(args: Array[String]): Unit = {
//  val env = StreamExecutionEnvironment.getExecutionEnvironment
//  env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
//  env.getConfig.setAutoWatermarkInterval(5000L) env.setParallelism(1)
//
//  val kafkaConfig = new Properties();
//  kafkaConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
//  kafkaConfig.put(ConsumerConfig.GROUP_ID_CONFIG, "test1");
//  val consumer = new FlinkKafkaConsumer011[String]("topic1", new SimpleStringSchema(), kafkaConfig)
//
//  env.addSource(consumer)
//    .map(x => { val a = x.split(",") Order(a(0), a(1).toLong, a(2), a(3).toDouble, a(4)) })
//    .assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor[Order](Time.seconds(30)) { override def extractTimestamp(element: Order): Long = element.orderTime })
//    .map(x => { AreaOrder(x.areaId, x.amount) })
//    .keyBy(_.areaId)
//    .timeWindow(Time.hours(1))
//    .trigger(ContinuousEventTimeTrigger.of(Time.minutes(1)))
//    .reduce(new ReduceFunction[AreaOrder] { override def reduce(value1: AreaOrder, value2: AreaOrder): AreaOrder = { AreaOrder(value1.areaId, value1.amount + value2.amount) }
//    })
//    .print()
//
//  env.execute()
//}}
//
//
//
//orderId03,1573874530000,gdsId03,300,beijing (2019-11-16 11:22:10,下一个触发时间是2019-11-16 11:23:00)
//orderId03,1573874740000,gdsId03,300,hangzhou (2019-11-16 11:25:40,下一个触发时间是2019-11-16 11:26:00)
//
//22-23
//23-24
//24-25
