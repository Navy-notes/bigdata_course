package order

import java.text.SimpleDateFormat

import org.apache.flink.api.common.state.{ValueState, ValueStateDescriptor}
import org.apache.flink.api.java.tuple.Tuple
import org.apache.flink.configuration.Configuration
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.scala.function.ProcessWindowFunction
import org.apache.flink.streaming.api.watermark.Watermark
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows
import org.apache.flink.streaming.api.windowing.evictors.TimeEvictor
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.triggers.ContinuousEventTimeTrigger
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.util.Collector


object OrderAmount {
  def main(args: Array[String]): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
    env.setParallelism(1)

    //env.readTextFile(Utils.orderInfoPath)
   // env.socketTextStream("192.168.123.102",9999)
   env.readTextFile("C:\\Users\\MuchM\\IdeaProjects\\FLinkZeYe\\src\\main\\resources\\test")
      .map( line =>{
        val fields = line.split("\t")
        val date = fields(6).trim
        val amount = fields(1).trim.toDouble
        val day = date.split(" ")(0)
        (day,date,amount)
      }) //解析数据
      .assignTimestampsAndWatermarks(new PageViewEventTimeExtractor)//指定水位
      .keyBy(0) //按天进行分组
      .window(TumblingEventTimeWindows.of(Time.days(1),Time.hours(16))) //滚动窗口
      .trigger(ContinuousEventTimeTrigger.of(Time.seconds(10))) //每隔10触发一次计算
      .evictor(TimeEvictor.of(Time.seconds(0),true)) //清除窗口数据
      .process(new myProcess()) //计算订单流水。
      .print()

    env.execute("xx")
  }


  /**
   * 指定水位
   */
  class PageViewEventTimeExtractor extends AssignerWithPeriodicWatermarks[(String,String,Double)]{
    var currentMaxEventTime = 0L;
    val maxOutofOrderness = 10000;//最大乱序时间
    override def getCurrentWatermark: Watermark = {
      new Watermark(currentMaxEventTime - maxOutofOrderness)
    }

    override def extractTimestamp(value: (String,String,Double), l: Long): Long = {
      //指定时间字段
      val TIME_FORMAT= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
      val date: Long = TIME_FORMAT.parse(value._2).getTime

      currentMaxEventTime = Math.max(date, currentMaxEventTime)

      date
    }
  }


  /**
   * 实时累加订单金额
   */
  class myProcess extends ProcessWindowFunction[(String,String,Double),Double,Tuple,TimeWindow] {
    private var amountState:ValueState[Double]=_

    override def open(parameters: Configuration): Unit = {
      amountState = getRuntimeContext.getState(
        new ValueStateDescriptor[Double]("product-state",
          classOf[Double])
      )

    }
    override def process(key: Tuple, context: Context,
                         elements: Iterable[(String, String,Double)],
                         out: Collector[Double]): Unit = {
      val iterator = elements.iterator
      var count: Double = 0
      count = amountState.value()
      if(count == null){
        count = 0L
      }
     while (iterator.hasNext) {
       val tuple = iterator.next()
       //对订单的金额进行累加
       count += tuple._3
     }
     //更新状态
      amountState.update(count)
      //把结果输出到下游
      out.collect(count)


    }
  }

}
