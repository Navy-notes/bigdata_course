package product

import java.sql.Timestamp

import utils.Utils
import org.apache.flink.api.common.functions.AggregateFunction
import org.apache.flink.api.common.state.{ListState, ListStateDescriptor}
import org.apache.flink.configuration.Configuration
import org.apache.flink.runtime.state.filesystem.FsStateBackend
import org.apache.flink.streaming.api.functions.{AssignerWithPeriodicWatermarks, KeyedProcessFunction}
import org.apache.flink.streaming.api.{CheckpointingMode, TimeCharacteristic}
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.scala.function.WindowFunction
import org.apache.flink.streaming.api.watermark.Watermark
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.util.Collector

import scala.collection.mutable.ListBuffer

/**
 * 热门商品统计
 */
object HotProduct {

  //用户行为对象
  case class UserBehavior(
                           userId: Long,
                           productId: Long,
                           categoryId: Long,
                           behavior: String,
                           timeStamp: Long,
                           sessionId: String
                         )

  case class ProductViewCount(
                               productId: Long,
                               windowEnd: Long,
                               count: Long
                             )

  def main(args: Array[String]): Unit = {
    //步骤一:获取执行环境
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    //步骤二:设置参数
    env.setParallelism(1)
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
//    env.enableCheckpointing(60000)
//    env.getCheckpointConfig.setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE)
//    env.getCheckpointConfig.setCheckpointTimeout(120000)
//    env.setStateBackend(new FsStateBackend("hdfs://xxx"))

    //步骤三:读取数据
    env.readTextFile(Utils.userBehaviorLogPath) //读取数据
      .map(Utils.string2UserBehavior(_)) //解析数据
      .assignTimestampsAndWatermarks(new EventTimeExtractor()) //指定watermark
      .filter(_.behavior == "P") //过滤用户行为数据
      .keyBy(_.productId) //按商品进行分组
      .timeWindow(Time.hours(1), Time.minutes(5)) //设置窗口
      .aggregate(new CountProduct(), new WindowResult()) //计算窗口数据
      .keyBy(_.windowEnd) //按照窗口进行分组
      .process(new TopHotProduct(3)) //实现求TopN
      .print()



    //启动程序
    env.execute("HotProduct")
  }

  /**
   * 里面实现求Topn逻辑
   * K,指定Key
   * I,输入的数据类型
   * O,输出的数据类型
   *
   * @param topN
   */
  class TopHotProduct(topN: Int)
    extends KeyedProcessFunction[Long, ProductViewCount, String] {
    private var productState: ListState[ProductViewCount] = _

    override def open(parameters: Configuration): Unit = {
      productState = getRuntimeContext.getListState(
        new ListStateDescriptor[ProductViewCount](
          "product-state",
          classOf[ProductViewCount]
        )
      )

    }


    override def processElement(value: ProductViewCount,
                                context: KeyedProcessFunction[Long, ProductViewCount, String]#Context,
                                collector: Collector[String]): Unit = {
      //把一个窗口里面的所有信息都存储下来
      productState.add(value)
      //注册一个定时器
      context.timerService().registerEventTimeTimer(value.windowEnd + 1)

    }

    /**
     * 定时器里面实现排序的功能
     *
     * @param timestamp
     * @param ctx
     * @param out
     */
    override def onTimer(timestamp: Long,
                         ctx: KeyedProcessFunction[Long, ProductViewCount, String]#OnTimerContext,
                         out: Collector[String]): Unit = {
      var allProduct: ListBuffer[ProductViewCount] = new ListBuffer[ProductViewCount]
      val iterator = productState.get().iterator()
      while (iterator.hasNext) {
        allProduct += iterator.next()
      }
      //实现降序排序
      val sortedProduct = allProduct.sortWith(_.count > _.count)
        .take(topN)
      productState.clear()

      val sb = new StringBuilder
      sb.append("时间:").append(new Timestamp(timestamp - 1))
        .append("\n")

      sortedProduct.foreach(product => {
        sb.append("商品ID:" + product.productId)
          .append("商品访问的次数:" + product.count)
          .append("\n")
      })
      sb.append("=============================")

      out.collect(sb.toString())

    }


  }


  class WindowResult extends WindowFunction[Long, ProductViewCount, Long, TimeWindow] {
    override def apply(key: Long,
                       window: TimeWindow,
                       input: Iterable[Long],
                       out: Collector[ProductViewCount]): Unit = {
      out.collect(ProductViewCount(key, window.getEnd, input.iterator.next()))
    }
  }


  /**
   * IN 输入的数据类型
   * OUT输出的数据类型
   * ACC 辅助变量的数据类型
   */
  class CountProduct extends AggregateFunction[UserBehavior, Long, Long] {
    override def createAccumulator(): Long = 0L

    override def add(in: UserBehavior, acc: Long): Long = acc + 1

    override def getResult(acc: Long): Long = acc

    override def merge(acc: Long, acc1: Long): Long = acc + acc1
  }


  class EventTimeExtractor extends AssignerWithPeriodicWatermarks[UserBehavior] {
    var currentMaxEventTime = 0L
    val maxOutOfOrderness = 10

    override def getCurrentWatermark: Watermark = {
      new Watermark((currentMaxEventTime - maxOutOfOrderness) * 1000)
    }

    override def extractTimestamp(userBehavior: UserBehavior, l: Long): Long = {
      val timeStamp = userBehavior.timeStamp * 1000
      currentMaxEventTime = Math.max(timeStamp, currentMaxEventTime)
      timeStamp
    }
  }


}
