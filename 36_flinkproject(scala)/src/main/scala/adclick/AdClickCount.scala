package adclick

import java.sql.Timestamp

import utils.Utils
import org.apache.flink.api.common.functions.AggregateFunction
import org.apache.flink.api.common.state.{ValueState, ValueStateDescriptor}
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.functions.{AssignerWithPeriodicWatermarks, KeyedProcessFunction}
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.scala.function.WindowFunction
import org.apache.flink.streaming.api.watermark.Watermark
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.util.Collector


// 输入的广告点击事件样例类
case class AdClickEvent( userId: Long, adId: Long, province: String, city: String, timestamp: Long )



// 按照省份统计的输出结果样例类
case class CountByProvince( windowEnd: String, province: String, count: Long )


// 输出的黑名单报警信息
case class BlackListWarning( userId: Long, adId: Long, msg: String )


/**
 * 广告点击统计
 */
object AdClickCount {
  //设置了一个侧输出流
  private val outputBlackList = new OutputTag[BlackListWarning]("blackwaring")

  def main(args: Array[String]): Unit = {
    //获取程序入口
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    //设置参数
    env.setParallelism(1)
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)


    val adEventStream = env.readTextFile(Utils.adClickLogPath) //获取数据
      .map(x => Utils.string2ClickEvent(x)) //解析数据
      .assignTimestampsAndWatermarks(new AdClickEventTimeExtractor()) //设置watermark

    //2. 计算黑名单
    val filterBlackListStream = adEventStream
      .keyBy(data => (data.userId, data.adId)) //分组(userid,adclickid)
      .process(new FilterBlackListUser(100)) //实时统计

    //3. 打印黑名单
    filterBlackListStream.getSideOutput(outputBlackList)
        .print()

    //4.计算各省份的广告点击量
       filterBlackListStream.keyBy(_.province) //按照省份分组
         .timeWindow(Time.hours(1), Time.seconds(5)) //指定滑动窗口
         .aggregate(new AdClickCount, new AdClickWindow) //聚合处理
         .print()


    env.execute("AdClickCount")

  }

  /**
   * 自定义窗口处理
   */
  class AdClickWindow extends WindowFunction[Long,CountByProvince,String,TimeWindow]{
    override def apply(key: String, window: TimeWindow,
                       input: Iterable[Long],
                       out: Collector[CountByProvince]): Unit = {
      out.collect(CountByProvince(new Timestamp(window.getEnd).toString,
        key,input.iterator.next()))
    }
  }

  /**
   * 对广告点击次数进行聚合统计
   */
  class AdClickCount extends AggregateFunction[AdClickEvent,Long,Long]{
    //辅助变量赋初始值
    override def createAccumulator(): Long = 0L
    //对每条数据加一
    override def add(in: AdClickEvent, acc: Long): Long = acc + 1
    //返回最后的结果
    override def getResult(acc: Long): Long = acc
    //把所有的数据加起来
    override def merge(acc: Long, acc1: Long): Long = acc + acc1
  }

  /**
   * 过滤黑名单数据
   * @param maxCount 最大次数
   */
  class FilterBlackListUser(maxCount:Int)
    extends KeyedProcessFunction[(Long,Long),AdClickEvent,AdClickEvent]{
    //保存当前用户对当前广告的点击量
    lazy val countState:ValueState[Long] = getRuntimeContext.getState(
      new ValueStateDescriptor[Long]("count-state",classOf[Long]))
    
    
    //保存是否发送过黑名单
    lazy val isSetBlackList:ValueState[Boolean] = getRuntimeContext.getState(
      new ValueStateDescriptor[Boolean]("issent-state",classOf[Boolean]))
//
    //保存定时器触发的时间戳
    lazy val resetTimeer:ValueState[Long] = getRuntimeContext.getState(
      new ValueStateDescriptor[Long]("resettime-state",classOf[Long])
    )

    override def processElement(value: AdClickEvent,
                                ctx: KeyedProcessFunction[(Long, Long),
                                  AdClickEvent, AdClickEvent]#Context,
                                out: Collector[AdClickEvent]): Unit = {
      val currentCount = countState.value()
      //如果当前用户的当前广告第一次来，注册定时器，每天00:00触发
      //在一天内，如果一个用户对一个广告点击超过了100次，黑名单。
      //也就是说，到了晚上12：00的时候，你要清空今天统计的数据。
      if(currentCount == 0){
       //计算时间
       val ts = (ctx.timerService().currentProcessingTime()/(1000*60*60*24) +1) * (1000 * 60 * 60 * 24)
        resetTimeer.update(ts)
        //注册定时器
        ctx.timerService().registerProcessingTimeTimer(ts)
      }
      //判断计数是否达到上线，如果达到加入黑名单
      if(currentCount >= maxCount){
        //是否发送过黑名单
        if(!isSetBlackList.value()){ //如果没有发送过黑名单消息
          //更新一下发送黑名单的状态
          isSetBlackList.update(true)
         //输入到侧输出流
          ctx.output(outputBlackList,
            BlackListWarning(value.userId,value.adId,"点击超过" + maxCount +" 次"))
        }
        return
      }
      //更新当前的状态，累加访问的次数
      countState.update(currentCount + 1)
      out.collect(value)
    }

    override def onTimer(timestamp: Long,
                         ctx: KeyedProcessFunction[(Long, Long),
                           AdClickEvent, AdClickEvent]#OnTimerContext,
                           out: Collector[AdClickEvent]): Unit = {
      if(timestamp == resetTimeer.value()){
        isSetBlackList.clear()
        countState.clear()
        resetTimeer.clear()
      }

    }

  }

}

class AdClickEventTimeExtractor extends AssignerWithPeriodicWatermarks[AdClickEvent]{
   //当前窗口的时间最大值
  var currentMaxEventTime = 0L
  //最大乱序时间 10s
  val maxOufOfOrderness = 10

  override def getCurrentWatermark: Watermark = {
    new Watermark((currentMaxEventTime - maxOufOfOrderness) * 1000)
  }

  override def extractTimestamp(element: AdClickEvent, previousElementTimestamp: Long): Long = {
    //时间字段
    val timestamp = element.timestamp * 1000

    currentMaxEventTime = Math.max(element.timestamp, currentMaxEventTime)
    timestamp;
  }
}
