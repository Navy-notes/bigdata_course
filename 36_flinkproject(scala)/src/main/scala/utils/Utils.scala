package utils

import java.text.SimpleDateFormat

import adclick.AdClickEvent
import network.ApacheLogEvent
import product.HotProduct.UserBehavior
import user.LoginEvent

object Utils {

  //用户行为日志数据路径
  val userBehaviorLogPath ="C:\\Users\\MuchM\\IdeaProjects\\FLinkZeYe\\src\\main\\resources\\data1.csv"

  //服务日志路径
  //时间日志路径
  val eventLogPath = "C:\\Users\\MuchM\\IdeaProjects\\FLinkZeYe\\src\\main\\resources\\data2.log"

  //广告点击日志路径
  val adClickLogPath = "C:\\Users\\MuchM\\IdeaProjects\\FLinkZeYe\\src\\main\\resources\\data3.csv"
  //订单日志路径
  val orderInfoPath = "C:\\Users\\MuchM\\IdeaProjects\\FLinkZeYe\\src\\main\\resources\\order_info.txt"

  //用户登录日志路径
  val userLoginEvent = "C:\\Users\\MuchM\\IdeaProjects\\FLinkZeYe\\src\\main\\resources\\data4.csv"


  /**
   * 根据字符串,把数据转换成为我们的用户行为对象
   * @param line
   * @return
   */
  def string2UserBehavior(line:String):UserBehavior={
    val fields = line.split(",")
    UserBehavior(
      fields(0).trim.toLong,
      fields(1).trim.toLong,
      fields(2).trim.toLong,
      fields(3).trim,
      fields(4).trim.toLong,
      fields(5).trim
    )
  }



  /**
   * 根据字符串把数据转换成为日志服务数据对象
   * @param line
   * @return
   */
  def string2ApacheLogEvent(line:String):ApacheLogEvent={
    val fields = line.split(" ")
    val dateFormat = new SimpleDateFormat("dd/MM/yyyy:HH:mm:ss")
    val timeStamp = dateFormat.parse(fields(3).trim).getTime
    ApacheLogEvent(fields(0).trim,fields(1).trim,timeStamp,
      fields(5).trim,fields(6).trim)
  }

  /**
   * 根据字符串生成广告点击日志对象
   * @param line
   * @return
   */
  def string2ClickEvent(line:String):AdClickEvent={
    val dataArray = line.split(",")
    AdClickEvent(dataArray(0).trim.toLong, dataArray(1).trim.toLong, dataArray(2).trim, dataArray(3).trim, dataArray(4).trim.toLong)
  }

  /**
   * 根据字符串生成一个用户登录日志对象
   * @param line
   * @return
   */
  def string2UserLoginEvent(line:String):LoginEvent={
    val dataArray = line.split(",")
    LoginEvent( dataArray(0).trim.toLong,
      dataArray(1).trim, dataArray(2).trim, dataArray(3).trim.toLong )
  }




}
