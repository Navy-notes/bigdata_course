import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkConf
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.{Seconds, StreamingContext}

object MyNetworkWordCountByWindow {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org.apache.spark").setLevel(Level.OFF)

    // 创建StreamingContext对象
    val conf = new SparkConf().setAppName("MyNetworkWordCount").setMaster("local[2]")
    // 两个参数 1:conf 2.采样时间间隔:每隔3s
    val ssc = new StreamingContext(conf, Seconds(3))
    // 创建一个Dstream
    val lines = ssc.socketTextStream("127.0.0.1", 1234, StorageLevel.MEMORY_ONLY)

    // 进行单词计数,分词
    val words = lines.flatMap(_.split(" ")).cache()

    val wordPair = words.map(x => (x, 1)).cache()

    // 窗口计数   （在窗口长度范围内，数据也具有累加的效果）
    val result = wordPair.reduceByKeyAndWindow((a: Int, b: Int) => (a + b), Seconds(30), Seconds(9))  // 滑动距离必须是采样间隔整数倍且不能为0


    // 打印结果   （每滑动距离的时间间隔，计算并打印输出一次）
    result.print()

    // 启动StreamingContext,进行计算
    ssc.start()
    // 等待任务的结束
    ssc.awaitTermination()
  }
}
