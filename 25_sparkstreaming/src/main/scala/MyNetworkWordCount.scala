import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkConf
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.{Seconds, StreamingContext}

object MyNetworkWordCount {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.OFF)
    System.setProperty("spark.ui.showConsoleProgress","false")

    //创建StreamingContext对象
    val conf = new SparkConf().setAppName("MyNetworkWordCount").setMaster("local[2]")
    //两个参数 1:conf 2.采样时间间隔:每隔3s
    val ssc = new StreamingContext(conf, Seconds(3))

    //创建一个Dstream
    val lines = ssc.socketTextStream("127.0.0.1", 1234, StorageLevel.MEMORY_ONLY)


    //进行单词计数,分词
    val words = lines.flatMap(_.split(" "))
    //计数
    val wordCount = words.map((_, 1)).reduceByKey(_ + _)

    //使用transform完成跟map一样的作用
    //val wordPair = words.transform(rdd => rdd.map(word=> (word, 1)))

    //打印结果
    wordCount.print()

    //启动StreamingContext,进行计算
    ssc.start()
    //等待任务的结束
    ssc.awaitTermination()

  }

}
