import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkConf
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.{Seconds, StreamingContext}

object MyTotalNetworkWordCount {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.OFF)
    System.setProperty("spark.ui.showConsoleProgress","false")

    // 创建StreamingContext对象
    val conf = new SparkConf().setAppName("MyNetworkWordCount").setMaster("local[2]")
    //两个参数 1:conf 2.采样时间间隔:每隔3s
    val ssc = new StreamingContext(conf, Seconds(3))

    // 设置检查点目录,保存之前的状态信息
    ssc.checkpoint("25_sparkstreaming/src/main/document/checkpoint/sparkstreaming/totalcount")

    // 创建一个DStream
    val lines = ssc.socketTextStream("127.0.0.1", 1234, StorageLevel.MEMORY_ONLY)

    // 进行单词计数,分词
    val words = lines.flatMap(_.split(" "))
    // 计数
    val wordPair = words.map(x => (x, 1))

    /**
     * 定义一个值函数  def
     * 1.当前值  2.之前值
     */
    val updateFunc = (currVal: Seq[Int], preValueState: Option[Int]) => {
      // 进行一个累加运算
      //1.得到当前传进来的所有值的和
      val currentTotal = currVal.sum
      //2.拿到之前的值
      val totalValue = preValueState.getOrElse(0)
      //3.返回
      Some(currentTotal + totalValue)
    }

    // 进行累加运算
    val totalResult = wordPair.updateStateByKey(updateFunc)

    // 输出
    totalResult.print()

    // 启动StreamingContext,进行计算
    ssc.start()
    // 等待任务的结束
    ssc.awaitTermination()


  }
}
