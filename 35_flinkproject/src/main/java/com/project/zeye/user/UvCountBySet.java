package com.project.zeye.user;

import com.project.zeye.beans.UserBehavior;
import com.project.zeye.utils.Utils;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks;
import org.apache.flink.streaming.api.functions.windowing.AllWindowFunction;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import javax.annotation.Nullable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * 统计每小时uv （使用Set集合去重）
 */
public class UvCountBySet {
    private static DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        env.readTextFile(Utils.USER_BEHAVIOR_LOG_PATH)
                .map(line -> Utils.string2UserBehavior(line))
                .assignTimestampsAndWatermarks(new EventTimeExtractor())
                .filter(userBehavior -> "P".equals(userBehavior.getBehavior()))
                .timeWindowAll(Time.hours(1))
                .apply(new HourlyUvCountFunction())
                .print();

        env.execute("UvCountBySet");
    }


    static class HourlyUvCountFunction implements AllWindowFunction<UserBehavior, Tuple2<String, Long>, TimeWindow> {

        @Override
        public void apply(TimeWindow timeWindow, Iterable<UserBehavior> iterable, Collector<Tuple2<String, Long>> out) throws Exception {
            // 利用Set集合去重
            Set<Long> userIdSet = new HashSet<>();

            Iterator<UserBehavior> iterator = iterable.iterator();
            while (iterator.hasNext()) {
                userIdSet.add(iterator.next().getUserId());
            }

            // set的大小即为活跃用户的个数
            out.collect(Tuple2.of(df.format(new Date(timeWindow.getEnd())), new Long(userIdSet.size())));

        }
    }


    static class EventTimeExtractor implements AssignerWithPeriodicWatermarks<UserBehavior> {
        private long currentMaxTs = 0;
        private long maxOutOfOrderness = 10;

        @Nullable
        @Override
        public Watermark getCurrentWatermark() {
            return new Watermark((currentMaxTs - maxOutOfOrderness) * 1000);
        }

        @Override
        public long extractTimestamp(UserBehavior userBehavior, long l) {
            currentMaxTs = Math.max(currentMaxTs, userBehavior.getTimeStamp());
            return userBehavior.getTimeStamp() * 1000;
        }
    }
}
