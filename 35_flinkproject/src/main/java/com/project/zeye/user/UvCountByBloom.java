package com.project.zeye.user;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;
import com.project.zeye.beans.UserBehavior;
import com.project.zeye.utils.Utils;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.triggers.CountTrigger;
import org.apache.flink.streaming.api.windowing.triggers.PurgingTrigger;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import javax.annotation.Nullable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

/**
 * 统计每小时uv （使用布隆过滤器去重）
     * 布隆过滤器【仅个人理解，实际并不完全准确】
         位图 + hash取模的思想，由于不相同的元素的hash值有可能一致，所以会存在误判率。
         误判率的大小取决于模数能有多大，模数就相当于在内存有多少个位。比如模数为1000w，则内存中占有1kw个bit，转换一下即1.2M内存。
         模数越大，则误判率越低，但是消耗的内存空间和计算资源就会越多，反之减小占用的空间，但是精确度会下降。
         （过滤器认为没有重复的元素就一定没有重复，认为有重复的元素可能并没有重复）

     * 使用bloom算法进行去重计算的uv有可能比实际的值偏小。
     * 参考博客：https://blog.csdn.net/danengbinggan33/article/details/89675408
 *
 */
public class UvCountByBloom {
    // 这里非线程安全，应该使用ThreadLocal保证多并行度下结果的准确性
    private static DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.createLocalEnvironmentWithWebUI(new Configuration());
        env.setParallelism(1);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        env.readTextFile(Utils.USER_BEHAVIOR_LOG_PATH)
                .map(line -> Utils.string2UserBehavior(line))
                .assignTimestampsAndWatermarks(new EventTimeExtractor())
                .filter(userBehavior -> "P".equals(userBehavior.getBehavior()))
                .map(userBehavior -> {
                    Long windowEndTs = userBehavior.getTimeStamp() - userBehavior.getTimeStamp() % 3600 + 3600;
                    return Tuple2.of(windowEndTs * 1000L, userBehavior.getUserId());
                })
                .returns(Types.TUPLE(Types.LONG, Types.LONG))
                .keyBy(0)                                   // 如果数据量特别大，为了避免数据倾斜，key的前缀需要加一个随机数进行分散处理，然后从输出端获取时再汇总
                                                                    // 或者使用operator state（但这种方式开发难度更大），那就不必keyBy分发，这样可以让数据负载均衡到所有的分区
                .window(TumblingEventTimeWindows.of(Time.hours(1)))
                .trigger(PurgingTrigger.of(CountTrigger.of(1)))     // 实现窗口每来n条数据就计算一次，并且清除数据（将全量聚合 -> 增量聚合）
                .process(new UvCountByBloomFunction())
                .print();

        env.execute("UvCountByBloom");
    }


    /**
     * 使用guava实现去重，并统计uv
     * 窗口函数的上下文无法注册定时器
     */
    static class UvCountByBloomFunction extends ProcessWindowFunction<Tuple2<Long, Long>, Tuple2<String, Long>, Tuple, TimeWindow> {
        // 只要key相同的数据集，尽管所在的时间窗口不一样，它们的KeyState依然是共同的
        private ValueState<Long> countState;
        private ValueState<BloomFilter> bloomFilterState;

        @Override
        public void open(Configuration parameters) throws Exception {
            countState = getRuntimeContext().getState(new ValueStateDescriptor<>("count-state", Long.class));
            bloomFilterState = getRuntimeContext().getState(new ValueStateDescriptor<BloomFilter>("bloom-state", BloomFilter.class));
        }

        @Override
        public void process(Tuple key, Context ctx, Iterable<Tuple2<Long, Long>> iterable, Collector<Tuple2<String, Long>> out) throws Exception {
            // 初始化state的值
            Long count = countState.value();
            if (count == null) {
                count = 0L;
            }
            BloomFilter bloomFilter = bloomFilterState.value();
            if (bloomFilter == null) {
                /**
                 * 创建时需要传入四个参数，但我们只要关心前三个就行：
                    1. Funnel，这是Guava中定义的一个接口，它和PrimitiveSink配套使用，主要是把任意类型的数据转化成Java基本数据类型（primitive value，如char，byte，int……），默认用java.nio.ByteBuffer实现，最终均转化为byte数组
                    2. expectedInsertions 期望插入数据数，int或long类型
                    3. fpp期望误判率，比如1E-7（千万分之一）
                    4. Strategy 策略，默认选取64位哈希映射函数，BloomFilterStrategies.MURMUR128_MITZ_64
                 *
                 * 比如，创建字符串布隆过滤器，使用编码UTF-8
                    BloomFilter<CharSequence> bloomFilter = BloomFilter.create(Funnels.stringFunnel(Charsets.UTF_8), 200000, 1E-7);
                 */
                bloomFilter = BloomFilter.create(Funnels.longFunnel(), 100000, 1E-7);
            }

            Iterator<Tuple2<Long, Long>> iterator = iterable.iterator();
            while(iterator.hasNext()) {
                Long userId = iterator.next().f1;
                if (!bloomFilter.mightContain(userId)) {
                    bloomFilter.put(userId);
                    count += 1;
                }
            }

            // 更新state
            countState.update(count);
            bloomFilterState.update(bloomFilter);


            out.collect(Tuple2.of(df.format(new Date((Long) key.getField(0))), count));
        }


        @Override
        // 每一个窗口调用一次，但貌似并不是窗口关闭的时候调用
        public void clear(Context context) throws Exception {
            countState.clear();
            bloomFilterState.clear();
        }
    }



    /**
     * 提取时间戳并注册水位
     */
    static class EventTimeExtractor implements AssignerWithPeriodicWatermarks<UserBehavior> {
        private long currentMaxTs = 0;
        private long maxOutOfOrderness = 10;

        @Nullable
        @Override
        public Watermark getCurrentWatermark() {
            return new Watermark((currentMaxTs - maxOutOfOrderness) * 1000);
        }

        @Override
        public long extractTimestamp(UserBehavior userBehavior, long l) {
            currentMaxTs = Math.max(currentMaxTs, userBehavior.getTimeStamp());
            return userBehavior.getTimeStamp() * 1000;
        }
    }
}
