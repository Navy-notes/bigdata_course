package com.project.zeye.user;

import com.project.zeye.beans.LoginEvent;
import com.project.zeye.beans.WindControlWarning;
import com.project.zeye.utils.Utils;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.cep.CEP;
import org.apache.flink.cep.PatternSelectFunction;
import org.apache.flink.cep.PatternStream;
import org.apache.flink.cep.pattern.Pattern;
import org.apache.flink.cep.pattern.conditions.SimpleCondition;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.apache.flink.streaming.api.windowing.time.Time;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Map;

/**
 * 使用CEP进行实时风控：3秒内连续失败登录两次即告警
 */
public class LoginWindControlWithCEP {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        env.setParallelism(1);

        // 步骤一：读取事件数据
        KeyedStream<LoginEvent, Tuple> loginEventStream = env.readTextFile(Utils.USER_LOGIN_LOG_PATH)
                .map(line -> Utils.string2LoginEvent(line))
                .assignTimestampsAndWatermarks(new EventTimeExtractor())
                .keyBy("userId");


        // 步骤二：定义匹配模式
        Pattern<LoginEvent, LoginEvent> loginFailPattern = Pattern
                .<LoginEvent>begin("firstFailEvent").where(new SimpleCondition<LoginEvent>() {
            @Override
            public boolean filter(LoginEvent loginEvent) throws Exception {
                return loginEvent.getEventType().equals("fail");
            }
        })
                .next("secondFailEvent").where(new SimpleCondition<LoginEvent>() {
                    @Override
                    public boolean filter(LoginEvent loginEvent) throws Exception {
                        return loginEvent.getEventType().equals("fail");
                    }
                })                          // 如果想要连续三个失败登录事件则继续.next.where；如果事件非严格相邻可用.follow
                .within(Time.seconds(3));   // 规定时间在三秒內


        // 步骤三：在事件流上应用模式，得到一个pattern stream
        PatternStream<LoginEvent> patternStream = CEP.pattern(loginEventStream, loginFailPattern);


        // 步骤四：从pattern stream上应用select function，检出匹配事件序列
        SingleOutputStreamOperator<WindControlWarning> loginFailDataStream = patternStream.select(new PatternSelectFunction<LoginEvent, WindControlWarning>() {
            @Override
            public WindControlWarning select(Map<String, List<LoginEvent>> map) throws Exception {
                // 从map中按照名称取出对应的事件
                LoginEvent firstFailEvent = map.get("firstFailEvent").get(0);    // 第一次登录失败事件
                LoginEvent secondFailEvent = map.get("secondFailEvent").get(0);  // 第二次登录失败事件

                return new WindControlWarning(firstFailEvent.getUserId(), firstFailEvent.getTimestamp(), secondFailEvent.getTimestamp(), "3秒内连续两次登录失败");
            }
        });

        // 输出告警信息
        loginFailDataStream.print();

        env.execute("LoginWindControlWithCEP");
    }



    static class EventTimeExtractor implements AssignerWithPeriodicWatermarks<LoginEvent> {
        private long currentMaxTs = 0;
        private long maxOutOfOrderness = 10;

        @Nullable
        @Override
        public Watermark getCurrentWatermark() {
            return new Watermark((currentMaxTs - maxOutOfOrderness) * 1000);
        }

        @Override
        public long extractTimestamp(LoginEvent loginEvent, long l) {
            currentMaxTs = Math.max(currentMaxTs, loginEvent.getTimestamp());
            return loginEvent.getTimestamp() * 1000;
        }
    }
}
