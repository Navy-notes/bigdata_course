package com.project.zeye.user;

import com.project.zeye.beans.LoginEvent;
import com.project.zeye.beans.WindControlWarning;
import com.project.zeye.utils.Utils;
import org.apache.commons.compress.utils.Lists;
import org.apache.flink.api.common.state.ListState;
import org.apache.flink.api.common.state.ListStateDescriptor;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.apache.flink.util.Collector;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;

/**
 * 实时风控：3秒之内登录连续失败达到两次，则进行告警
 */
public class LoginWindControl {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        env.readTextFile(Utils.USER_LOGIN_LOG_PATH)
                .map(line -> Utils.string2LoginEvent(line))
                .assignTimestampsAndWatermarks(new EventTimeExtractor())
                .keyBy("userId")
                .process(new LoginWindControlFunction())
                .print();

        env.execute("LoginWindControl");
    }


    /**
     * 用户登录检测：如果连续失败达到两次后判断事件相隔在3s之内则告警
     */
    static class LoginWindControlFunction extends KeyedProcessFunction<Tuple, LoginEvent, WindControlWarning> {
        private ListState<LoginEvent> loginFailState;

        @Override
        public void open(Configuration parameters) throws Exception {
            loginFailState = getRuntimeContext().getListState(new ListStateDescriptor<>("loginFailState", LoginEvent.class));
        }

        @Override
        public void processElement(LoginEvent event, Context ctx, Collector<WindControlWarning> out) throws Exception {
            // state初始化赋值
            if (loginFailState.get() == null) {
                loginFailState.addAll(Collections.emptyList());
            }
            ArrayList<LoginEvent> failList = Lists.newArrayList(loginFailState.get().iterator());

            // 失败事件判断
            if ("fail".equals(event.getEventType())) {
                if (failList.size() != 0) {
                    Long firstFailTime = failList.get(0).getTimestamp();
                    Long lastFailTime = event.getTimestamp();
                    // （使用绝对值是为了避免一些时间乱序导致差值小于0的情况被误判为告警输出）
                    if (Math.abs(lastFailTime - firstFailTime) < 3) {
                        // 达到告警规则，输出
                        out.collect(new WindControlWarning(event.getUserId(), firstFailTime, lastFailTime, "3秒内连续两次登录失败"));
                    }
                    // 移除第一次失败事件
                    failList.remove(0);
                }
                // 记录本趟失败事件
                failList.add(event);
                loginFailState.update(failList);
            }
            else {
                // 如果是成功事件，那么要清空记录连续失败的state
                loginFailState.clear();
            }
        }
    }

    static class EventTimeExtractor implements AssignerWithPeriodicWatermarks<LoginEvent> {
        private long currentMaxTs = 0;
        private long maxOutOfOrderness = 10;

        @Nullable
        @Override
        public Watermark getCurrentWatermark() {
            return new Watermark((currentMaxTs - maxOutOfOrderness) * 1000);
        }

        @Override
        public long extractTimestamp(LoginEvent loginEvent, long l) {
            currentMaxTs = Math.max(currentMaxTs, loginEvent.getTimestamp());
            return loginEvent.getTimestamp() * 1000;
        }
    }
}
