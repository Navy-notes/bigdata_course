package com.project.zeye.adclick;

import com.project.zeye.beans.AdClickEvent;
import com.project.zeye.beans.AdCountByProvince;
import com.project.zeye.beans.BlackListWarning;
import com.project.zeye.utils.Utils;
import org.apache.flink.api.common.functions.AggregateFunction;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;

import javax.annotation.Nullable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 各个省份的广告点击统计，需要过滤出黑名单
 *
 * 思考：广告点击统计和生成黑名单是否可以分成两个独立的Job作业去跑？不可以，说明这两个之间是存在耦合关联的。
 *       广告点击统计是否要求排除黑名单脏数据的影响，本例中只是对黑名单用户100次以后的点击事件进行了过滤。
 *       在判定该用户进入黑名单之前，他的浏览和点击行为我们认为他是正常现象，归于了统计的范围内。这对于pv计算等点击量统计的业务场景，精确度要求不高是没有问题的。
 *       如果想要实现当判断一个用户为黑名单后，他当天的之前的点击行为作废，如果这涉及到金额财务统计，要求精确的话，我们就得把那100次的点击量减回去。
 *       可以如此实现，侧流输出的黑名单字段中加上该用户的省份信息以及时间段信息，后期进行结算的时候再做处理，出现一个黑名单用户就在其相应的省份和时间段的点击量减去100即可。
 *       另一种可能则是实时校正，目前思路是想采用join流的方法但没能给出进一步实现的理论支撑。
 */
public class AdClickCountJob {
    // 定义一个侧输出流标签
    private static OutputTag<BlackListWarning> outputTag = new OutputTag<BlackListWarning>("blackwarning"){};

    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        // 获取数据并解析数据，设置watermark
        SingleOutputStreamOperator<AdClickEvent> adEventStream = env.readTextFile(Utils.AD_CLICK_LOG_PATH)
                .map(line -> Utils.string2ClickEvent(line))
                .assignTimestampsAndWatermarks(new EventTimeExtractor());

        // 计算黑名单的用户数据并过滤
        SingleOutputStreamOperator<AdClickEvent> filterBlackListStream = adEventStream.keyBy("userId", "adId")
                .process(new FilterBlackList(100));

        // 侧流输出黑名单用户
        filterBlackListStream.getSideOutput(outputTag).print();

        // 计算各省份的广告点击量并输出
        filterBlackListStream.keyBy("province")
                .timeWindow(Time.hours(1), Time.seconds(5))
                .aggregate(new AdClickCount(), new AdClickWindow())
                .print();


        env.execute("AdClickCountJob");
    }


    static class EventTimeExtractor implements AssignerWithPeriodicWatermarks<AdClickEvent> {
        private long currentMaxTimestamp = 0;
        private long maxOutOfOrderness = 10;

        @Nullable
        @Override
        public Watermark getCurrentWatermark() {
            return new Watermark((currentMaxTimestamp - maxOutOfOrderness) * 1000);
        }

        @Override
        public long extractTimestamp(AdClickEvent adClickEvent, long l) {
            currentMaxTimestamp = Math.max(adClickEvent.getTimestamp(), currentMaxTimestamp);
            return adClickEvent.getTimestamp() * 1000;
        }
    }


    /**
     * 过滤黑名单数据
     */
    static class FilterBlackList extends KeyedProcessFunction<Tuple, AdClickEvent, AdClickEvent> {
        // 同一个广告允许同一个用户点击的最大次数
        private int allowedMaxClicks;
        // 保存当前用户对当前广告的点击量
        private ValueState<Integer> countState;
        // 保存是否发送过黑名单
        private ValueState<Boolean> isSetBlackList;

        public FilterBlackList(int allowedMaxClicks) {
            this.allowedMaxClicks = allowedMaxClicks;
        }

        @Override
        public void open(Configuration parameters) throws Exception {
            countState = getRuntimeContext().getState(new ValueStateDescriptor<>("count-state", Integer.class,0));
            isSetBlackList = getRuntimeContext().getState(new ValueStateDescriptor<>("issent-state", Boolean.class,false));
        }

        @Override
        public void processElement(AdClickEvent adClickEvent, Context ctx, Collector<AdClickEvent> out) throws Exception {

            /**
             * 如果当前用户的当前广告第一次来，注册定时器，在次日的00:00触发
             * 在一天内，如果一个用户对一个广告点击超过了100次，则进入黑名单。
             * 但是过了晚上23:59:59后，你要清空今天统计的数据。
             * （根据不同的需求情况，定时器的时间类型也可以是EventTime）
             */
            if (countState.value() == 0) {
                // 设置定时器触发的时间戳
                long ts = (ctx.timerService().currentProcessingTime() / (3600 * 24 * 1000) + 1) * 3600 * 24 * 1000;
                // 注册一个定时器，每次零点触发
                ctx.timerService().registerProcessingTimeTimer(ts);
            }

            // 更新当前的状态，累加访问的次数
            countState.update(countState.value() + 1);

            // 判断计数是否达到允许的上限，如果达到则加入黑名单
            if (countState.value() > 100) {
                // 判断是否发送过黑名单，如果状态为false则发送到黑名单并更新状态
                if (!isSetBlackList.value()) {
                    String msg = "点击超过" + allowedMaxClicks + "次!";
                    // 发送到侧输出流
                    ctx.output(outputTag, new BlackListWarning(adClickEvent.getUserId(), adClickEvent.getAdId(), msg));
                    // 更新状态
                    isSetBlackList.update(true);
                }
                return;
            }

            out.collect(adClickEvent);
        }

        @Override
        public void onTimer(long timestamp, OnTimerContext ctx, Collector<AdClickEvent> out) throws Exception {
            // 到点，重置状态
            countState.update(0);
            isSetBlackList.update(false);
//            countState.clear();
//            isSetBlackList.clear();
        }
    }


    /**
     * 对广告点击次数进行聚合统计
     */
    static class AdClickCount implements AggregateFunction<AdClickEvent, Long, Long> {
        // 辅助变量赋初始值
        @Override
        public Long createAccumulator() {
            return 0L;
        }

        // 对每条数据加一
        @Override
        public Long add(AdClickEvent adClickEvent, Long acc) {
            return acc + 1;
        }

        // 返回最后的结果
        @Override
        public Long getResult(Long result) {
            return result;
        }

        // 把所有分区的数据加起来
        @Override
        public Long merge(Long acc1, Long acc2) {
            return acc1 + acc2;
        }
    }


    /**
     * 自定义窗口函数
     */
    static class AdClickWindow implements WindowFunction<Long, AdCountByProvince, Tuple, TimeWindow> {
        @Override
        public void apply(Tuple key, TimeWindow timeWindow, Iterable<Long> iterable, Collector<AdCountByProvince> out) throws Exception {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            out.collect(new AdCountByProvince(df.format(new Date(timeWindow.getEnd())), key.getField(0), iterable.iterator().next()));
        }
    }
}
