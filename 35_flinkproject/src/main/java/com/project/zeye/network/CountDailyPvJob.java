package com.project.zeye.network;

import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.apache.flink.util.Collector;

import javax.annotation.Nullable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * KeyState + Timer实现---统计每日pv并实时输出
 * 使用到的算子map、keyBy、process
 * 本例为PageViewJob的拓展延伸
 *
 * 测试数据：采用test文本以及netcat socket流
 */
public class CountDailyPvJob {
    private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

//        env.readTextFile("F:\\IDEAProjects\\bigdata_course\\35_flinkproject\\src\\main\\resources\\test")
        // 模拟实时流和事件流的处理最好是用kafka或者socket流等实时监听的线程
        // 否则读取文件作为数据源很有可能会被flink当成批模式处理，结果就和实时处理的思维体系有很大不同了。
        env.socketTextStream("localhost", 1234)
                .map(line -> {
                    String[] fields = line.split("\t");
                    String day = fields[6].trim().split(" ")[0];
                    Long ts = dateFormat.parse(fields[6].trim()).getTime();
                    return Tuple2.of(day, ts);
                })
                .returns(Types.TUPLE(Types.STRING, Types.LONG))
                .assignTimestampsAndWatermarks(new EventTimeExtractor())
                .keyBy(0)     // 使用KeyState必须要用到算子keyBy，除此还有其他原因这里也必须用keyBy
                .process(new RealTimePvFunction(10 * 1000))
                .print();

        env.execute("CountDailyPvJob");
    }


    static class RealTimePvFunction extends KeyedProcessFunction<Tuple, Tuple2<String, Long>, Tuple2<String, Integer>> {
        // 存储pv的累加结果
        private ValueState<Integer> pvCountState;
        // 清除前一日pv数据的触发时间戳
        private ValueState<Long> pvPurgeState;
        // 输出当前pv值的触发时间戳
        private ValueState<Long> pvFireState;
        // 缓存当日的最大时间戳（这里也必须用KeyState，不然对于所有的key的事件，这个值都是相同的）
        private ValueState<Long> currentDayMaxTs;

        // 定义每次输出的时间间隔，单位(毫秒)
        private long interval;

        public RealTimePvFunction(long interval) {
            this.interval = interval;
        }

        @Override
        public void open(Configuration parameters) throws Exception {
            pvCountState = getRuntimeContext().getState(new ValueStateDescriptor<>("pv-count", Integer.class));
            pvPurgeState = getRuntimeContext().getState(new ValueStateDescriptor<>("pv-purge", Long.class));
            pvFireState = getRuntimeContext().getState(new ValueStateDescriptor<>("pv-fire", Long.class));
            currentDayMaxTs = getRuntimeContext().getState(new ValueStateDescriptor<>("currentDayMaxTs", Long.class));
        }

        @Override
        public void processElement(Tuple2<String, Long> element, Context ctx, Collector<Tuple2<String, Integer>> out) throws Exception {
            // 如果水流中当前的watermark已经进入到了nextDay，那么后面迟到的数据不再处理（类似窗口关闭不再计算）
            if (currentDayMaxTs.value() == null) {
                currentDayMaxTs.update(dateFormat.parse(ctx.getCurrentKey().getField(0) + " 23:59:59").getTime() + 999);  // 单位是毫秒
            }
            if (ctx.timerService().currentWatermark() > currentDayMaxTs.value())
                return;

            // 计算pv
            Integer pv = pvCountState.value();
            if (pv == null) {
                pv = 0;
            }
            pvCountState.update(pv + 1);

            // 注册定时清除器并更新state
            if (pvPurgeState.value() == null) {
                long timestampNextDay = (element.f1 / (3600 * 24 * 1000) + 1) * 3600 * 24 * 1000;
                ctx.timerService().registerEventTimeTimer(timestampNextDay);
                pvPurgeState.update(timestampNextDay);
            }

            // 注册定时触发器并更新state
            if (pvFireState.value() == null) {
                long timestampNextFire = (element.f1 - element.f1 % interval) + interval;
                ctx.timerService().registerEventTimeTimer(timestampNextFire);
                pvFireState.update(timestampNextFire);
            }
        }

        /**
         *  触发时间戳是区分不同定时器的唯一标识。
         *
         *  在flink中，会把定时器根据触发时间进行升序排序，排在前面的定时器先触发，排在后面的定时器后触发。
         *  举例，流环境中注册了事件时间的定时器T1和T2，他们的触发时间戳t1 < t2。
         *        当水流中来了一个watermark，w3 > t2 > t1。那么定时器T1先触发，然后再到定时器T2触发。
         *
         *  watermark的有效性：
         *      flink认为相同值的watermark是无效的、重复的，是无法触发时间窗口、定时器、timeTrigger等的，甚至不会往下游传递。
         *      watermark的生成必须保持递增性。而watermark的有效性必须是递增且单调的。
         *
         *  先执行processElement方法，再看条件是否触发onTimer执行。
         */
        @Override
        public void onTimer(long timestamp, OnTimerContext ctx, Collector<Tuple2<String, Integer>> out) throws Exception {
            String currentDay = ctx.getCurrentKey().getField(0);
            Integer currentPv = pvCountState.value();

            // 次日触发输出并清除状态
            if (timestamp == pvPurgeState.value()) {
                out.collect(Tuple2.of(currentDay + "_final", currentPv));
                pvCountState.clear();
                pvPurgeState.clear();
                pvFireState.clear();
            }
            // 输出并设置下一次的触发器
            if (null != pvFireState.value() && timestamp == pvFireState.value()) {
                out.collect(Tuple2.of(currentDay, currentPv));
                // 方式一：根据间隔周期性注册定时器（模仿ContinuousEventTimeTrigger的实现）
//                ctx.timerService().registerEventTimeTimer(timestamp + interval);
//                pvFireState.update(timestamp + interval);
                // 方式二：根据事件所携带的时间戳来决定下一次触发时间，类似断点式触发
                pvFireState.clear();
            }
        }
    }


    static class EventTimeExtractor implements AssignerWithPeriodicWatermarks<Tuple2<String, Long>> {
        private long currentMaxTimestamp = Long.MIN_VALUE;

        @Nullable
        @Override
        public Watermark getCurrentWatermark() {
            // 这里不设置延迟时间
            return new Watermark(currentMaxTimestamp);
        }

        @Override
        public long extractTimestamp(Tuple2<String, Long> element, long l) {
            currentMaxTimestamp = Math.max(element.f1, currentMaxTimestamp);
            return element.f1;
        }
    }
}
