package com.project.zeye.network;

import com.project.zeye.beans.ApacheLogEvent;
import com.project.zeye.beans.UrlViewCount;
import com.project.zeye.utils.Utils;
import org.apache.flink.api.common.state.ListState;
import org.apache.flink.api.common.state.ListStateDescriptor;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.shaded.guava18.com.google.common.collect.Lists;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import javax.annotation.Nullable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * 热门页面统计
 */
public class HotPageJob {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        env.readTextFile(Utils.EVENT_LOG_PATH)
                .map(line -> Utils.string2ApacheLogEvent(line))
                .assignTimestampsAndWatermarks(new EventTimeExtractor())
                .keyBy("url")
                .timeWindow(Time.minutes(10), Time.seconds(5))
                // 当算子需要实现的方法中带有Collector<T>，那么就不能用最简的lambda表达式，如果使用稍微简单的lambda，也需要使用returns声明返回流的数据类型
                .apply(new WindowFunction<ApacheLogEvent, UrlViewCount, Tuple, TimeWindow>() {
                    @Override
                    public void apply(Tuple key, TimeWindow timeWindow, Iterable<ApacheLogEvent> iterable, Collector<UrlViewCount> out) throws Exception {
                        int count = Lists.newArrayList(iterable).size();
                        out.collect(new UrlViewCount(key.getField(0), new Long(count), timeWindow.getEnd()));
                    }
                })
                .keyBy("windowEnd")
                .process(new TopNHotPage(5))
                .print();



        env.execute("HotPageJob");
    }


    /**
     * 提取时间戳并注册watermark
     */
    static class EventTimeExtractor implements AssignerWithPeriodicWatermarks<ApacheLogEvent> {
        private long currentMaxTimeStamp = 0;
        private long maxOutOfOrderness = 10 * 1000;    // 延迟时长为10s

        @Nullable
        @Override
        public Watermark getCurrentWatermark() {
            return new Watermark(currentMaxTimeStamp - maxOutOfOrderness);
        }

        @Override
        public long extractTimestamp(ApacheLogEvent logEvent, long preLogEventTimestamp) {
            currentMaxTimeStamp = Math.max(logEvent.getTimestamp(), currentMaxTimeStamp);
            return logEvent.getTimestamp();
        }
    }

    /**
     * 实现TopN逻辑
     */
    static class TopNHotPage extends KeyedProcessFunction<Tuple, UrlViewCount, String> {
        private ListState<UrlViewCount> urlState;
        private int topN;

        public TopNHotPage(int topN) {
            this.topN = topN;
        }

        @Override
        public void open(Configuration parameters) throws Exception {
            urlState = getRuntimeContext().getListState(
                    new ListStateDescriptor<UrlViewCount>("url-state", UrlViewCount.class)
            );
        }

        @Override
        public void processElement(UrlViewCount urlCount, Context ctx, Collector<String> out) throws Exception {
            if (urlState.get() == null) {
                urlState.addAll(Collections.emptyList());
            }
            urlState.add(urlCount);

            ctx.timerService().registerEventTimeTimer(urlCount.getWindowEnd() + 1);
        }

        @Override
        public void onTimer(long timestamp, OnTimerContext ctx, Collector<String> out) throws Exception {
            List<UrlViewCount> urlCountResults = Lists.newArrayList(urlState.get());
            urlState.clear();

            urlCountResults.sort((u1,u2) -> (int) (u2.getCount() - u1.getCount()));

            StringBuilder sb = new StringBuilder();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sb.append("时间：").append(dateFormat.format(new Date(timestamp - 1))).append("\n");

            int num = 0;
            for (UrlViewCount obj : urlCountResults) {
                if (num >= topN) break;
                sb.append("URL：").append(obj.getUrl())
                        .append("  访问量：").append(obj.getCount())
                        .append("\n");
                num++;
            }

            sb.append("======================================================");

            out.collect(sb.toString());
        }
    }
}
