package com.project.zeye.network;

import com.project.zeye.beans.UserBehavior;
import com.project.zeye.utils.Utils;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AssignerWithPunctuatedWatermarks;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.apache.flink.streaming.api.windowing.time.Time;

import javax.annotation.Nullable;

/**
 * 实时统计每小时的pv
 *
 * 思考：(每日更新pv)
     如果想要统计每日的pv，该如何实现。首先我觉得从需求上来讲，实时统计每日pv没必要也不可取。
     因为这个需求完全可以经由离线统计来做，实时作业应该针对的是更小时间粒度的统计。
     从开销上来说，如果开一个时间窗口跨度为1天，那么需要存储的状态会非常大，对内存的压力是个不小的考验；而且实时作业不同于离线任务，是需要一直占用内存和计算资源的，这会影响集群的其他任务的运行。

    如果从技术角度来说，应该如何实现？（实践验证：com.project.zeye.network.CountDailyPvJob）
        简单的方法可以开一个跨度为1天的时间窗口，通过简单sum即可得出，如果要输出窗口信息，那么可以用aggregate算子，传入聚合函数和窗口函数的实现。
        但是时间跨度为1天的时间窗只会在每天凌晨触发一次，而且存储的状态量较大。
        在Flink的Streming API中，可以借助定时器 + 状态实现一个自定义的TimeWindow：
        使用keyBy + process算子，在ProcessFunction中，定义一个ValueState存储pv的累加结果，每来一条数据则迭代计算一次；然后设置两个定时器并定义两个ValueState分别存储这两个定时器的触发时间，一个是等到次日凌晨的0点触发输出并清除状态，另一个则是每间隔一小段时间比如10秒触发一次输出。
        如此即可实现统计每日的pv，并且能够实时地展示更新，存储的状态量也不大，比较的灵活。

    刚才说到自定义窗口的思路，Flink源码中有没类似的案例或者在其他需求实现的时候有没有相同的体会？
        实现TopN排序统计热门商品/品种/页面的需求，实现思路是在第一次通过时间窗口得到每小时所有商品的浏览量后，需要根据窗口分组，对上游时间窗口的计算结果做一次排序并选取出TopN个元素。
        所以使用了keyBy根据窗口结束时间分组，然后使用process算子，在ProcessFunction中，定义一个ListState存储上游窗口的计算结果，然后设置定时器，等到该次窗口结束后的1ms再执行排序求TopN的逻辑，
        这里的定时器 + 状态我理解成其实是相当于二次开窗聚合，然后对这些聚合的数据也即第一次窗口聚合后的计算结果，进行排序取TopN。
        在Flink源码中，计数窗口就是用triggle触发器（四种触发策略）和state状态来实现的。如果需要实现滑动窗口，还可以使用evictor驱逐器来控制。时间窗口则也是通过在上下文中注册定时器timer以及配合state状态来实现的。
 */
public class PageViewJob {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        env.setParallelism(1);

        env.readTextFile(Utils.USER_BEHAVIOR_LOG_PATH)                              //读数据
                .map(line -> Utils.string2UserBehavior(line))                       //解析数据
                .filter(userBehavior -> "P".equals(userBehavior.getBehavior()))     //过滤出来page view的行为数据
                .assignTimestampsAndWatermarks(new EventTimeExtractor())            //设置水位
                .map(userBehavior -> Tuple2.of("pv", 1L))                           //单词计数，让每个pv出现一次
                .returns(Types.TUPLE(Types.STRING, Types.LONG))
                .timeWindowAll(Time.hours(1))                                       //滚动窗口，统计每小时的数据
                .sum(1)                                              //计算结果
                .print();


        env.execute("PageViewJob");
    }


    static class EventTimeExtractor implements AssignerWithPunctuatedWatermarks<UserBehavior> {
        private long currentMaxTimestamp = 0;
        private long maxOutOfOrderness = 10 * 1000;

        @Nullable
        @Override
        public Watermark checkAndGetNextWatermark(UserBehavior userBehavior, long curTs) {
            currentMaxTimestamp = Math.max(curTs, currentMaxTimestamp);
            return new Watermark(currentMaxTimestamp - maxOutOfOrderness);
        }

        @Override
        public long extractTimestamp(UserBehavior userBehavior, long preTs) {
            return userBehavior.getTimeStamp() * 1000;
        }
    }
}
