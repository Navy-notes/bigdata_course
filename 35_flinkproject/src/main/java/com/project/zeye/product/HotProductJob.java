package com.project.zeye.product;

import com.project.zeye.beans.ProductViewCount;
import com.project.zeye.beans.UserBehavior;
import com.project.zeye.utils.Utils;
import org.apache.flink.api.common.functions.AggregateFunction;
import org.apache.flink.api.common.state.ListState;
import org.apache.flink.api.common.state.ListStateDescriptor;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.shaded.guava18.com.google.common.collect.Lists;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import javax.annotation.Nullable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

/**
 * 热门商品统计
 */
public class HotProductJob {
    public static void main(String[] args) throws Exception {
        // 创建环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        // 对接Kafka的topic话，分区数多少那么并行度需要对应上，大topic通常在20个左右
        env.setParallelism(1);

        // 配置参数
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
//        env.setStateBackend(new FsStateBackend("hdfs://..."));
        env.enableCheckpointing(60 * 1000);
        env.getCheckpointConfig().setCheckpointTimeout(120 * 1000);
        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);

        // 处理数据
        env.readTextFile(Utils.USER_BEHAVIOR_LOG_PATH)                              // 获取数据
                .map(line -> Utils.string2UserBehavior(line))                       // 解析数据
//                .returns(Types.POJO(UserBehavior.class))                          // 除了flatMap以外，其他的算子比如map、filter使用lambda表达式且返回的数据类型不是元组的话都不必须要使用 returns 算子
                .assignTimestampsAndWatermarks(new EventTimeExtractor())            // 指定watermark
                .filter(userBehavior -> "P".equals(userBehavior.getBehavior()))     // 过滤用户行为数据
                .keyBy("productId")                                         // 按商品进行分类
                .timeWindow(Time.hours(1), Time.minutes(5))                         // 设置窗口
                .aggregate(new CountProduct(), new WindowResult())                  // 计算窗口数据
                .keyBy("windowEnd")                                         // 按照窗口进行分组
                .process(new TopHotProduct(3))
                .print();


        // 执行作业
        env.execute("HotProductJob");
    }


    /**
     * 里面实现求Topn逻辑
     * K,固定为Tuple类型
     * I,输入的数据类型
     * O,输出的数据类型
     *
     * @param topN
     */
    static class TopHotProduct extends KeyedProcessFunction<Tuple, ProductViewCount, String> {
        // 存储上游窗口计算后的结果数据（state一定不能为静态变量）
        private ListState<ProductViewCount> productCountResults;
        // 自定义选取排序的结果数
        private static int topN;

        public TopHotProduct(int topN) {
            this.topN = topN;
            // 注意：不能在构造方法这里初始化状态...
        }

        @Override
        public void open(Configuration parameters) throws Exception {
            // 初始化状态
            productCountResults = getRuntimeContext().getListState(
                    new ListStateDescriptor<ProductViewCount>("product-state", ProductViewCount.class)
            );
        }

        @Override
        public void processElement(ProductViewCount elememt, Context ctx, Collector<String> out) throws Exception {
            if (productCountResults.get() == null) {
                productCountResults.addAll(Collections.emptyList());
            }
            productCountResults.add(elememt);

            // 注册一个定时器，等上游窗口所有的计算结果都存储到state后再触发
            ctx.timerService().registerEventTimeTimer(elememt.getWindowEnd() + 1);
        }

        /**
         * 定时器里面实现排序的功能
         */
        @Override
        public void onTimer(long timestamp, OnTimerContext ctx, Collector<String> out) throws Exception {
            // 获取上游窗口所有的计算结果
            ArrayList<ProductViewCount> productList = Lists.newArrayList(productCountResults.get());

            // 清空缓存，释放内存压力
            productCountResults.clear();

            // 降序排序  lambda表达式实现 Comparator 接口
            productList.sort((p1, p2) -> (int) (p2.getCount() - p1.getCount()));

            // 取 topN 个作为结果返回
            StringBuilder sb = new StringBuilder(100);
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sb.append("时间：").append(dateFormat.format(new Date(timestamp - 1))).append("\n");
            for (int i = 0; i < topN; i++) {
                sb.append("商品ID：").append(productList.get(i).getProductId())
                        .append(" 商品浏览量=").append(productList.get(i).getCount())
                        .append("\n");
            }
            sb.append("=========================================================");

            out.collect(sb.toString());
        }
    }

    /**
     * IN   输入的数据类型
     * OUT  输出的数据类型
     * KEY  固定为Tuple类型
     * W    窗口类型
     */
    static class WindowResult implements WindowFunction<Long, ProductViewCount, Tuple, TimeWindow> {

        @Override
        public void apply(Tuple key, TimeWindow timeWindow, Iterable<Long> iterable, Collector<ProductViewCount> out) throws Exception {
            out.collect(new ProductViewCount(key.getField(0), iterable.iterator().next(), timeWindow.getEnd()));
        }
    }

    /**
     * IN 输入的数据类型
     * OUT 输出的数据类型
     * ACC 辅助变量的数据类型
     */
    static class CountProduct implements AggregateFunction<UserBehavior, Long, Long> {

        @Override
        public Long createAccumulator() {
            return 0L;
        }

        @Override
        public Long add(UserBehavior userBehavior, Long acc) {
            return acc + 1;
        }

        @Override
        public Long getResult(Long acc) {
            return acc;
        }

        @Override
        public Long merge(Long acc1, Long acc2) {
            return acc1 + acc2;
        }
    }

    /**
     * 提取时间戳并注册watermark
     */
    static class EventTimeExtractor implements AssignerWithPeriodicWatermarks<UserBehavior> {
        private long currentMaxTimeStamp = 0;
        private long maxOutOfOrderness = 10;    // 延迟时长为10s

        @Nullable
        @Override
        public Watermark getCurrentWatermark() {
            return new Watermark((currentMaxTimeStamp - maxOutOfOrderness) * 1000);
        }

        @Override
        public long extractTimestamp(UserBehavior obj, long l) {
            currentMaxTimeStamp = Math.max(obj.getTimeStamp(), currentMaxTimeStamp);
            return obj.getTimeStamp() * 1000;
        }
    }
}
