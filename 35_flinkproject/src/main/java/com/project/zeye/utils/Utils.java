package com.project.zeye.utils;

import com.project.zeye.beans.AdClickEvent;
import com.project.zeye.beans.ApacheLogEvent;
import com.project.zeye.beans.LoginEvent;
import com.project.zeye.beans.OrderInfo;
import com.project.zeye.beans.UserBehavior;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/11/12 21:51
 */
public class Utils {
    /**
     * 用户行为日志数据路径
     */
    public static String USER_BEHAVIOR_LOG_PATH = "F:\\IDEAProjects\\bigdata_course\\35_flinkproject\\src\\main\\resources\\data1.csv";

    /**
     * 服务日志路径
     */
    public static String EVENT_LOG_PATH = "F:\\IDEAProjects\\bigdata_course\\35_flinkproject\\src\\main\\resources\\data2.log";

    /**
     * 广告点击日志路径
     */
    public static String AD_CLICK_LOG_PATH = "F:\\IDEAProjects\\bigdata_course\\35_flinkproject\\src\\main\\resources\\data3.csv";

    /**
     * 订单日志路径
     */
    public static String ORDER_INFO_LOG_PATH = "F:\\IDEAProjects\\bigdata_course\\35_flinkproject\\src\\main\\resources\\test";

    /**
     * 用户登录日志路径
     */
    public static String USER_LOGIN_LOG_PATH = "F:\\IDEAProjects\\bigdata_course\\35_flinkproject\\src\\main\\resources\\data4.csv";



    /**
     * 根据字符串,把数据转换成为我们的用户行为对象
     * @param line
     * @return
     */
    public static UserBehavior string2UserBehavior(String line) {
        String[] fields = line.split(",");
        return new UserBehavior(
                new Long(fields[0].trim()),
                new Long(fields[1].trim()),
                new Long(fields[2].trim()),
                new String(fields[3].trim()),
                new Long(fields[4].trim()),
                new String(fields[5].trim())
                );
    }

    /**
     * 根据字符串,把数据转换成日志服务数据对象
     * @param line
     * @return
     */
    public static ApacheLogEvent string2ApacheLogEvent(String line) throws ParseException {
        String[] fields = line.split(" ");
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy:HH:mm:ss");
        return new ApacheLogEvent(
                fields[0].trim(),
                fields[1].trim(),
                dateFormat.parse(fields[3].trim()).getTime(),
                fields[5].trim(),
                fields[6].trim()
        );
    }

    /**
     * 根据字符串生成广告点击日志对象
     * @param line
     * @return
     */
    public static AdClickEvent string2ClickEvent(String line) {
        String[] fields = line.split(",");
        return new AdClickEvent(
                new Long(fields[0].trim()),
                new Long(fields[1].trim()),
                fields[2].trim(),
                fields[3].trim(),
                new Long(fields[4].trim())
        );
    }

    /**
     * 根据日志行生成订单实体类
     * @param line
     * @return
     */
    public static OrderInfo string2OrderInfo(String line) throws ParseException {
        String[] fields = line.split("\t");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return new OrderInfo(
                fields[0].trim(),
                new Long(fields[1].trim()),
                new Integer(fields[2].trim()),
                fields[3].trim(),
                fields[4].trim(),
                fields[5].trim(),
                fields[6].trim().split(" ")[0],
                dateFormat.parse(fields[6].trim()).getTime()
        );
    }

    /**
     * 根据字符串生成一个用户登录日志对象
     * @param line
     * @return
     */
    public static LoginEvent string2LoginEvent(String line) {
        String[] fields = line.split(",");
        return new LoginEvent(
                new Long(fields[0].trim()),
                fields[1].trim(),
                fields[2].trim(),
                new Long(fields[3].trim())
        );
    }

}
