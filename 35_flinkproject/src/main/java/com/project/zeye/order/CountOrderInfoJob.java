package com.project.zeye.order;

import com.project.zeye.beans.OrderInfo;
import com.project.zeye.utils.Utils;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.evictors.TimeEvictor;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.triggers.ContinuousEventTimeTrigger;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import javax.annotation.Nullable;
import java.util.Iterator;

/**
 * 实时统计订单数量
 */
public class CountOrderInfoJob {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        /**
         * 运行结果：
         *  (2020-09-18,10)
            (2020-09-19,2)
         */
        env.readTextFile(Utils.ORDER_INFO_LOG_PATH)
        /**
         * 运行结果：
         *  (2020-09-18,7)
            (2020-09-18,9)
            (2020-09-18,10)
            (2020-09-19,2)
         */
//        env.socketTextStream("localhost", 1234)
                .map(line -> Utils.string2OrderInfo(line))
                .assignTimestampsAndWatermarks(new EventTimeExtractor())
                .keyBy("day")                                                  // 这里是因为要使用KeyState + Evictor所以才需要用keyBy，否则不用keyBy也能完成基础的需求计算，用keyBy容易引发数据倾斜的问题
                .window(TumblingEventTimeWindows.of(Time.days(1), Time.hours(16)))     // 统计一天00:00-24:00的整点时间(由于中国是东八区默认一天的整点时间是8:00-24:00)
                .trigger(ContinuousEventTimeTrigger.of(Time.seconds(10)))              // trigger触发不会清除窗口数据，因为它的触发策略是FIRE，但它触发后会清除自己用来保存上一个触发节点时间的ReducingState
                /**
                 * 如果这里设置保留7s大小的窗口数据，而触发窗口计算的eventTime为24s的话，则在计算后只保留(17s,24s]区间的数据，其余的数据全部清除
                 * true表示是否doEvictAfter（默认是false，即在触发计算之前清除窗口特定的数据）
                 * 注:（这里使用驱逐器，在每次触发计算后删除全量聚合窗口中的数据，能够避免由于ContinuousEventTimeTrigger周期性触发但实际数据来源并无事件更新所导致的空转行为。
                      原理是窗口触发计算输出需要满足两个条件：一是watermark大于等于待触发的时间戳，二是窗口中得有数据。所以窗口中没有数据自然无法触发计算和输出）
                */
                .evictor(TimeEvictor.of(Time.seconds(0), true))           // 在触发计算后，删除窗口中现存的所有的数据（这里设置的是只保留size为0的窗口数据）
                .process(new CountOrderInfoFunction())
                .print();

        env.execute("CountOrderInfoJob");
    }


    /**
     * 实时累加订单金额
     */
    static class CountOrderInfoFunction extends ProcessWindowFunction<OrderInfo, Tuple2<String, Long>, Tuple, TimeWindow> {
        /**
         * 这里定义state是为了存储在窗口的全量聚合过程中，由于中途不断地有触发输出，所以为了避免冗余计算，节省内存空间和提高效率，
           就把中间的迭代结果保存了下来，并且在每一次触发计算后清除窗口的数据，配合evictor使用。
         */
        private ValueState<Long> countState;

        @Override
        public void open(Configuration parameters) throws Exception {
            countState = getRuntimeContext().getState(new ValueStateDescriptor<>("count-state", Long.class));
        }


        @Override
        public void process(Tuple key, Context ctx, Iterable<OrderInfo> iterable, Collector<Tuple2<String, Long>> out) throws Exception {
            Long count = countState.value();
            if (count == null) {
                count = 0L;
            }

            Iterator<OrderInfo> iterator = iterable.iterator();
            while (iterator.hasNext()) {
                iterator.next();
                count += 1L;
            }
            countState.update(count);

            out.collect(Tuple2.of(key.getField(0), count));
        }
    }


    static class EventTimeExtractor implements AssignerWithPeriodicWatermarks<OrderInfo> {
        private long currentMaxTs = 0;
        private long maxOutOfOrderness = 10 * 1000;

        @Nullable
        @Override
        public Watermark getCurrentWatermark() {
            return new Watermark(currentMaxTs - maxOutOfOrderness);
        }

        @Override
        public long extractTimestamp(OrderInfo orderInfo, long l) {
            currentMaxTs = Math.max(orderInfo.getTimestamp(), currentMaxTs);
            return orderInfo.getTimestamp();
        }
    }

}
