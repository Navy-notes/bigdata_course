package com.project.zeye.order;

import com.project.zeye.beans.OrderInfo;
import com.project.zeye.utils.Utils;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.evictors.TimeEvictor;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.triggers.ContinuousEventTimeTrigger;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import javax.annotation.Nullable;
import java.util.Iterator;

/**
 * 实时统计订单金额
 */
public class CountOrderAmountJob {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        env.setParallelism(1);

        /**
         *  (2020-09-18,591500.0)
            (2020-09-18,870500.0)
            (2020-09-18,870670.0)
            (2020-09-19,340.0)
         */
        env.socketTextStream("localhost", 1234)
//        env.readTextFile(Utils.ORDER_INFO_LOG_PATH)
                .map(line -> Utils.string2OrderInfo(line))
                .assignTimestampsAndWatermarks(new EventTimeExtractor())
                .keyBy("day")
                .window(TumblingEventTimeWindows.of(Time.days(1), Time.hours(16)))
                .evictor(TimeEvictor.of(Time.seconds(0), true))
                .trigger(ContinuousEventTimeTrigger.of(Time.seconds(10)))
                .process(new CountOrderAmountFunction())
                .print();

        env.execute("CountOrderAmountJob");
    }


    /**
     * 实时累加订单金额
     */
    static class CountOrderAmountFunction extends ProcessWindowFunction<OrderInfo, Tuple2<String, Double>, Tuple, TimeWindow> {
        private ValueState<Double> totalAmount;

        @Override
        public void open(Configuration parameters) throws Exception {
            totalAmount = getRuntimeContext().getState(new ValueStateDescriptor<>("totalAmount-state", Double.class));
        }

        @Override
        public void process(Tuple key, Context ctx, Iterable<OrderInfo> iterable, Collector<Tuple2<String, Double>> out) throws Exception {
            Double value = totalAmount.value();
            if (value == null) {
                value = 0.0;
            }

            Iterator<OrderInfo> iterator = iterable.iterator();
            while (iterator.hasNext()) {
                value += iterator.next().getAmount();
            }
            totalAmount.update(value);

            out.collect(Tuple2.of(key.getField(0), value));
        }


    }

    static class EventTimeExtractor implements AssignerWithPeriodicWatermarks<OrderInfo> {
        private long currentMaxTs = 0;
        private long maxOutOfOrderness = 10 * 1000;

        @Nullable
        @Override
        public Watermark getCurrentWatermark() {
            return new Watermark(currentMaxTs - maxOutOfOrderness);
        }

        @Override
        public long extractTimestamp(OrderInfo orderInfo, long l) {
            currentMaxTs = Math.max(orderInfo.getTimestamp(), currentMaxTs);
            return orderInfo.getTimestamp();
        }
    }
}
