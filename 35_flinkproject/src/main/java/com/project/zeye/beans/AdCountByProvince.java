package com.project.zeye.beans;

/**
 * 按照省份统计的输出结果样例类
 */
public class AdCountByProvince {
    private String windowEnd;
    private String province;
    private Long count;

    public AdCountByProvince(String windowEnd, String province, Long count) {
        this.windowEnd = windowEnd;
        this.province = province;
        this.count = count;
    }

    public AdCountByProvince(String windowEnd) {
        this.windowEnd = windowEnd;
    }

    public String getWindowEnd() {
        return windowEnd;
    }

    public void setWindowEnd(String windowEnd) {
        this.windowEnd = windowEnd;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "AdCountByProvince{" +
                "windowEnd='" + windowEnd + '\'' +
                ", province='" + province + '\'' +
                ", count=" + count +
                '}';
    }
}
