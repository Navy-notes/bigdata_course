package com.project.zeye.beans;

/**
 * 热点商品统计的展示结果
 */
public class ProductViewCount {
    private Long productId;
    private Long count;
    private Long windowEnd;

    public ProductViewCount() {
    }

    public ProductViewCount(Long productId, Long count, Long windowEnd) {
        this.productId = productId;
        this.count = count;
        this.windowEnd = windowEnd;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Long getWindowEnd() {
        return windowEnd;
    }

    public void setWindowEnd(Long windowEnd) {
        this.windowEnd = windowEnd;
    }

    @Override
    public String toString() {
        return "ProductViewCount{" +
                "productId=" + productId +
                ", count=" + count +
                ", windowEnd=" + windowEnd +
                '}';
    }

//    // 降序排序
//    @Override
//    public int compareTo(ProductViewCount o) {
//        return (int) (o.count - this.count);
//    }
}
