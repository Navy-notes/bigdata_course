package com.project.zeye.beans;

/**
 * 用户登录事件
 */
public class LoginEvent {
    private Long userId;
    private String ip;
    private String eventType;   // success or fail
    private Long timestamp;

    public LoginEvent(Long userId, String ip, String eventType, Long timestamp) {
        this.userId = userId;
        this.ip = ip;
        this.eventType = eventType;
        this.timestamp = timestamp;
    }

    public LoginEvent() {
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }
}
