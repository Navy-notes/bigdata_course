package com.project.zeye.beans;

/**
 * 输出的黑名单报警信息
 */
public class BlackListWarning {
    private Long userId;
    private Long adId;
    private String message;

    public BlackListWarning() {
    }

    public BlackListWarning(Long userId, Long adId, String message) {
        this.userId = userId;
        this.adId = adId;
        this.message = message;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getAdId() {
        return adId;
    }

    public void setAdId(Long adId) {
        this.adId = adId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "BlackListWarning{" +
                "userId=" + userId +
                ", adId=" + adId +
                ", message='" + message + '\'' +
                '}';
    }
}
