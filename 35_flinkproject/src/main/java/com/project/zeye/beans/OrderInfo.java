package com.project.zeye.beans;

/**
 * 订单实体类
 */
public class OrderInfo {
    private String orderId;
    private Long amount;            // 金额
    private Integer status;         // 订单状态
    private String userId;
    private String payWay;          // 支付方式
    private String paySerialNumber; // 支付流水号
    private String day;
    private Long timestamp;

    public OrderInfo() {
    }

    public OrderInfo(String orderId, Long amount, Integer status, String userId, String payWay, String paySerialNumber, String day, Long timestamp) {
        this.orderId = orderId;
        this.amount = amount;
        this.status = status;
        this.userId = userId;
        this.payWay = payWay;
        this.paySerialNumber = paySerialNumber;
        this.day = day;
        this.timestamp = timestamp;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getPayWay() {
        return payWay;
    }

    public void setPayWay(String payWay) {
        this.payWay = payWay;
    }

    public String getPaySerialNumber() {
        return paySerialNumber;
    }

    public void setPaySerialNumber(String paySerialNumber) {
        this.paySerialNumber = paySerialNumber;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }
}
