package cn.nx.tags

import cn.nx.beans.Logs
import org.apache.commons.lang.StringUtils

/**
  * 给app打标签
  */
object Tags4App extends Tags {
  
  override def makeTags(args: Any*): Map[String, Int] = {
    var map = Map[String, Int]()
    if (args.length > 1) {
      val log = args(0).asInstanceOf[Logs]
      val appDict = args(1).asInstanceOf[Map[String, String]]
      val appName = appDict.getOrElse(log.appid, log.appname)
      if (StringUtils.isNotBlank(appName)) {
        map += ("APP" + appName -> 1)
      }
    }
    map
  }
}
