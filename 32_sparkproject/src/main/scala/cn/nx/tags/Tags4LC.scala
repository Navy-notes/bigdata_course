package cn.nx.tags

import cn.nx.beans.Logs

/**
  * 打广告位标签
  */
object Tags4LC extends Tags{

  override def makeTags(args: Any*): Map[String, Int] = {
    var map = Map[String, Int]()
    if (args.length > 0) {
      val log = args(0).asInstanceOf[Logs]
      if (log.adspacetype != null && log.adspacetype != 0) {
        log.adspacetype match {
            case x if x >= 10 => map += ("LC"+x -> 1)
            case x if x < 10 => map += ("LC0"+x -> 1)
        }
      }
    }
    map
  }
}
