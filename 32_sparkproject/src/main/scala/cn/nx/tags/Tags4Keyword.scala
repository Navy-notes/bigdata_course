package cn.nx.tags

import cn.nx.beans.Logs
import org.apache.commons.lang.StringUtils

/**
  * 打关键字标签
  */
object Tags4Keyword extends Tags {
  
  override def makeTags(args: Any*): Map[String, Int] = {
    var map = Map[String, Int]()
    if (args.length > 0) {
      val log = args(0).asInstanceOf[Logs]
      if (StringUtils.isNotBlank(log.keywords)) {
        val fields = log.keywords.split("\\|")
//        for (word <- fields) {
//          if (word.length >= 3 && word.length <= 8) {
//            map += ("K".concat(word) -> 1)
//          }
//        }
        fields.filter(word => word.length >= 3 && word.length <= 8)
          .map(word => map += ("K".concat(word.replace(":", "")) -> 1))
      }
    }
    map
  }
}
