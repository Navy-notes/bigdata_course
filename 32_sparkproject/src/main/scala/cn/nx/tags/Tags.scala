package cn.nx.tags


/**
  * 标签接口规范
  */
trait Tags {
  /**
    * 打标签的方法
    * @param args
    * @return
    */
  def makeTags(args:Any*): Map[String, Int]
}
