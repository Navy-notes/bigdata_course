package cn.nx.tags

import cn.nx.beans.Logs

/**
  * 打设备标签
  */
object Tags4Device extends Tags {

  override def makeTags(args: Any*): Map[String, Int] = {
    var map = Map[String, Int]()
    if (args.length > 1) {
      val log = args(0).asInstanceOf[Logs]
      val deviceDict = args(1).asInstanceOf[Map[String, String]]
      // 操作系统标签
      val os = deviceDict.getOrElse(log.client.toString, deviceDict.get("4").get)
      map += (os -> 1)

      // 联网方式
      val network = deviceDict.getOrElse(log.networkmannername, deviceDict.get("NETWORKOTHER").get)
      map += (network -> 1)

      // 运营商标签
      val isp = deviceDict.getOrElse(log.ispname, deviceDict.get("OPERATOROTHER").get)
      map += (isp -> 1)
    }
    map
  }
}
