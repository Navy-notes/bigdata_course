package cn.nx.tags

import cn.nx.beans.Logs
import org.apache.commons.lang.StringUtils

/**
  * 打地域标签
  */
object Tags4Area extends Tags {
  override def makeTags(args: Any*): Map[String, Int] = {
    var map = Map[String, Int]()
    if (args.length > 0) {
      val log = args(0).asInstanceOf[Logs]
      if (StringUtils.isNotBlank(log.provincename)) {
        map += ("ZP".concat(log.provincename) -> 1)
      }
      if (StringUtils.isNotBlank(log.cityname)) {
        map += ("ZC" + log.cityname -> 1)
      }
    }
    map
  }
}
