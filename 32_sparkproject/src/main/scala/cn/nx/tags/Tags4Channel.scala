package cn.nx.tags

import cn.nx.beans.Logs
import org.apache.commons.lang.StringUtils

/**
  * 打渠道标签
  */
object Tags4Channel extends Tags {
  
  override def makeTags(args: Any*): Map[String, Int] = {
    var map = Map[String, Int]()
    if (args.length > 0) {
      val log = args(0).asInstanceOf[Logs]
      if (StringUtils.isNotBlank(log.channelid)) {
        map += ("CN".concat(log.channelid) -> 1)
      }
    }
    map
  }
}
