package cn.nx.tags

import cn.nx.beans.Logs
import cn.nx.utils.Utils
import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}



/**
  * 简单合并上下文标签
  * (根据相同用户ID的标签数据进行聚合)
  * 参数：
      F:\IDEAProjects\bigdata_course\32_sparkproject\src\main\resources\data.txt
      F:\IDEAProjects\bigdata_course\32_sparkproject\src\main\resources\appmapping.txt
      F:\IDEAProjects\bigdata_course\32_sparkproject\src\main\resources\device_mapping.txt
      F:\IDEAProjects\bigdata_course\32_sparkproject\src\main\resources\outdata\tagsContext

  * 业务逻辑，参考文档32_sparkproject/src/main/document/用户画像需求文档.doc
  */
object TagsContext {
  def main(args: Array[String]): Unit = {
    // 日志设置
    Logger.getLogger("org").setLevel(Level.OFF)
    System.setProperty("spark.ui.showConsoleProgress","false")

    // 参数判断
    if (args.length < 4) {
      println(
        """
          |cn.nx.tags.TagsContext
          |<inputLogPath> 输入日志文件的路径
          |<appMappingPath> app映射文件路径
          |<deviceMappingPath> 设备的映射文件路径
          |<outputPath> 输出路径
        """.stripMargin)
      System.exit(0)
    }

    // 接收传参
    val Array(inputLogPath, appMappingPath, deviceMappingPath, outputPath) = args

    // 构建spark执行环境
    val conf = new SparkConf().setAppName(s"${this.getClass.getSimpleName}").setMaster("local")
    conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    conf.registerKryoClasses(Array(classOf[Logs]))
    val sc = new SparkContext(conf)


    // 由于并行架构，需要生成App广播变量
    val appMap = sc.textFile(appMappingPath).flatMap(line => {
      var map = Map[String, String]()
      val fields = line.split("\t")
      if (fields.length > 4) {
        map += (fields(4) -> fields(1))
      }
      map
    }).collect().toMap    // 从文件读取数据然后在本地内存中缓存Map结构的数据
    val appMapBroadcast = sc.broadcast(appMap)


    // 同理，生成Device广播变量
    val deviceMap = sc.textFile(deviceMappingPath).map(line => {
      var map = Map[String, String]()
      val fields = line.split("\t")
      if (fields.length > 1) {
        map += (fields(0) -> fields(1))
      }
      map
    }).collect().flatten.toMap
    val deviceMapBroadcast = sc.broadcast(deviceMap)


    // 读取日志，打标签并合并
    sc.textFile(inputLogPath).map(line => {
      val log = Logs.line2Logs(line)
      val lcTag = Tags4LC.makeTags(log)
      val appTag = Tags4App.makeTags(log, appMapBroadcast.value)
      val channelTag = Tags4Channel.makeTags(log)
      val deviceTag = Tags4Device.makeTags(log, deviceMapBroadcast.value)
      val keywordTag = Tags4Keyword.makeTags(log)
      val areaTag = Tags4Area.makeTags(log)
      // 返回类型
      val userId = getNotEmptyID(log).getOrElse("")
      (userId, (lcTag ++ appTag ++ channelTag ++ deviceTag ++ keywordTag ++ areaTag).toList)
    }).filter(!_._1.equals(""))
      .reduceByKey((list1, list2) => {
        (list1 ++ list2).groupBy(_._1).map(tuple => {
          (tuple._1, tuple._2.map(_._2).sum)
        }).toList
      })
      .foreach(element => println(element._1 + "->" + element._2.mkString(" ")))

    /**
      * //奈学教学版
    sc.textFile(inputLogPath).map(line=>{
      val log = Logs.line2Logs(line)
      val localTag = Tags4Local.makeTags(log)
      val appTag = Tags4App.makeTags(log,appMapBroadCast.value)
      val channelTag = Tags4Channel.makeTags(log)
      val deviceTag = Tags4Device.makeTags(log, deviceMapBroadCast.value)
      val keyWordsTag = Tags4keyWords.makeTags(log)
      val areaTag = Tags4Area.makeTags(log)

      val userid = getNotEmptyID(log).getOrElse("")

      (userid,(localTag ++ appTag ++ channelTag ++ deviceTag ++ keyWordsTag ++ areaTag).toList)
    }).filter(!_._1.toString.equals(""))
      .reduceByKey{
        case(list1,list2)=>{
          (list1 ++ list2).groupBy(_._1).map{
            case(k,list)=>{
              (k,list.map(t=>t._2).sum)
            }
          }.toList
        }
      }.foreach(tuple =>{
      println(tuple._1 + "->" + tuple._2.mkString("\t"))
    })
      */

    // 释放资源
    sc.stop()
  }


  /**
    * 获取用户唯一不为空的ID
    * @param log
    * @return
    */
  def getNotEmptyID(log: Logs): Option[String] = {
    log match {
      case v if v.imei.nonEmpty => Some("IMEI:" + Utils.formatIMEID(v.imei))
      case v if v.imeimd5.nonEmpty => Some("IMEIMD5:" + v.imeimd5.toUpperCase)
      case v if v.imeisha1.nonEmpty => Some("IMEISHA1:" + v.imeisha1.toUpperCase)

      case v if v.androidid.nonEmpty => Some("ANDROIDID:" + v.androidid.toUpperCase)
      case v if v.androididmd5.nonEmpty => Some("ANDROIDIDMD5:" + v.androididmd5.toUpperCase)
      case v if v.androididsha1.nonEmpty => Some("ANDROIDIDSHA1:" + v.androididsha1.toUpperCase)

      case v if v.mac.nonEmpty => Some("MAC:" + v.mac.replaceAll(":|-", "").toUpperCase)
      case v if v.macmd5.nonEmpty => Some("MACMD5:" + v.macmd5.toUpperCase)
      case v if v.macsha1.nonEmpty => Some("MACSHA1:" + v.macsha1.toUpperCase)

      case v if v.idfa.nonEmpty => Some("IDFA:" + v.idfa.replaceAll(":|-", "").toUpperCase)
      case v if v.idfamd5.nonEmpty => Some("IDFAMD5:" + v.idfamd5.toUpperCase)
      case v if v.idfasha1.nonEmpty => Some("IDFASHA1:" + v.idfasha1.toUpperCase)

      case v if v.openudid.nonEmpty => Some("OPENUDID:" + v.openudid.toUpperCase)
      case v if v.openudidmd5.nonEmpty => Some("OPENDUIDMD5:" + v.openudidmd5.toUpperCase)
      case v if v.openudidsha1.nonEmpty => Some("OPENUDIDSHA1:" + v.openudidsha1.toUpperCase)

      case _ => None
    }
  }

}
