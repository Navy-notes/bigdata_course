package cn.nx.tags.user

import cn.nx.beans.Logs
import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}


/**
  * 根据找到的common_ship聚合用户的所有上下文标签，得到最终的用户画像
  * 输入：标签第一次合并结果.txt
  *       userid_common_ship
  * 打印输出
  */
object UserTags {
  def main(args: Array[String]): Unit = {
    // 日志设置及环境配置
    Logger.getLogger("org").setLevel(Level.OFF)
    System.setProperty("spark.ui.showConsoleProgress","false")

    val conf = new SparkConf().setMaster("local").setAppName("GenerateUniqueIdByGraph")
    conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    conf.registerKryoClasses(Array(classOf[Logs]))

    val sc = new SparkContext(conf)

    val initCombineFilePath = "F:\\IDEAProjects\\bigdata_course\\32_sparkproject\\src\\main\\scala\\cn\\nx\\tags\\user\\标签第一次合并结果.txt"
    val commonShipFilePath = "F:\\IDEAProjects\\bigdata_course\\32_sparkproject\\src\\main\\scala\\cn\\nx\\tags\\user\\userid_common_ship"


    // 读取第一次标签合并的结果文件，并转换数据格式
    val firstCombineData = sc.textFile(initCombineFilePath).map(line => {
      val fields = line.split(" ")
      val tagList = fields(1).split(",").flatMap(tag  => {
        var list = List[(String, Int)]()
        val arr = tag.split(":")
        list ++= List((arr(0), arr(1).toInt))
        list
      }).toList
      (fields(0), tagList)
    })

    // 读取用户id关系文件
    val commonShip = sc.textFile(commonShipFilePath)
      .flatMap(line => {
        val fields = line.split("\t")
        // slice区别于foreach的是，可以指定从下标1的元素开始遍历，跳过首元素
        fields.slice(1, fields.length).map((_,fields(0)))
      })


    // join然后进行第二次合并，即得到最终的用户画像
    firstCombineData.join(commonShip)     //spark的join算子使用的是内连接，使用外连接则可以用leftOuterJoin等算子
      .map {
        case (username, (tagList, uniqueId)) => {
          (uniqueId, tagList)
        }
      }
      .reduceByKey((list1, list2) => {
        (list1 ++ list2).groupBy(_._1).map {
          case (tag, elements) => {
            (tag, elements.map(_._2).sum)
          }
        }.toList
      })
      .foreach(println(_))



    sc.stop()
  }

}
