package cn.nx.tags.user

import cn.nx.beans.Logs
import cn.nx.utils.Utils
import org.apache.commons.lang.StringUtils
import org.apache.log4j.{Level, Logger}
import org.apache.spark.graphx.{GraphLoader, VertexRDD}
import org.apache.spark.{SparkConf, SparkContext}

/**
  * 使用图计算生成userid_common_ship文件：
  *
  *   输入：vertices_username.txt
  *   中间结果：edge_reduceby
  *   输出：userid_common_ship
  */
object GenerateUniqueIdByGraph {
  def main(args: Array[String]): Unit = {
    // 日志设置及环境配置
    Logger.getLogger("org").setLevel(Level.OFF)
    System.setProperty("spark.ui.showConsoleProgress","false")

    val conf = new SparkConf().setMaster("local").setAppName("GenerateUniqueIdByGraph")
    conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    conf.registerKryoClasses(Array(classOf[Logs]))

    val sc = new SparkContext(conf)

    // 获取每条数据里所有的id号，生成vertices_username.txt文件
    /*sc.textFile("F:\\IDEAProjects\\bigdata_course\\32_sparkproject\\src\\main\\resources\\data.txt")
      .map(line => {
        val log = Logs.line2Logs(line)
        val set = getAllUserIdsByLog(log)
        (Math.abs(set.hashCode()), set)
      }).cache()*/

    val verticesFilePath = "F:\\IDEAProjects\\bigdata_course\\32_sparkproject\\src\\main\\scala\\cn\\nx\\tags\\user\\vertices_username.txt"
    val edgeFilePath = "F:\\IDEAProjects\\bigdata_course\\32_sparkproject\\src\\main\\scala\\cn\\nx\\tags\\user\\edge_reduceby"
    val shipFilePath = "F:\\IDEAProjects\\bigdata_course\\32_sparkproject\\src\\main\\scala\\cn\\nx\\tags\\user\\userid_common_ship"

    // 本演示案例中，直接从设计好的vertices_username.tx读取数据并构造(hashId,Set<String>)
    val users = sc.textFile(verticesFilePath)
      .map(line => {
        val fields = line.split(" ")
        val names = fields(1).split(",").flatMap(name => {
          var set = Set[String]()
          if (StringUtils.isNotBlank(name)) set += name
          set
        }).toSet                                                  // 这里需要显示声明返回类型，否则会默认处理成Array[String]
        (Math.abs((fields(0) * 123456).hashCode).toLong, names)   // 这里的key必须为Long类型，不然和VertexRDD联接join的时候会报错
      })
      .cache()

    // 从user中分析得到名字具有关联的边际关系并生成文件edge_reduceby.txt
    users.flatMap{
      case (hashId, names) => {
        names.map((_,hashId.toString))
      }
    }
      // 以name作为key把不同的hashId聚合起来
      .reduceByKey((hashid_1, hashid_2) => hashid_1 + "," + hashid_2)
      .flatMap{
        // 分析每组聚合后的数据，得到边的关系
        case (name, hashids) => {
          val idArr = hashids.split(",")
          // 以每组第一个id作为起始点，连接组内其他顶点(包括自己)
          idArr.map(id => idArr(0).concat(" ").concat(id))     // 这里存储的文件需要符合edgeListFile的格式
        }
      }
      .saveAsTextFile(edgeFilePath)

    // 构建图环境
    val graph = GraphLoader.edgeListFile(sc, edgeFilePath)
    // 找到连通域的点之间的关系
    val relationship = graph.connectedComponents().vertices


    // user join(内连接) relationship，生成用户id关系文件：userid_common_ship.txt
    users.join(relationship).map{
      case (hashId, (names, commonId)) => {
        (commonId, names)
      }
    }.reduceByKey((set1, set2) => set1 ++ set2)
      .map(element => element._1.toString + "\t" + element._2.mkString("\t"))
      .saveAsTextFile(shipFilePath)



    println("job finish!")
    sc.stop()
  }






  /**
    * 获取每条日志所有的用户ID
    *
    * @param log
    * @return
    */
  def getAllUserIdsByLog(log: Logs): Set[String] = {
      //Set内的元素可以去重，两个Set之间合并也会去重
      var ids = Set[String]()

      if (log.imei.nonEmpty)  ids += ("IMEI:" + Utils.formatIMEID(log.imei))
      if (log.imeimd5.nonEmpty) ids += ("IMEIMD5:" + log.imeimd5.toUpperCase)
      if (log.imeisha1.nonEmpty) ids += ("IMEISHA1:" + log.imeisha1.toUpperCase)

      if (log.androidid.nonEmpty) ids += ("ANDROIDID:" + log.androidid.toUpperCase)
      if (log.androididmd5.nonEmpty) ids += ("ANDROIDIDMD5:" + log.androididmd5.toUpperCase)
      if (log.androididsha1.nonEmpty) ids += ("ANDROIDIDSHA1:" + log.androididsha1.toUpperCase)

      if (log.mac.nonEmpty) ids += ("MAC:" + log.mac.replaceAll(":|-", "").toUpperCase)
      if (log.macmd5.nonEmpty) ids += ("MACMD5:" + log.macmd5.toUpperCase)
      if (log.macsha1.nonEmpty) ids += ("MACSHA1:" + log.macsha1.toUpperCase)

      if (log.idfa.nonEmpty) ids += ("IDFA:" + log.idfa.replaceAll(":|-", "").toUpperCase)
      if (log.idfamd5.nonEmpty) ids += ("IDFAMD5:" + log.idfamd5.toUpperCase)
      if (log.idfasha1.nonEmpty) ids += ("IDFASHA1:" + log.idfasha1.toUpperCase)

      if (log.openudid.nonEmpty) ids += ("OPENUDID:" + log.openudid.toUpperCase)
      if (log.openudidmd5.nonEmpty) ids += ("OPENDUIDMD5:" + log.openudidmd5.toUpperCase)
      if (log.openudidsha1.nonEmpty)  ids += ("OPENUDIDSHA1:" + log.openudidsha1.toUpperCase)

      ids
  }

}
