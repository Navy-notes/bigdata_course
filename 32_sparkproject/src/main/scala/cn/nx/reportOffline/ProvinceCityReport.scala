package cn.nx.reportOffline

import cn.nx.beans.Logs
import cn.nx.tools.ReportUtils
import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}

/**
  * 以省份和城市维度汇总报表
  * 传参：
      F:\IDEAProjects\bigdata_course\32_sparkproject\src\main\resources\data.txt
      F:\IDEAProjects\bigdata_course\32_sparkproject\src\main\resources\outdata\provinceReport
      F:\IDEAProjects\bigdata_course\32_sparkproject\src\main\resources\outdata\cityReport
  */
object ProvinceCityReport {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.OFF)
    System.setProperty("spark.ui.showConsoleProgress","false")

    // 1.判断参数个数
    if(args.length < 3){
      println(
        """
          |cn.nx.reportOffline.ProvinceCityReport
          |<logInputPath> 文件输入目录
          |<provinceDataPath> 省份结果文件目录
          |<cityDataPath> 城市结果文件目录
        """.stripMargin)
      System.exit(0)
    }

    // 2.接收参数
    val Array(logInputPath, provinceDataPath, cityDataPath) = args

    // 3.环境参数配置
    val conf = new SparkConf().setAppName(s"${this.getClass.getSimpleName}").setMaster("local")
    conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    conf.registerKryoClasses(Array(classOf[Logs]))

    // 4.构建Spark上下文
    val sc = new SparkContext(conf)

    // 5.读取文件，进行业务逻辑处理
    val provinceCityRDD = sc.textFile(logInputPath).map(line => {
      val log = Logs.line2Logs(line)
      val adRequest = ReportUtils.calculateRequest(log)
      val adResponse = ReportUtils.calculateResponse(log)
      val adClick = ReportUtils.calculateShowClick(log)
      val adCost = ReportUtils.calculateAdCost(log)
      // 列表之间的拼接操作：(1,2,3) ++ (4,5,6)  =>  (1,2,3,4,5,6)
      (log.provincename, log.cityname, adRequest ++ adResponse ++ adClick ++ adCost)
    }).cache()


    // 计算省份维度的指标，输出
    provinceCityRDD.map(tuple => (tuple._1, tuple._3))
      .reduceByKey((list1, list2) => {
        // 列表之间的拉链操作：(1,2,3).zip((4,5,6)) => ((1,4),(2,5),(3,6))
        list1.zip(list2).map(element => element._1 + element._2)
      })
      .foreach(println)


    // 计算城市维度的指标，输出
    provinceCityRDD.map(tuple => {
      (tuple._1 + tuple._2, tuple._3)
    }).reduceByKey{
      case (list1, list2) => {
        list1.zip(list2).map{
          case (x, y) => x + y
        }
      }
    }.foreach(tuple => {
      val provinceAndCityName = tuple._1
      val report = tuple._2.mkString(",")
      println(provinceAndCityName + " " + report)
    })

    // 6.释放资源
    sc.stop()
  }
}
