package cn.nx.reportOffline

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

/**
  * 统计各省市数据量分布
  * 传参：
     F:\IDEAProjects\bigdata_course\32_sparkproject\src\main\resources\outdata\toParquet
     F:\IDEAProjects\bigdata_course\32_sparkproject\src\main\resources\outdata\provinceCityAnalyse
  */
object ProvinceCityAnalyse {
  def main(args: Array[String]): Unit = {
    // 1.判断参数个数
    if(args.length < 2) {
      println("""
        |cn.nx.reportOffline.ProvinceCityAnalyse <inputFilePath><outputFilePath>
        |<inputPath> 输入文件路径
        |<outputPath> 输出文件路径
      """.stripMargin)
      System.exit(0)
    }

    // 2.接受参数
    val Array(inputPath, outputPath) = args

    // 3.添加配置参数
    val conf = new SparkConf().setAppName(s"${this.getClass.getSimpleName}").setMaster("local")

    // 4.构建运行环境
    val spark = SparkSession.builder().config(conf).getOrCreate()

    // 5.读取文件，做逻辑代码开发
    val dataFrame = spark.read.parquet(inputPath)
    dataFrame.createOrReplaceTempView("logs")
    val sqlQuery =
      """
        select count(1),provincename,cityname
        from logs
        group by provincename,cityname
        order by provincename
      """

    // 6.存储结果文件
    spark.sql(sqlQuery).write.json(outputPath)

    // 7.释放资源
    spark.stop()
  }
}
