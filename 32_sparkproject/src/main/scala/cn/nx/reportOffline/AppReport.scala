package cn.nx.reportOffline

import cn.nx.beans.Logs
import cn.nx.tools.ReportUtils
import org.apache.commons.lang.StringUtils
import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}

/**
  * 从APP程序的维度进行报表汇总
  * 传参：
    F:\IDEAProjects\bigdata_course\32_sparkproject\src\main\resources\data.txt
    F:\IDEAProjects\bigdata_course\32_sparkproject\src\main\resources\appmapping.txt
    F:\IDEAProjects\bigdata_course\32_sparkproject\src\main\resources\outdata\appReport
  */
object AppReport {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.OFF)
    System.setProperty("spark.ui.showConsoleProgress","false")

    if (args.length < 3) {
      println(
        """
          |cn.nx.reportOffline.AppReport
          |<logDataPath> 日志目录
          |<appMappingPath> 映射文件目录
          |<outputPath> 输出结果文件目录
        """.stripMargin)
      System.exit(0)
    }

    val Array(logDataPath, appMappingPath, outputPath) = args

    val conf = new SparkConf().setAppName(s"${this.getClass.getSimpleName}").setMaster("local")
    conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    conf.registerKryoClasses(Array(classOf[Logs]))

    val sc = new SparkContext(conf)

    // 获取映射文件作为广播变量
    val appMap = sc.textFile(appMappingPath).flatMap(line => {
      var map = Map[String, String]()
      val fields = line.split("\t")
      map += (fields(4) -> fields(1))
      map
    }).collect().toMap

    val broadcastAppMap = sc.broadcast(appMap)

    // 生成app报表
    sc.textFile(logDataPath)
      .map(line => {
        val log = Logs.line2Logs(line)

        val adRequest = ReportUtils.calculateRequest(log)
        val adResponse = ReportUtils.calculateResponse(log)
        val adClick = ReportUtils.calculateShowClick(log)
        val adCost = ReportUtils.calculateAdCost(log)

        val appName = broadcastAppMap.value.getOrElse(log.appid, log.appname)
        (appName, adRequest ++ adResponse ++ adClick ++ adCost)
      })
      .filter(x => StringUtils.isNotBlank(x._1))
      .reduceByKey((list1, list2) => {
        list1.zip(list2).map(element => element._1 + element._2)
      })
      .foreach(x => println(x._1 + " " + x._2.mkString(",")))


    sc.stop()
  }
}
