package cn.nx.tools

import cn.nx.beans.Logs
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

/**
  * Spark相关工具类
  * 传参：
     F:\IDEAProjects\bigdata_course\32_sparkproject\src\main\resources\data.txt
     F:\IDEAProjects\bigdata_course\32_sparkproject\src\main\resources\outdata\toParquet
     gzip
  */
object Tools {
  def main(args: Array[String]): Unit = {
    // 1.判断传入参数个数
    if(args.length < 3){
      println(
        """
          |cn.nx.tools.Tools <dataPath><outputPath><compressionCode>
          |<dataPath> 日志所在路径
          |<outputPath> 结果文件存放的路径
          |<compressionCode> 指定的压缩格式
        """.stripMargin)
      System.exit(0)
    }

    // 2.接受参数
    val Array(dataPath, outputPath, compressionCode) = args

    // 3.配置序列化方式以及压缩格式（spark2.x的默认数据压缩格式是snappy）
    val conf = new SparkConf()
    conf.setAppName(s"${this.getClass.getSimpleName}")
    conf.setMaster("local")
    conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    conf.registerKryoClasses(Array(classOf[Logs]))
//    conf.set("spark.io.compression.codec", "gzip")                  // 这种配置方式运行会报错，表示不支持gzip压缩
    conf.set("spark.sql.parquet.compression.codec", compressionCode)  // 这行参数支持gzip压缩

    // 4.创建SparkSession对象
     val ssc = SparkSession.builder().config(conf).getOrCreate()

    // ***sparkSession加载文件，load。 如果这样读取然后直接转parquet文件的话，由于csv没有schema信息，转parquet时则会采用默认字段名_c0,_c1...来配置
//    val dataFrame = ssc.read.csv(dataPath)

    // 5.用sparkContext读取文件并转换得到Logs格式的RDD，然后再转成DataFrame
    val rddLogs = ssc.sparkContext.textFile(dataPath).map(Logs.line2Logs(_))
    val dataFrame = ssc.createDataFrame(rddLogs)

    // 6.将结果文件以parquet的格式存储，save
    dataFrame.write.parquet(outputPath)

    // 7.释放资源
    ssc.stop()
  }

}
