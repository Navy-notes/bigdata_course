package cn.nx.graphtest.teaching

import cn.nx.beans.Logs
import org.apache.spark.{SparkConf, SparkContext}

object UserTags {
  def main(args: Array[String]): Unit = {
    //1.判断参数
//    if(args.length<3){
//      println(
//        """
//          |cn.nx.tags.user.UserTags
//          |<inputTagsPath> 上下文初次合并的标签结果
//          |<shipPath> 用户id关系文件路径
//          |<resultTagsOutputPath> z最终结果输出路径
//          |""".stripMargin)
//      System.exit(0)
//    }
//    //2.接收参数
//    val Array(inputTagsPath,shipPath,resultTagsOutputPath)=args
    //3.创建程序入口
    val conf = new SparkConf()
    conf.setMaster("local")
    conf.setAppName(s"${this.getClass.getSimpleName}")
    conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    conf.registerKryoClasses(Array(classOf[Logs]))
    val sc = new SparkContext(conf)

    //4.读取昨天的上下文合并的数据
    val ut = sc.textFile("C:\\Users\\MuchM\\Desktop\\NX\\授课\\系统课\\项目课程\\用户画像DMP系统\\三节课\\资料\\上下文标签数据.txt")
      .map(line => {
        val fields = line.split("\t")
        // slice类似foreach()，这里的意思是指定从下标1的元素开始遍历，跳过首元素。
        val tags = fields.slice(1, fields.length).flatMap(kv => {
          var map = Map[String, Int]()
          val tkv = kv.split(":")
          map += (tkv(0) -> tkv(1).toInt)
          map
        })
        (fields(0), tags.toList)
      })

    //5.读取用户id关系文件
    val us = sc.textFile("C:\\Users\\MuchM\\Desktop\\NX\\授课\\系统课\\项目课程\\用户画像DMP系统\\三节课\\资料\\ship.txt")
      .flatMap(line => {
        val fields = line.split("\t")
        fields.slice(1, fields.length).map(t => (t, fields(0)))
      })

    //6.合并
    //(李希沅,(9527,((D00020005,2),(APP马上赚,2))))
    us.join(ut)
      .map{
        case(uid,(hashCode,tags)) =>(hashCode,tags)
      }.reduceByKey{
      case(list1,list2)=>{
        (list1 ++ list2).groupBy(_._1).map{
          case(tk,stk) =>{
            val sum = stk.map(_._2).sum
            (tk,sum)
          }
        }.toList
      }
    }.foreach(println(_))

    //7.释放资源
    sc.stop()

  }

}
