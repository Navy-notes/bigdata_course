package cn.nx.graphtest.teaching

import cn.nx.beans.Logs
import org.apache.spark.graphx.GraphLoader
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.mutable.ArrayBuffer

/**
 * 生成用户id关系文件
 */

object UserGraph {
  def main(args: Array[String]): Unit = {
    //1.判断参数
//    if (args.length < 3) {
//      println(
//        """
//          |cn.nx.tags.user.UserGraph
//          |<inputDataPath> 输入的数据路径
//          |<followersOutputPath> followers文件的存储目录
//          |<useridShipPath> 用户id关系文件目录
//           """.stripMargin)
//      System.exit(0)
//    }
    //2.接收参数
//    val Array(inputDataPath, followersOutputPath, useridShipPath) = args
    //3.创建程序入口
    val conf = new SparkConf()
    conf.setMaster("local")
    conf.setAppName(s"${this.getClass.getSimpleName}")
    conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    conf.registerKryoClasses(Array(classOf[Logs]))
    val sc = new SparkContext(conf)

    //4.获取每条数据里面所有的id号
    //    sc.textFile(inputDataPath).map(line=>{
    //      val log = Logs.line2Logs(line)
    //      (Math.abs(getUserIds(log).hashCode()),getUserIds(log))
    //    })
    //.cache()
    //1)
    val array = Array(
      Tuple2(1L, Set("帅哥", "李希沅")),
      Tuple2(2L, Set("美男子", "李希沅")),
      Tuple2(3L, Set("李希沅"))
    )

    val users = sc.parallelize(array)

    //2)
    users.flatMap(tuple =>{
      tuple._2.map(id =>(id,tuple._1.toString))  //帅哥,1
    })

    //3)
      .reduceByKey((a:String,b:String) => a.concat(",").concat(b))
    //4)
      .flatMap(t=>{
        val ids = t._2.split(",")
        val ships = new ArrayBuffer[(String, String)]()
        if(ids.size ==1){
          ships += Tuple2(ids(0),ids(0))
        }else {
          ids.map(id =>ships += Tuple2(ids(0),id))
        }
        ships
      }).foreach(println(_))
    //saveAsTextFile(myfollower)

    //5.生成用户id关系文件
    val grapxh = GraphLoader.edgeListFile(sc,"C:\\Users\\MuchM\\Desktop\\NX\\授课\\系统课\\项目课程\\用户画像DMP系统\\三节课\\资料\\myfollower.txt")
    val cc = grapxh.connectedComponents().vertices
    cc.foreach(println(_))

    /**
     * (1,1)
     * (3,1)
     * (2,1)
     */
    /**
     * Tuple2(1L, Set("帅哥", "李希沅")),
     * Tuple2(2L, Set("美男子", "李希沅")),
     * Tuple2(3L, Set("李希沅"))
     * join
     * (1,1)
     * (3,1)
     * (2,1)
     */
    //1,(("帅哥","李希沅"),1)
    //spark的join算子使用的是内连接
    users.join(cc).map{
      case(id,(names,minID))=>{
        (minID,names)
      }
    }.reduceByKey(_++_)   // 两个Set之间合并也自带去重效果
        .foreach(tuple =>println(Math.abs(tuple._2.hashCode()) +"->" +tuple._2.mkString(",")))


    //6.释放资源
    sc.stop()


  }

  //获取所有的用户id
  def getUserIds(log: Logs): Set[String] = {
    var ids = Set[String]()
    if (log.imei.nonEmpty) ids ++= Set("imei" + log.imei.toUpperCase())
    if (log.imeimd5.nonEmpty) ids ++= Set("IMEIMD5" + log.imeimd5.toUpperCase())
    if (log.imeisha1.nonEmpty) ids ++= Set("IMEISHA1" + log.imeisha1.toUpperCase())

    if (log.androidid.nonEmpty) ids ++= Set("ANDROIDID:" + log.androidid.toUpperCase)
    if (log.androididmd5.nonEmpty) ids ++= Set("ANDROIDIDMD5:" + log.androididmd5.toUpperCase)
    if (log.androididsha1.nonEmpty) ids ++= Set("ANDROIDIDSHA1:" + log.androididsha1.toUpperCase)

    if (log.idfa.nonEmpty) ids ++= Set("IDFA:" + log.idfa.toUpperCase)
    if (log.idfamd5.nonEmpty) ids ++= Set("IDFAMD5:" + log.idfamd5.toUpperCase)
    if (log.idfasha1.nonEmpty) ids ++= Set("IDFASHA1:" + log.idfasha1.toUpperCase)

    if (log.mac.nonEmpty) ids ++= Set("MAC:" + log.mac.toUpperCase)
    if (log.macmd5.nonEmpty) ids ++= Set("MACMD5:" + log.macmd5.toUpperCase)
    if (log.macsha1.nonEmpty) ids ++= Set("MACSHA1:" + log.macsha1.toUpperCase)

    if (log.openudid.nonEmpty) ids ++= Set("OPENUDID:" + log.openudid.toUpperCase)
    if (log.openudidmd5.nonEmpty) ids ++= Set("OPENUDIDMD5:" + log.openudidmd5.toUpperCase)
    if (log.openudidsha1.nonEmpty) ids ++= Set("OPENUDIDSHA1:" + log.openudidsha1.toUpperCase)
    ids
  }
}
