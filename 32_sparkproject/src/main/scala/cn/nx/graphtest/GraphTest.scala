package cn.nx.graphtest

import org.apache.log4j.{Level, Logger}
import org.apache.spark.graphx.{Edge, Graph}
import org.apache.spark.{SparkConf, SparkContext}

/**
  * spark图计算
  */
object GraphTest {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.OFF)
    System.setProperty("spark.ui.showConsoleProgress","false")

    val conf = new SparkConf().setAppName("GraphTest").setMaster("local")
    var sc = new SparkContext(conf)

    // 构建顶点
    val users = Array(
      (3L, ("rxin", "student")),
      (7L, ("jgonzal", "postdoc")),
      (5L, ("franklin", "prof")),
      (2L, ("istoica", "prof"))
    )
    val vertices = sc.parallelize(users, 1)         //rowRDD

    // 构建边
    val relationships = Array(
      Edge(3L, 7L, "collab"),
      Edge(5L, 3L, "advisor"),
      Edge(2L, 5L, "colleague"),
      Edge(5L, 7L, "pi")
    )
    val edges = sc.parallelize(relationships, 1)    //schema

    // 根据顶点和边绘图
    val graph = Graph(vertices, edges)

    // 输出所有的顶点元素
    graph.vertices.foreach(println)
    // 输出所有的边
    graph.edges.foreach(println)

    // 输出(无向图)连通域的点关联
    graph.connectedComponents().vertices.foreach(println)



    // 过滤特定的顶点
    println(
      graph.vertices.filter {
        case (id, (name, job)) => {
          job == "prof"
        }
      }.count()
    )

    // 过滤特定的边
    println(
      graph.edges.filter(edge => edge.srcId > edge.dstId).count()
    )


    sc.stop()
  }
}
