package cn.nx.graphtest

import org.apache.log4j.{Level, Logger}
import org.apache.spark.graphx.GraphLoader
import org.apache.spark.{SparkConf, SparkContext}

/**
  * 图计算好友关系
  */
object ConnectedComponentsExample {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.OFF)
    System.setProperty("spark.ui.showConsoleProgress","false")

    //graphx 基于RDD
    val conf = new SparkConf()
    conf.setMaster("local")
    conf.setAppName("ConnectedComponentsExample")
    val sc = new SparkContext(conf)

    //构建出来图有多种方式
    val grapxh = GraphLoader.edgeListFile(sc,"F:\\IDEAProjects\\bigdata_course\\32_sparkproject\\src\\main\\resources\\followers.txt")

    val cc = grapxh.connectedComponents().vertices
    cc.foreach(println)

    /**
      * 有两组关系
      *(4,4)
       (6,4)
       (7,4)

      *(1,1)
       (3,1)
       (5,1)
       (2,1)
      */
    val users = sc.textFile("F:\\IDEAProjects\\bigdata_course\\32_sparkproject\\src\\main\\resources\\users.txt").map(line => {
      val fields = line.split(",")
      (fields(0).toLong, fields(1))
    })

    // (1,刘德华) join   (1,1)
    // (1 (刘德华,1)) （commonId代表的是共同好友的那个id）
    users.join(cc).map{
      case(id,(username,commonId)) =>(commonId,username)
    }.reduceByKey( (x:String,y:String) => x + ","+ y)
      .foreach(tuple =>{
        println(tuple._2)   // 打印每组好友关系
      })


    sc.stop()
  }

}
