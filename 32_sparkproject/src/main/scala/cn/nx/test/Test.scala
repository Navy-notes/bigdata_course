package cn.nx.test

import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}

/**
  * 一些学习和探索
  */
object Test {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.OFF)
    System.setProperty("spark.ui.showConsoleProgress","false")

    val conf = new SparkConf().setMaster("local").setAppName("studyAndTest")
    val sc = new SparkContext(conf)

    val deviceMap = sc.textFile("F:\\IDEAProjects\\bigdata_course\\32_sparkproject\\src\\main\\resources\\device_mapping.txt")
      .map(line => {
        var map = Map[String, String]()
        val fields = line.split("\t")
        if (fields.length > 1) {
          map += (fields(0) -> fields(1))
        }
        map
      }).collect().flatten.toMap
    deviceMap.foreach(println)
    println(deviceMap)

    val list = List((1,"b"), (2,"b"), (1,"a"))
    sc.parallelize(list, 1).reduceByKey(_.concat(",").concat(_)).foreach(println)


    sc.stop()
  }


}
