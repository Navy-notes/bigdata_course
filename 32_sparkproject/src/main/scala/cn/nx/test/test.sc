val list1 = List(1,2,3)
val list2 = List(4,5,6)

list1 ++ list2
list1.zip(list2)
list1.zip(list2).toMap

var map1 = Map[String,Int]()
var map2 = Map[String,Int]()
map1 += ("hadoop" -> 1)
map2 += ("hello" -> 1)

map1 ++ map2
val list3 = (map1 ++ map2).toList

val list4 = List(("hello",1))


list3 ++ list4

val map3 = (1 -> "b", 2 -> "b", 1->"a")
