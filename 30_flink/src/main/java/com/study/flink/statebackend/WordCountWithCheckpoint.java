package com.study.flink.statebackend;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.api.common.state.ListState;
import org.apache.flink.api.common.state.ListStateDescriptor;
import org.apache.flink.api.common.time.Time;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.runtime.state.FunctionInitializationContext;
import org.apache.flink.runtime.state.FunctionSnapshotContext;
import org.apache.flink.runtime.state.filesystem.FsStateBackend;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.checkpoint.CheckpointedFunction;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * state -> checkpoint:
 *  keyed
 *  operator  
 */
public class WordCountWithCheckpoint {
    public static void main(String[] args) throws Exception {
        ParameterTool parameterTool = ParameterTool.fromArgs(args);
        String hostname = parameterTool.get("hostname");
        int port = parameterTool.getInt("port");
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();


        /**
         * checkpoint的配置
         */
        // 每10s 向JobManager同步一次 state，然后做checkpoint
        // 如果数据量比较大，建议5分钟左右checkpoint的一次。阿里他们使用的时候 也是这样建议的。
        env.enableCheckpointing(10000);      // checkpoint默认是不开启的，如果需要开启需要显式的声明这一句代码

        // asynchronousSnapshots默认为true，即做分布式的异步快照
        FsStateBackend fsStateBackend = new FsStateBackend("file:///F:/IDEAProjects/bigdata_course/30_flink/src/main/resources/checkpoint");

        env.setStateBackend(fsStateBackend);


//        // 默认状态存储后端就是Memory
//        env.setStateBackend(new MemoryStateBackend());

//        env.setStateBackend(new RocksDBStateBackend("hdfs://10.0.0.9:8020/flink/checkpoint"));

        // checkpoint遵照精确一次的语义（默认）
        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.EXACTLY_ONCE);

        // 两次checkpoint之间的最小间隔为0.5s
        env.getCheckpointConfig().setMinPauseBetweenCheckpoints(500);

        // 一次checkpoint的最大完成时间为60s，否则该检查点被丢弃
        env.getCheckpointConfig().setCheckpointTimeout(60000);

        // cancel程序的时候会保存checkpoint（默认不保存）
        env.getCheckpointConfig().enableExternalizedCheckpoints(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);



        /**
         * 重启策略的配置（一般使用的比较多的是固定间隔Fixed delay）
         */
        env.setRestartStrategy(RestartStrategies.fixedDelayRestart(
                3, // 尝试重启的次数
                Time.of(10, TimeUnit.SECONDS) // 两次重启之间的间隔
        ));

//        env.setRestartStrategy(RestartStrategies.failureRateRestart(
//                3,            // 一个时间段内的最大失败次数
//                Time.minutes(5),        // 衡量失败次数的时间段
//                Time.seconds(10)        // 间隔
//        ));


        DataStreamSource<String> dataStream = (DataStreamSource<String>) env.socketTextStream(hostname, port);
//        DataStreamSource<String> dataStream = env.socketTextStream("localhost", 1234);
        SingleOutputStreamOperator<Tuple2<String, Integer>> result = dataStream.flatMap(new FunctionWithOperatorState()).setParallelism(1)

//                .filter(ele -> !"key".equals(ele.f0)).setParallelism(1)
                .keyBy(0)
                .sum(1).uid("word-count").setParallelism(2);



//        result.writeToSocket("localhost", 5678, tuple -> (tuple.f0 + " -> " + tuple.f1 + "\n").getBytes());
        result.print().setParallelism(1);

        env.execute("WordCount check point....");
    }

    /**
     * 使用算子状态
     */
    static class FunctionWithOperatorState implements FlatMapFunction<String, Tuple2<String, Integer>>, CheckpointedFunction {
        private ListState<String> listState;
        private List<String> list;

        @Override
        public void flatMap(String line, Collector<Tuple2<String, Integer>> out) throws Exception {
            list.add(line);
            String[] fields = line.split(" ");
            for (String word : fields) {
                out.collect(Tuple2.of(word, 1));
            }
        }

        /**
         * 进行（轻量级、异步）分布式快照checkpoint
         */
        @Override
        public void snapshotState(FunctionSnapshotContext ctx) throws Exception {
            listState.update(list);
        }

        /**
         * operator-state的初始状态，包括从故障恢复过来
         */
        @Override
        public void initializeState(FunctionInitializationContext ctx) throws Exception {
            listState = ctx.getOperatorStateStore().getListState(
                    new ListStateDescriptor<String>("operatpr-state", String.class));
            list = new ArrayList<>();
            // 如果是故障恢复
            if (ctx.isRestored()) {
                for (String historyEle : listState.get()) {
                    list.add(historyEle);
                }
            }
        }
    }

}
