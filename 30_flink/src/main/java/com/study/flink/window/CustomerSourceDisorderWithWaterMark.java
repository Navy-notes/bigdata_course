package com.study.flink.window;

import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AssignerWithPunctuatedWatermarks;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.apache.flink.streaming.api.windowing.time.Time;

import javax.annotation.Nullable;

/**
 * 需求：每隔5秒计算最近10秒的单词次数（引入watermark解决乱序的问题）
 * 结果：
     (hadoop,2)
     (hadoop,3)
     (hadoop,1)
 */
public class CustomerSourceDisorderWithWaterMark {

    /**
     * 提取事件时间并注册水位线
     *     泛型T： 输入事件的数据类型
     *     实现接口： AssignerWithPunctuatedWatermarks
     *
     * 这里的watermark基于断点式(非周期性)生成，每当接受到一个事件时，才会调用checkAndGetNextWatermark()方法。
     * 用户需要实现该方法，可以根据每个事件的特征，有条件地去控制watermark的生成；也可以直接生成watermark，那就相当于每来一个事件，就往它的后面插入一条水位线。
     */
    public static class EventTimeExtractor implements AssignerWithPunctuatedWatermarks<Tuple2<String, Long>> {

        private Long maxTs = Long.MIN_VALUE;
        private Long delay = 5 * 1000L;     // 设置 5s 延迟

        @Nullable
        @Override
        public Watermark checkAndGetNextWatermark(Tuple2<String, Long> element, long extractedTimestamp) {
            maxTs = Math.max(maxTs, extractedTimestamp);
            return new Watermark(maxTs - delay);
        }

        @Override
        public long extractTimestamp(Tuple2<String, Long> element, long previousElementTimestamp) {
            return element.f1;
        }
    }

    /**
     * 流程序主入口
     * @param args
     */
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        // 步骤一：设置事件时间类型，默认的是 ProcessingTime
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        env.addSource(new CustomerSourceDisorderByEventTime.TestSource())
                .map((String event) -> {
                    String[] split = event.split(",");
                    return Tuple2.of(split[0], Long.parseLong(split[1]));
                })
                .returns(Types.TUPLE(Types.STRING, Types.LONG))
                // 步骤二：在keyBy之前注册时间字段和水位线
                .assignTimestampsAndWatermarks(new EventTimeExtractor())      // 步骤三：自定义实现AssignerWithPeriodicWatermarks或AssignerWithPunctuatedWatermarks
                .keyBy(0)
                .timeWindow(Time.seconds(10), Time.seconds(5))
                .process(new CustomerSourceDisorderByEventTime.SumProcessFunction())
                .print();

        env.execute("Disorder With WaterMark");
    }

}
