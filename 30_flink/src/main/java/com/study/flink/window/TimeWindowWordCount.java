package com.study.flink.window;

import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.util.Collector;

/**
 * 每隔5秒统计最近10秒的单词出现的次数（不捕获窗口信息）
 */
public class TimeWindowWordCount {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setParallelism(1);

        DataStreamSource<String> dataStream = env.socketTextStream("localhost", 1234);
        dataStream.flatMap((String line, Collector<Tuple2<String, Long>> out ) -> {
            String[] words = line.split(" ");
            for (String word : words) {
                out.collect(Tuple2.of(word, 1L));
            }
        })
                .returns(Types.TUPLE(Types.STRING, Types.LONG))
                .keyBy(0)
                .timeWindow(Time.seconds(10), Time.seconds(5))
                .sum(1)
                .print();

        /**
         * 没有窗口的时候，是来一条数据（一个event）触发一次处理（本案例就看是否有输出即可）；
         * 有窗口的时候，每次处理（输出）是根据窗口的滑动时间决定的（滚动窗口的滑动时间的等于windowSize），
             当到了触发窗口的时间点时，只要窗口范围内有数据，就会处理（输出），否则跳过本次窗口（无输出）。
             所以其实窗口操作一定程度上可以理解成攒批，批流其实不分家。
         */
        env.execute("Slide TimeWindow WordCount");
    }
}
