package com.study.flink.window;

import org.apache.commons.lang3.time.FastDateFormat;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.shaded.guava18.com.google.common.collect.Lists;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;



/**
 * 每隔5秒统计最近10秒的单词出现的次数（捕获窗口信息）
 * 输入：
     hello
     hello
 * 输出：
     当前系统时间：01:26:20
     窗口处理时间：01:26:20
     (hello,1)
     窗口开始时间：01:26:10
     窗口结束时间：01:26:20
     =====================================================
     当前系统时间：01:26:25
     窗口处理时间：01:26:25
     (hello,2)
     窗口开始时间：01:26:15
     窗口结束时间：01:26:25
     =====================================================
     当前系统时间：01:26:30
     窗口处理时间：01:26:30
     (hello,1)
     窗口开始时间：01:26:20
     窗口结束时间：01:26:30
     =====================================================
 * 分析结果可知：
     窗口每5秒进行一次处理并且只有当该窗口结束时才触发计算，窗口处理时间即当前的系统时间。
     窗口的大小/长度为10秒。（endTime - startTime）
   验证 TimeWindowWordCount 中的猜想和思考。
 */
public class WordCountWithProcessWindowFunction {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        DataStreamSource<String> dataStream = env.socketTextStream("127.0.0.1", 1234);

        dataStream.flatMap((String line, Collector<Tuple2<String, Integer>> out) -> {
            String[] words = line.split(" ");
            for (String word : words) {
                out.collect(Tuple2.of(word, 1));
            }
        })
                .returns(Types.TUPLE(Types.STRING, Types.INT))
                .keyBy(0)
                .timeWindow(Time.seconds(10), Time.seconds(5))
                .process(new MySumProcessWindowFunction())      //类似sum的实现，但是可以捕获窗口信息
                .print();

        env.execute("WordCount With ProcessWindowFunction");
    }

    /**
     * IN, OUT, KEY, W extends Window
     */
    public static class MySumProcessWindowFunction
            extends ProcessWindowFunction<Tuple2<String, Integer>, Tuple2<String, Integer>, Tuple, TimeWindow> {
        FastDateFormat dateformat = FastDateFormat.getInstance("HH:mm:ss");

        @Override
        public void process(Tuple key, Context context, Iterable<Tuple2<String, Integer>> elements, Collector<Tuple2<String, Integer>> out) throws Exception {
            System.out.println("当前系统时间：" + dateformat.format(System.currentTimeMillis()));
            System.out.println("窗口处理时间：" + dateformat.format(context.currentProcessingTime()));

            int sum = Lists.newArrayList(elements.iterator()).size();
            out.collect(Tuple2.of(key.getField(0), sum));

            System.out.println("窗口开始时间：" + dateformat.format(context.window().getStart()));
            System.out.println("窗口结束时间：" + dateformat.format(context.window().getEnd()));
            System.out.println("=====================================================");
        }
    }
}
