package com.study.flink.window;

import org.apache.commons.lang3.time.FastDateFormat;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import javax.annotation.Nullable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;

/**
 * 需求：每隔5秒计算最近10秒的单词次数（引入事件时间的概念）
 * 结果：
     (hadoop,1)     // 第一个窗口有误
     (hadoop,3)
     (hadoop,1)
 */
public class CustomerSourceDisorderByEventTime {

    /**
     * 自定义数据源：在第13秒发送一条数据，第16秒发送一条数据，在第19秒发送一条数据
     */
    public static class TestSource implements SourceFunction<String> {
        FastDateFormat dateFormat = FastDateFormat.getInstance("HH:mm:ss");

        @Override
        public void run(SourceContext<String> sourceContext) throws Exception {
            String currentTime = String.valueOf(System.currentTimeMillis());

            while (Integer.parseInt(currentTime.substring(currentTime.length() - 4)) > 100) {
                currentTime = String.valueOf(System.currentTimeMillis());
                continue;
            }

            System.out.println("开始发送事件的时间：" + dateFormat.format(System.currentTimeMillis()));

            // 13
            String event="hadoop," + (System.currentTimeMillis() - 3600 * 24 * 1000L);

            TimeUnit.SECONDS.sleep(3);
            sourceContext.collect(event);
//            sourceContext.collect(event);

            // 16
            TimeUnit.SECONDS.sleep(3);
            sourceContext.collect("hadoop," + (System.currentTimeMillis() - 3600 * 24 * 1000L));

            // 19
            TimeUnit.SECONDS.sleep(3);
            sourceContext.collect(event);


            // 这个事件是用于触发前面的窗口
            sourceContext.collect("hadoop," + System.currentTimeMillis());


            TimeUnit.SECONDS.sleep(3000);
        }

        @Override
        public void cancel() {

        }
    }


    /**
     * 自定义sum计数器
     */
    public static class SumProcessFunction
            extends ProcessWindowFunction<Tuple2<String, Long>, Tuple2<String, Integer>, Tuple, TimeWindow> {
        @Override
        public void process(Tuple key, Context context, Iterable<Tuple2<String, Long>> elements, Collector<Tuple2<String, Integer>> out) throws Exception {
            DateFormat dateFormat =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            System.out.println("====================================================");
            System.out.println("当前窗口开始时间：" + dateFormat.format(context.window().getStart()));
            System.out.println("当前窗口结束时间：" + dateFormat.format(context.window().getEnd()));
//            System.out.println("当前水位线：" + dateFormat.format(context.currentWatermark()));

            int sum = 0;
            for (Tuple2<String, Long> element : elements) {
                sum++;
            }

            out.collect(Tuple2.of(key.getField(0), sum));
        }
    }


    /**
     * 提取事件时间并注册水位线
     *     泛型T： 输入事件的数据类型
     *     实现接口： AssignerWithPeriodicWatermarks
     *
     * 由于是周期性生成watermark，Flink默认保持每200ms的频率调用一次getCurrentWatermark()，在事件流中插入当前的水位线。
     * （并且PeriodicWatermarks会先于source的事件流输入而生成，可以理解成搭建一个自来水管道系统，肯定是整个管道系统都竣工了以后才去通水流，这是实时框架不同于批处理模型的地方。）
     * 而每来一个事件，就调用一次extractTimestamp()方法，获取当前的事件时间，并更新影响watermark的 maxTs。
     * 所以，当没有事件捕获的时候，maxTs就不会变化，即使是周期性地调用getCurrentWatermark()方法，新产生的watermark也不会有更新。（当然，这取决于用户如何实现该方法的代码逻辑）
     *
     * 可通过代码env.getConfig().setAutoWatermarkInterval(ms)设置周期频率
     */
    public static class EventTimeExtractor implements AssignerWithPeriodicWatermarks<Tuple2<String, Long>> {
        // 当前最大时间戳
        private Long maxTs = Long.MIN_VALUE;
        // 延迟毫秒数
        private Long delay = 0 * 1000L;     // 无延迟

//        private FastDateFormat dateFormat = FastDateFormat.getInstance("HH:mm:ss");

        @Nullable
        @Override
        public Watermark getCurrentWatermark() {
//            System.out.println(dateFormat.format(maxTs - delay));
            return new Watermark(maxTs - delay);
        }

        @Override
        public long extractTimestamp(Tuple2<String, Long> element, long previousElementTimestamp) {
//            System.out.println(dateFormat.format((System.currentTimeMillis())));
            maxTs = Math.max(maxTs, element.f1);      // 这里是为了保持watermark的递增性
            return element.f1;
        }
    }

    /**
     * 流程序主入口
     * @param args
     */
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        // 步骤一：设置事件时间类型，默认的是 ProcessingTime
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        env.addSource(new TestSource())
                .map((String event) -> {
                    String[] split = event.split(",");
                    return Tuple2.of(split[0], Long.parseLong(split[1]));
                })
                .returns(Types.TUPLE(Types.STRING, Types.LONG))
                // 步骤二：在keyBy之前注册时间字段和水位线
                .assignTimestampsAndWatermarks(new EventTimeExtractor())      // 步骤三：自定义实现AssignerWithPeriodicWatermarks或AssignerWithPunctuatedWatermarks
                .keyBy(0)
                .timeWindow(Time.seconds(10), Time.seconds(5))
                .process(new SumProcessFunction())
                .print();

        env.execute("Disorder By EventTime");
    }

}
