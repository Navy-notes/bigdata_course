﻿深入浅出Kafka第一天

A.课程内容  

1.Kafka的体系架构,高性能原理,安装部署.
 
2.Kafka生产者 , 消费者原理以及核心参数.

3.Kafka重要的概念巩固以及扩展知识.
 

B.授课方式

1.简单知识快节奏 

2.重点知识记笔记 


C.课程目标

计算  存储

离线方向:hdfs(分布式文件存储系统)  mapreduce,Hive,Spark,Flink

实时方向:Kafka(分布式消息系统)  Storm,SparkStreaming,Flink


1.能够搞定公司里kafka遇到的问题
2.面试杀手锏
3.给大家提供学习kafka的思路 




Day01:
Kafka体系架构,高性能原理,安装部署


一.消息系统

理解为一个大大的缓存  


好处

问题:
1.会让我们的流程变慢
2.出现数据丢失
3.如果消费者的速度跟不上生产者怎么办
4.如果消费者短暂挂掉怎么办
5.生产者的速度落后于消费者怎们办等等


二.Kafka

概念.

1.体系架构
topic  主题   -->广播,频道  逻辑上概念     可以基于分布式环境存储的消息

存储:类似于SparkRDD   

offset 消息的顺序/位置 


kafka元信息   


2.kafka高性能原理
写 读
磁盘几十个T    128G内存

a. 采用磁盘的顺序写保证写数据的高性能

    追加 而且是写到os cache上然后就算写完了，等待一定程度再刷写到磁盘

b. kafka采用服务端日志分段存储

    Topic-a  3个分区
        kafka1:
        分区目录:topic-a-0
        kafka2:
        分区目录:topic-a-1
        kafka3:
        分区目录:topic-a-2
    每个分区目录下有多个.log文件

    分段策略属性：
        log.roll.{hours,ms} ---日志滚动的周期时间，到达指定周期时间时，强制生成一个新的segment，默认值是168（7day）
        log.segment.bytes   ---每个segment的最大容量。到达指定容量时，将强制生成一个新的segment，默认值是1G（-1为不限制）
    日志保存清理策略：
        log.retention.hours         ---日志在磁盘的保存时间，可以选择hours,minutes和ms，默认值是168（7day）
        log.segment.delete.delay.ms ---日志文件被真正删除前的保留时间，默认值是60000
        注：日志的真正清除时间。当删除的条件满足以后，日志将被“删除”，但是这里的删除其实只是将该日志进行了“delete”标注，文件只是无法被索引到了而已。
            但是文件本身，仍然是存在的，只有当过了log.segment.delete.delay.ms 这个时间以后，文件才会被真正的从文件系统中删除。


c. kafka采用服务端冗余副本方式
 
    一个副本   一份数据     0.8版本没有副本机制

    副本关系：
    partition是有角色之分的
    1)leader partition
        a.客户端写数据  消费者读数据操作的都是leader partition   （主写主读）
        b.会维护一个ISR  in-sync-Replica  副本列表
        (pA,pB,pC) ISR副本列表，AR = ISR + OSR
    2)follower partition
        定期的去leader partition同步数据

d.kafka采用服务端稀疏索引方式 + 二分查找方式

    producer-->kafka <--Consumer
    稀疏索引： .log文件每隔4kb大小，.index文件记录一次offset
    消费的位置问题
        offset 消息之间的相对的偏移位置 偏移量
        position  物理位置

e.服务端0拷贝：看两张png

【Kafka 高性能原理总结】
    MMF机制内存映射文件
    磁盘顺序写
    0拷贝
    批处理、批量压缩
    分段存储
    稀疏索引,二分查找方式
    服务端冗余副本
    消费端支持流式并行，主题的分区数决定下游消费的并行度




3.安装部署
1.需求场景分析.

假设 10亿请求 -->kafka 

【二八法则】
    10亿-->一天24小时过来的
    12:00--8:00 -->20%数据，2亿请求

    16小时-->8亿请求
    16*0.2=3小时  高峰期
    6亿数据-->3小时处理完的

qps
6亿/3小时=5.5万/s qps=5.5万  

一个请求 数据量 50kb

10亿 * 50kb = 46T 

两个副本  46T*2=92T

保留3天 

92T * 3天 = 276T

需求: 搞定10亿请求,高峰期5.5万qps,存储276T数据


2.规划资源

a.服务器
1)虚拟机还是物理机？     物理机
2)几台？
    四倍高峰期，20万 qps  -->5台
    搞定10亿请求,高峰期5.5万qps,存储276T数据  ,需要5台物理机

b.磁盘评估.
1)SSD,普通机械硬盘?   SAS
    SSD硬盘: 随机读写性能比较好（顺序写的性能和普通的硬盘差不多），价格贵
    SAS盘: 某方面性能不是很好,但是便宜

2)每台服务器需要多少磁盘？

    5台物理机，276T的数据，每台至少需要60T的内存

    28法则：准备的容量*0.8=需要的容量
    75T

    每台： 11块硬盘 * 7T = 77T
    集群： 77T * 5 = 385T


场景总结:
搞定10亿请求,高峰期5.5万qps,存储276T数据  ,需要5台物理机,每台:11块(SAS)*7T磁盘


c.内存评估
1)尽可能多的给oscache
2)JVM  10G

hdfs的namenode jvm 元数据 几十个G  100G

100个topic * 5 partition *2 =1000partition
1000 * 1G(1个log文件的大小) = 1000G内存
0.25 * 1000=250G内存
250/5=50G内存
50+10=60G   64G内存条


场景总结:
搞定10亿请求,高峰期5.5万qps,存储276T数据  ,需要5台物理机,每台:11(SAS)*7T磁盘,需要64G内存(128G内存更好)


d.cpu压力评估
线程 cpu core
1)kafka哪些线程
    接收请求的线程
    处理请求线程
    定时清理的线程
    拉取数据的线程
    定时检查ISR列表线程  等等

100多个线程

cpu core 4个 几十个线程
cpu core 8个 轻松几十个线程
cpu core 16个  32个更好

2cpu * 8=16个

场景总结:
搞定10亿请求,高峰期5.5万qps,存储276T数据  ,需要5台物理机，每台:11(SAS)*7T磁盘,需要64G内存(128G内存更好)
需要16个cpu core(32个更好)


e.网络需求评估
千兆  万兆


场景总结:
搞定10亿请求,高峰期5.5万qps,存储276T数据  ,需要5台物理机,
每台：11(SAS)*7T磁盘，需要64G内存(128G内存更好)，需要16个cpu core(32个更好)，千兆网卡,万兆更好

-->采购-->运维搭建-->部署服务


f.集群规划和搭建

4.测试使用

5.压力测试
发送50万条数据 消费50万条数据


停掉kafka集群不要直接用kill命令，而应该执行./bin/kafka-server-stop.sh，不然停留在oscache中的数据会丢失


运维工具:
    kafkamanager、
    kafka-eagle （40_summary\src\main\大数据知识体系总结文档\Kafka知识体系吐血总结.pdf  ---Page46）

读取.log文件的内容：
    bin/kafka-run-class.sh kafka.tools.DumpLogSegments --files /tmp/kafka-logs/test3-0/00000000000000000000.log  --print-data-log



作业:
kafka体系架构图--画一遍 

kafkamanager搭建好 截图










































































































































