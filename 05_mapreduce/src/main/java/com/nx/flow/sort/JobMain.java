package com.nx.flow.sort;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import java.io.IOException;

/**
 * @author Yuliang.Lee
 * create  2021/8/21 22:50
 */
public class JobMain {

    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        //一、初始化一个Job对象
        Configuration configuration = new Configuration();
        Job job = Job.getInstance(configuration, "flow_sort");

        //二、设置Job对象的相关的信息 ，里面包含了8个小步骤
        //1、设置输入的路径，让程序能找到输入文件的位置0
        job.setInputFormatClass(TextInputFormat.class);
        TextInputFormat.addInputPath(job,new Path("05_mapreduce/src/main/document/data/output/flow_sum/part-r-00000"));

        //2、设置Mapper的类型，并设置 k2 v2
        job.setMapperClass(FlowSortMapper.class);
        job.setMapOutputKeyClass(FlowSortBean.class);
        job.setMapOutputValueClass(NullWritable.class);

        // 3 分区
        // 4 排序
        // 5 局部合并combine
        // 6 分组
        // 四个步骤，都是Shuffle阶段，现在使用默认的即可

        //7、设置Reducer的类型，并设置k3 v3
        job.setReducerClass(FlowSortReducer.class);
        job.setOutputKeyClass(FlowSortBean.class);
        job.setOutputValueClass(NullWritable.class);

        //8、设置输出的路径，让程序给结果放到一个地方去
        job.setOutputFormatClass(TextOutputFormat.class);
        TextOutputFormat.setOutputPath(job,new Path("05_mapreduce/src/main/document/data/output/flow_sort"));

        //三、等待程序完成
        boolean b = job.waitForCompletion(true);
        System.out.println(b);
        System.exit(b ? 0 : 1);
    }
}
