package com.nx.flow.sort;

import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/8/21 22:38
 */
public class FlowSortBean implements WritableComparable<FlowSortBean> {
    private String phone;           // 手机号
    private Integer upFlow;         // 上行数据包
    private Integer downFlow;       // 下行数据包
    private Integer upCountFlow;    // 上行流量
    private Integer downCountFlow;  // 下行流量

    public FlowSortBean() {
    }

    public FlowSortBean(String phone, Integer upFlow, Integer downFlow, Integer upCountFlow, Integer downCountFlow) {
        this.phone = phone;
        this.upFlow = upFlow;
        this.downFlow = downFlow;
        this.upCountFlow = upCountFlow;
        this.downCountFlow = downCountFlow;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getUpFlow() {
        return upFlow;
    }

    public void setUpFlow(Integer upFlow) {
        this.upFlow = upFlow;
    }

    public Integer getDownFlow() {
        return downFlow;
    }

    public void setDownFlow(Integer downFlow) {
        this.downFlow = downFlow;
    }

    public Integer getUpCountFlow() {
        return upCountFlow;
    }

    public void setUpCountFlow(Integer upCountFlow) {
        this.upCountFlow = upCountFlow;
    }

    public Integer getDownCountFlow() {
        return downCountFlow;
    }

    public void setDownCountFlow(Integer downCountFlow) {
        this.downCountFlow = downCountFlow;
    }

    @Override
    public String toString() {
        return  phone + '\t' +
                upFlow + '\t' +
                downFlow + '\t' +
                upCountFlow + '\t' +
                downCountFlow;
    }


    @Override
    public int compareTo(FlowSortBean o) {
        // 根据upFlow倒序排序
        int result = o.upFlow.compareTo(this.upFlow);

//        // 但是在调试的过程中发现，这种通过compare的排序应该还是在reduce的时候中做的
//        if (result == 0)
//            return -1;

        return result;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeUTF(phone);
        dataOutput.writeInt(upFlow);
        dataOutput.writeInt(downFlow);
        dataOutput.writeInt(upCountFlow);
        dataOutput.writeInt(downCountFlow);
    }


    /**
     *  这是反序列化方法
        反序列时候  注意序列化的顺序
        先序列化的先出来
     * @param dataInput
     * @throws IOException
     */
    @Override
    public void readFields(DataInput dataInput) throws IOException {
        this.phone = dataInput.readUTF();
        this.upFlow = dataInput.readInt();
        this.downFlow = dataInput.readInt();
        this.upCountFlow = dataInput.readInt();
        this.downCountFlow = dataInput.readInt();
    }
}
