package com.nx.flow.sum;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * @author LIAO
 * create  2021-06-22 22:35
 * Reducer<KEYIN,VALUEIN,KEYOUT,VALUEOUT>
 *     KEYIN    phoneNum 的类型
 *     VALUEIN  FlowCountBean
 *     KEYOUT  Text
 *     VALUEOUT  FlowCountBean
 *
 *
 *
| 上行数据包 | upFlow        | int    |
| 下行数据包 | downFlow      | int    |
| 上行流量   | upCountFlow   | int    |
| 下行流量   | downCountFlow | int    |
 */
public class FlowCountReducer extends Reducer<Text,FlowCountBean,Text,FlowCountBean>  {
    @Override
    protected void reduce(Text key, Iterable<FlowCountBean> values, Context context) throws IOException, InterruptedException {
        //1、遍历values，将四个字段进行累加
        Integer upFlow = 0;
        Integer downFlow = 0;
        Integer upCountFlow = 0;
        Integer downCountFlow = 0;
        for (FlowCountBean value : values) {
            upFlow += value.getUpFlow();
            downFlow += value.getDownFlow();
            upCountFlow += value.getUpCountFlow();
            downCountFlow += value.getDownCountFlow();
        }

        //2、创建一个FlowBean对象，存放累加之后的数据
        FlowCountBean flowBean = new FlowCountBean();
        flowBean.setUpFlow(upFlow);
        flowBean.setDownFlow(downFlow);
        flowBean.setUpCountFlow(upCountFlow);
        flowBean.setDownCountFlow(downCountFlow);

        //3、写入到上下文
        context.write(key,flowBean);
    }
}
