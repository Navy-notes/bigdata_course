package com.nx.flow.partition;

import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.io.Text;

import java.io.IOException;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/8/22 13:51
 */
public class FlowPartitionReducer extends Reducer<Text, FlowPartitionBean, Text, FlowPartitionBean> {
    @Override
    protected void reduce(Text key, Iterable<FlowPartitionBean> values, Context context) throws IOException, InterruptedException {
        int upFlowSum = 0;
        int downFlowSum = 0;
        int upCountFlowSum = 0;
        int downCountFlowSum = 0;

        for (FlowPartitionBean bean : values) {
            upFlowSum += bean.getUpFlow();
            downFlowSum += bean.getDownFlow();
            upCountFlowSum += bean.getUpCountFlow();
            downCountFlowSum += bean.getDownCountFlow();
        }
        FlowPartitionBean result = new FlowPartitionBean(upFlowSum, downFlowSum, upCountFlowSum, downCountFlowSum);

        context.write(key, result);
    }
}
