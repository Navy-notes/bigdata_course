package com.nx.flow.partition;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/8/22 13:39
 */
public class FlowPartitionMapper extends Mapper<LongWritable, Text, Text, FlowPartitionBean> {
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] split = value.toString().split("\t");
        String phone = split[1];
        Integer upFlow = Integer.parseInt(split[6]);
        Integer downFlow = Integer.parseInt(split[7]);
        Integer upCountFlow = Integer.parseInt(split[8]);
        Integer downCountFlow = Integer.parseInt(split[9]);

        FlowPartitionBean bean = new FlowPartitionBean(upFlow, downFlow, upCountFlow, downCountFlow);

        context.write(new Text(phone), bean);
    }
}
