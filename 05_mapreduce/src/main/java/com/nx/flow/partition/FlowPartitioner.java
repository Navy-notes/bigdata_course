package com.nx.flow.partition;

import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.io.Text;
/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/8/22 13:45
 */
public class FlowPartitioner extends Partitioner<Text, FlowPartitionBean> {
    @Override
    public int getPartition(Text text, FlowPartitionBean flowPartitionBean, int i) {
        String prefix = text.toString().substring(0, 3);
        if ("135".equalsIgnoreCase(prefix)) {
            return 0;
        }
        else if ("136".equalsIgnoreCase(prefix)) {
            return 1;
        }
        else if ("137".equalsIgnoreCase(prefix)) {
            return 2;
        }
        return 3;
    }
}
