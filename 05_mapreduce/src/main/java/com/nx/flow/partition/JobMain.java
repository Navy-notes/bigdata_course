package com.nx.flow.partition;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import java.io.IOException;

/**
 * @author Yuliang.Lee
 * create  2021/8/2 13:56
 */
public class JobMain {

    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        //一、初始化一个Job对象
        Configuration configuration = new Configuration();
        Job job = Job.getInstance(configuration, "flow_partition");

        //二、设置Job对象的相关的信息 ，里面包含了8个小步骤
        //1、设置输入的路径，让程序能找到输入文件的位置0
        job.setInputFormatClass(TextInputFormat.class);
        TextInputFormat.addInputPath(job,new Path("05_mapreduce/src/main/document/data/input/flow.log"));

        //2、设置Mapper的类型，并设置 k2 v2
        job.setMapperClass(FlowPartitionMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(FlowPartitionBean.class);

        // 3 分区
        job.setPartitionerClass(FlowPartitioner.class);
        // 4 排序
        // 5 局部合并combine
        // 6 分组
        // 四个步骤，都是Shuffle阶段，现在使用默认的即可

        job.setNumReduceTasks(4);

        //7、设置Reducer的类型，并设置k3 v3
        job.setReducerClass(FlowPartitionReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(FlowPartitionBean.class);

        //8、设置输出的路径，让程序给结果放到一个地方去
        job.setOutputFormatClass(TextOutputFormat.class);
        TextOutputFormat.setOutputPath(job,new Path("05_mapreduce/src/main/document/data/output/flow_partition"));

        //三、等待程序完成
        boolean b = job.waitForCompletion(true);  //设置为false也有日志打印输出
        System.out.println(b);
        System.exit(b ? 0 : 1);
    }
}
