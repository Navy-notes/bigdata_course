package com.nx.combiner;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import sun.text.resources.cldr.kw.FormatData_kw;

import java.io.IOException;

/**
 * @author LIAO
 * create  2021-06-22 21:59
 *  Reducer<KEYIN,VALUEIN,KEYOUT,VALUEOUT>
 *      KEYIN  map阶段传递过来的key的类型
 *      VALUEIN map阶段传递过来的value的类型
 *      KEYOUT 局部合并的key的类型
 *      VALUEOUT  局部合并的value的类型
 */
public class MyCombiner extends Reducer<Text,LongWritable,Text,LongWritable> {
    @Override
    protected void reduce(Text key, Iterable<LongWritable> values, Context context) throws IOException, InterruptedException {
        //1、定义变量
        long count = 0;

        //2、进行累加
        for (LongWritable value : values) {
            count += value.get();
        }

        //3、写入到上下文
        context.write(key,new LongWritable(count));
    }
}
