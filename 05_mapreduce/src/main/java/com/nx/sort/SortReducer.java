package com.nx.sort;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * @author LIAO
 * create  2021-06-22 21:48
 * Reducer<KEYIN,VALUEIN,KEYOUT,VALUEOUT>
 *     KEYIN,   MySortBean
 *     VALUEIN  NullWritable
 *     KEYOUT   MySortBean
 *     VALUEOUT   NullWritable
 */
public class SortReducer extends Reducer<MySortBean,NullWritable,MySortBean,NullWritable> {
    @Override
    protected void reduce(MySortBean key, Iterable<NullWritable> values, Context context) throws IOException, InterruptedException {
        //将map阶段的结果拿过来汇总
        context.write(key,NullWritable.get());
    }
}
