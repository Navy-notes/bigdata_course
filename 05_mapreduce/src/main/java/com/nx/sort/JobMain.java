package com.nx.sort;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import java.io.IOException;

/**
 * @author LIAO
 * create  2021-06-22 21:50
 */
public class JobMain {
    /**
     * 这个main是程序的入口
     * 其中用一个Job对象来管理这个程序运行的很多的参数
     * 指定用哪个类作为Mapper的业务逻辑类，指定哪个类用Reducer的业务逻辑类
     * ...... 等等 其他的各种需要的参数
     * @param args
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        //一、初始化一个Job对象
        Configuration configuration = new Configuration();
        Job job = Job.getInstance(configuration, "sort");

        //二、设置Job对象的相关的信息 ，里面包含了8个小步骤
        //1、设置输入的路径，让程序能找到输入文件的位置
        job.setInputFormatClass(TextInputFormat.class);
        TextInputFormat.addInputPath(job,new Path("05_mapreduce/src/main/document/data/input/test3.txt"));

        //2、设置Mapper的类型，并设置 k2 v2
        job.setMapperClass(SortMapper.class);
        job.setMapOutputKeyClass(MySortBean.class);
        job.setMapOutputValueClass(NullWritable.class);

        //3 4 5 6 四个步骤，都是Shuffle阶段，现在使用默认的即可

        //7、设置Reducer的类型，并设置k3 v3
//        job.setNumReduceTasks(2);   // 如果要求全局排序不能使用多个分区
        job.setReducerClass(SortReducer.class);
        job.setOutputKeyClass(MySortBean.class);
        job.setOutputValueClass(NullWritable.class);

        //8、设置输出的路径，让程序给结果放到一个地方去
        job.setOutputFormatClass(TextOutputFormat.class);
        TextOutputFormat.setOutputPath(job,new Path("05_mapreduce/src/main/document/data/output/sort"));

        //三、等待程序完成
        boolean b = job.waitForCompletion(true);
        System.out.println(b);
        System.exit(b ? 0 : 1);
    }
}
