package com.nx.sort;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @author LIAO
 * create  2021-06-22 21:27
 *  Mapper<KEYIN, VALUEIN, KEYOUT, VALUEOUT>
 *      KEYIN  一行文本的偏移量的  类型
 *      VALUEIN     一行的文本
 *      KEYOUT    MySortBean
 *      VALUEOUT    NullWritable
 */
public class SortMapper extends Mapper<LongWritable, Text, MySortBean, NullWritable>{
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        //1、拆分数据  一行文本作为一个拆分
        String[] split = value.toString().split(" ");

        //2、将对应的的值传入到MySortBean的实例对象中
        MySortBean mySortBean = new MySortBean();
        mySortBean.setWord(split[0]);
        mySortBean.setNum(Integer.parseInt(split[1]));

        //3、写入到上下文
        context.write(mySortBean,NullWritable.get());
    }
}
