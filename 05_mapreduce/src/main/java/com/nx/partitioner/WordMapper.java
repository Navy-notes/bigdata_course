package com.nx.partitioner;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @author LIAO
 * create  2021-06-22 20:19
 * Mapper<KEYIN, VALUEIN, KEYOUT, VALUEOUT>
 *     KEYIN  偏移量的 类型
 *     VALUEIN  一行具体的文本
 *     KEYOUT   用户自定义输出的key 的 类型
 *     VALUEOUT 用户自定义输出的value的 类型
 */
public class WordMapper extends Mapper<LongWritable, Text, Text, LongWritable>{
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        //1、切分单词
        String[] words = value.toString().split(" ");

        //2、单词转换
        for (String word : words) {
            //3、写入上下文
            context.write(new Text(word),new LongWritable(1));
        }
    }
}
