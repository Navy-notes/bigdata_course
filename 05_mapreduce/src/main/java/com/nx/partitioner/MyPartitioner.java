package com.nx.partitioner;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

/**
 * @author LIAO
 * create  2021-06-22 20:30
 * Partitioner<KEY, VALUE>
 *     KEY  单词的类型
 *     VALUE  单词的次数 的类型
 */
public class MyPartitioner extends Partitioner<Text, LongWritable> {

    //return (key.hashCode() & Integer.MAX_VALUE) % numReduceTasks;
    //numReduceTasks 设置2 ，那么上面的结果的值 只有 0 和 1
    @Override
    public int getPartition(Text text, LongWritable longWritable, int numPartitions) {
        //分区  需求：根据单词的长度给单词出现的次数的结果存储到不同文件中
        //根据单词的长度进行判断，单词长度>=5 的在一个结果文件中， < 5的在一个文件中
        if (text.toString().length() >=5){
            // 返回的是分区编号。比如总分区数有3个，则分区编号有[0,1,2]
            return 0;
        }else {
            // 若存在返回的分区编号溢出数组上界，则MR执行失败。
            // 但是，当分区数为1的时候，尽管返回的分区编号溢出边界，但是任务依然执行成功。原因是分区数为1时，直接所有的数据都丢一个文件了，源码中估计也认为没必要走分发的代码逻辑，自然就不会产生indexOutOfArray的异常。
            return 1;
        }
    }
}
