package com.nx.partitioner;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import java.io.IOException;

/**
 * @author LIAO
 * create  2021-06-22 20:37
 * 需求：根据单词的长度给单词出现的次数的结果存储到不同文件中
 * //根据单词的长度进行判断，单词长度>=5 的在一个结果文件中， < 5的在一个文件中
 */
public class JobMain {

    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        //一、初始化一个Job
        Configuration configuration = new Configuration();
        Job job = Job.getInstance(configuration, "partitioner");

        //二、配置Job的相关的信息
        //1、设置输入路径
        //job.setInputFormatClass(TextInputFormat.class);
        TextInputFormat.addInputPath(job,new Path("05_mapreduce/src/main/document/data/input/test2.txt"));

        //2、设置Mapper类型
        job.setMapperClass(WordMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(LongWritable.class);

        //3 4 5 6 Shuffle
        //3、设置分区
        job.setPartitionerClass(MyPartitioner.class);

        //7、设置Reducer类型
        job.setReducerClass(WordReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(LongWritable.class);

        //设置ReduceTask得个数，默认不设置的话是1个
        job.setNumReduceTasks(2);

        //8、设置输出路径
        //job.setOutputFormatClass(TextOutputFormat.class);
        TextOutputFormat.setOutputPath(job,new Path("05_mapreduce/src/main/document/data/output/partitioner"));

        //三、等待完成 提交
        boolean b = job.waitForCompletion(true);
        System.out.println(b);
        System.exit(b ? 0 : 1);
    }
}
