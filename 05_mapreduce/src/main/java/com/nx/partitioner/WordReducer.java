package com.nx.partitioner;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * @author LIAO
 * create  2021-06-22 20:25
 * Reducer<KEYIN,VALUEIN,KEYOUT,VALUEOUT>
 *     KEYIN    map阶段输出的key的类型
 *     VALUEIN  数字   的类型
 *     KEYOUT   最终的单词 的类型
 *     VALUEOUT     最终的次数的 类型
 */
public class WordReducer extends Reducer<Text,LongWritable,Text,LongWritable>  {
    @Override
    protected void reduce(Text key, Iterable<LongWritable> values, Context context) throws IOException, InterruptedException {
        //1、定义一个变量
        long count = 0;

        //2、迭代
        for (LongWritable value : values) {
            count += value.get();
        }

        //3、写入到上下文
        context.write(key,new LongWritable(count));
    }
}
