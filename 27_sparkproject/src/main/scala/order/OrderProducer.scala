package order

import java.util.{Properties, UUID}

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import kafka.producer.{KeyedMessage, Producer, ProducerConfig}

import scala.collection.mutable.ArrayBuffer


//订单实体类
case class Order(orderId: String, provinceId: Int, price: Float)

//kafka消息的生产者,向orderTopic放数据
object OrderProducer {
  def main(args: Array[String]): Unit = {
    // Jackson ObjectMapper
    val mapper = new ObjectMapper()
    mapper.registerModule(DefaultScalaModule)

    // 存储kafka相关的集群信息
    val props = new Properties()
    // kafka集群
    props.put("metadata.broker.list", ConstantUtils.METADATA_BROKER_LIST)
    // 设置向topic中存储数据的方式
    props.put("producer.type", ConstantUtils.PRODUCER_TYPE)
    //设置key和message的序列化类
    props.put("serializer.class", ConstantUtils.SERIALIZER_CLASS)
    props.put("key.serializer.class", ConstantUtils.SERIALIZER_CLASS)

    // 创建生产者
    var producer: Producer[String, String] = null

    try {
      // 生产者的配置信息
      val config = new ProducerConfig(props)
      // 创建一个producer实例对象
      producer = new Producer[String, String](config)

      // 存储message的可变数组
      val messageBuffer = new ArrayBuffer[KeyedMessage[String, String]]()
      // 迷你一直产生N条订单数据
      while (true) {
        // 清空数组里面的message
        messageBuffer.clear()

        //随机数用于确定每次随机产生的订单的数目
        val random: Int = RandomUtils.getRandomNum(3000) + 1000
        // TODO :startTime
        val startTime = System.currentTimeMillis()

        // 生成订单数据
        for (index <- 0 until random) {
          // 订单id
          val orderId = UUID.randomUUID().toString
          //省份Id
          val provinceId = RandomUtils.getRandomNum(34) + 1
          // 订单金额
          val orderPrice = RandomUtils.getRandomNum(80) + 100.5F

          // 创建订单实例
          val order =Order(orderId,provinceId,orderPrice)
          // TODO 将实体类转换成Json格式数据
          // 创建message
          val message = new KeyedMessage[String, String](ConstantUtils.ORDER_TOPIC, orderId, mapper.writeValueAsString(order))

          // 打印一下
          //println(s"Order Json :${mapper.writeValueAsString(order)}")

          // 向message数组里面增加message
          messageBuffer += message
          // 一条一条地发送数据
          //producer.send(message)

        }

        // 批量发送数据到topic里面
        producer.send(messageBuffer:_*)

        // TODO :endTime
        val endTime =System.currentTimeMillis()

        println(s"-----Send Messages:${random},SpendTime:${endTime -startTime}-----")

        // 休息一下
        Thread.sleep(RandomUtils.getRandomNum(100) * 10)
      }

    } catch {
      case e: Exception => e.printStackTrace()
    } finally {
      // 关闭producer
      if (null != producer) producer.close()
    }


  }
}
