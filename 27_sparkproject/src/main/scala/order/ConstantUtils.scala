package order

// 存储一些常量数据
object ConstantUtils {
  /**
   * 定义kafka相关的集群配置信息
   */
  // kafka集群
  val METADATA_BROKER_LIST ="127.0.0.1:9092"
  // 发送数据的方式
  val PRODUCER_TYPE ="async"
  // 序列化类
  val SERIALIZER_CLASS ="kafka.serializer.StringEncoder"
  //Topic名称
  val ORDER_TOPIC = "test6667"
  //OFFSET
  val AUTO_OFFSET_RESET = "largest"

}
