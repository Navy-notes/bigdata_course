package order

import java.text.SimpleDateFormat
import java.util.Date

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import kafka.serializer.StringDecoder
import org.apache.spark.sql.SparkSession
import org.apache.spark.streaming.kafka.KafkaUtils
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.streaming.{Duration, Minutes, Seconds, StreamingContext}

// 从kafka topic里面获取数据,统计订单量和订单总值
object OrderTurnoverStreaming {
  // 检查点目录
  val CHECKPOINT_DIRECTORY = "27_sparkproject/src/main/spark_checkpoint/order2"
  // 采样间隔
  val BATCH_INTERVAL:Duration =Seconds(2)
  // 窗口长度
  val WINDOW_INTERVAL :Duration =BATCH_INTERVAL * 5
  // 滑动距离
  val SLIDER_INTERVAL :Duration =BATCH_INTERVAL * 3

  // 贷出函数
 def sparkOperation(args: Array[String])(operation:StreamingContext =>Unit)={

   def functionToCreateContext():StreamingContext ={
     // 创建配置对象
     val sparkConf = new SparkConf()
       .setAppName("OrderTurnoverStreaming")
       .setMaster("local[3]")

     //TODO 优化 设置spark从kafka topic 中每秒钟获取每个分区最多的数据条目数
     sparkConf.set("spark.streaming.kafka.maxRatePerPartition", "10000")
     //TODO 优化 设置序列化的方式
     sparkConf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
     sparkConf.registerKryoClasses(Array(classOf[Order]))

     // 创建sparkcontext上下文对象
     val sc = SparkContext.getOrCreate(sparkConf)

     // 设置日志级别
     sc.setLogLevel("WARN")

    // 创建ssc对象
     val ssc:StreamingContext = new StreamingContext(sc, BATCH_INTERVAL)
     // 调用用户函数,处理数据
     operation(ssc)

     // 设置检查点目录
     ssc.checkpoint(CHECKPOINT_DIRECTORY)

     // 确保我们交互式查询时候,数据不被删除
     ssc.remember(Minutes(1))

     ssc
   }

   var context:StreamingContext =null

    try {
      //Stop any Existing Context
      val stopActiveContext =true
      if(stopActiveContext){
        StreamingContext.getActive().foreach(_.stop(stopSparkContext = true))
      }

      //context = StreamingContext.getOrCreate(CHECKPOINT_DIRECTORY, functionToCreateContext)
       context = StreamingContext.getActiveOrCreate(CHECKPOINT_DIRECTORY, functionToCreateContext)

      // 设置日志级别
      context.sparkContext.setLogLevel("WARN")
      context.start()
      context.awaitTermination()
    } catch {
      case e:Exception =>e.printStackTrace()
    } finally {
      // 关闭资源
      context.stop(stopSparkContext = true,stopGracefully = true)
    }
  }



  def main(args: Array[String]): Unit = {
    sparkOperation(args)(processOrderData)
  }



  // 用户函数
  def processOrderData(ssc:StreamingContext):Unit={
    /**
     * 1.从kafkatopic里面获取数据 direct
     */
    // 设置kafka连接相关信息(第一个参数)
    val kafkaParams:Map[String,String] =Map(
      "metadata.broker.list" -> ConstantUtils.METADATA_BROKER_LIST,
      "auto.offset.reset"-> ConstantUtils.AUTO_OFFSET_RESET
    )

    // Topics(第二个参数)
    val topicsSet = ConstantUtils.ORDER_TOPIC.split(",").toSet

    // 采用direct方式从kafka读取数据
    val orderDStream = KafkaUtils.createDirectStream[String, String, StringDecoder, StringDecoder](ssc, kafkaParams, topicsSet)


    // 需求一
    /**
     * 2.处理订单数据 需求一:实时累加统计各省份营业额
     */

    val orderTurnoverDStream = orderDStream
      //a.解析json格式数据,数据转换成DStream[(key,Value)]
      .map(tuple => {
        //解析json格式数据为Order
        val order = ObjectMapperSingleton.getInstance().readValue(tuple._2, classOf[Order])
        (order.provinceId, order)
      })
      //b.使用updateStateBykey进行一个实时累加统计 ---各省份
      .updateStateByKey(
        //updateFunc
        (orders: Seq[Order], state: Option[Float]) => {
          //获取当前省份传递进来的订单营业额
          val currentOrdersPrice = orders.map(_.price).sum
          //获取当前省份以前的营业额
          val previousPrice = state.getOrElse(0.0F)
          //累加营业额并返回
          Some(currentOrdersPrice + previousPrice)
        }
      )

//    orderTurnoverDStream.print()

    /**
     * 3.将各省份实时统计的营业额进行输出
     */
    orderTurnoverDStream.foreachRDD(
      (rdd, time)=>{
        //格式化时间
        val batchInterval = new SimpleDateFormat("yyyy-MM--dd HH:mm:ss").format(new Date(time.milliseconds))
        println("--------------------------------------")
        println(s"batchInterval : ${batchInterval}")
        println("--------------------------------------")
        rdd.coalesce(1)
          .foreachPartition(iter =>{
            iter.foreach(println)
          })
      }
    )

    // 需求二
    /**
     * 统计每10s钟各个省份的订单量
     */
//    orderDStream
//    // 设置窗口大小,滑动距离
//      .window(WINDOW_INTERVAL, SLIDER_INTERVAL)
//      .foreachRDD(
//        (rdd, time)=>{
//          // 格式化时间
//          val sliderInterval = new SimpleDateFormat("yyyy-MM--dd HH:mm:ss").format(new Date(time.milliseconds))
//          println(s"-------sliderInterval :${sliderInterval}----------")
//          // 判断RDD是否有数据
//          if(!rdd.isEmpty()){
//            val orderRDD = rdd.map(tuple => {
//              // 解析json格式数据
//              ObjectMapperSingleton.getInstance().readValue(tuple._2, classOf[Order])
//            }
//            )
//            // 创建SparkSession对象
//            val spark = SparkSessionSingleton.getInstance(rdd.sparkContext.getConf)
//
//            // 转换成DataFrame ,导入隐式转换
//            import spark.implicits._
//            val orderDS = orderRDD.toDS()
//
//            // 使用DSL分析
//            val provinceCountDS = orderDS.groupBy("provinceId").count()
//
//            // 打印
//            provinceCountDS.show(15, truncate = false)
//
//          }
//
//        }
//      )


  }

}


// 创建ObjectMapper 单例对象
object ObjectMapperSingleton{
  @transient private var instance:ObjectMapper = _
  def getInstance():ObjectMapper={
    if (instance ==null){
       instance = new ObjectMapper()
       instance.registerModule(DefaultScalaModule)
    }
    instance
  }
}


// 创建SparkSession单例对象
object SparkSessionSingleton{
  @transient private var instance:SparkSession=_
  def getInstance(sparkConf:SparkConf):SparkSession={
    if (instance ==null){
       instance = SparkSession.builder().config(sparkConf).getOrCreate()
    }
    instance
  }
}