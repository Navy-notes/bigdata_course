package order

import scala.util.Random

// 随机数的获取
object RandomUtils {
def getRandomNum(bound:Int):Int={
  // 创建随机序列
  val random = new Random()
  // 生成随机数字
  random.nextInt(bound)
}
}
