package com.study.pojo;

public class WordAndOne {
    private String word;
    // 字段可以使用基本类型，这里用int只是测试，建议最好是都用实体类或封装类型
    private int count;

    // Flink认可的POJO类必须保留无参构造方法
    public WordAndOne() {
    }

    public WordAndOne(String word, int count) {
        this.word = word;
        this.count = count;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public int getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "WordAndOne{" +
                "word='" + word + '\'' +
                ", count=" + count +
                '}';
    }
}