package com.study.slot;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

/**
 * 这个任务里面应该有几个task？
 *   1       1        1        1
 * [socket  flatMap] keybased [keyby|sum  sink]
 *
 * 答案是：1个任务槽，两个task
 */
public class WordCount04 {
    public static void main(String[] args) throws Exception {

//        ParameterTool parameterTool = ParameterTool.fromArgs(args);
//        String hostname = parameterTool.get("hostname");
//        int port = parameterTool.getInt("port");

        StreamExecutionEnvironment env = StreamExecutionEnvironment.createLocalEnvironmentWithWebUI(new Configuration());

        //在集群里面运行的时候，默认的并行度就是1
        env.setParallelism(1);


        DataStreamSource<String>    data = env.socketTextStream("localhost", 1234);
        SingleOutputStreamOperator<MyWordCount> result =
                data.flatMap(new SplitWord())
                .keyBy("word")
                .sum("count");


        result.print();


        env.execute("word count");


    }






    public static class SplitWord implements FlatMapFunction<String,MyWordCount>{
        @Override
        public void flatMap(String line, Collector<MyWordCount> collector) throws Exception {
            String[] fields = line.split(",");
            for (String word : fields) {
                collector.collect(new MyWordCount(word, 1));
            }
        }
    }



    public static class MyWordCount{
        private String word;
        private int count;

        public MyWordCount(){

        }

        public MyWordCount(String word, int count) {
            this.word = word;
            this.count = count;
        }

        public String getWord() {
            return word;
        }

        public void setWord(String word) {
            this.word = word;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        @Override
        public String toString() {
            return "MyWordCount{" +
                    "word='" + word + '\'' +
                    ", count=" + count +
                    '}';
        }
    }

}
