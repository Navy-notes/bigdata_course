package com.study.slot;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer010;
import org.apache.flink.util.Collector;

import java.util.Properties;


/**
 * Task_1
 *  source 3
 *  flatMap 3
 *
 * Task_2
 *  keyby sum 3
 *  map 3
 *
 * Task_3
 *  sink 1
 *
 * subtask = 3 + 3 + 1 = 7
 *
 */
public class WordCount05 {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.createLocalEnvironmentWithWebUI(new Configuration());
        String topic="test6667";
        Properties consumerProperties = new Properties();
        consumerProperties.setProperty("bootstrap.servers","localhost:9092");
        consumerProperties.setProperty("group.id","wc5_consumer");


        FlinkKafkaConsumer010<String> myConsumer =
                new FlinkKafkaConsumer010<>(topic, new SimpleStringSchema(), consumerProperties);
       //task
        DataStreamSource<String> data = env.addSource(myConsumer).setParallelism(3);



        SingleOutputStreamOperator<Tuple2<String, Integer>> wordOneStream = data.flatMap(new FlatMapFunction<String, Tuple2<String, Integer>>() {
            @Override
            public void flatMap(String line,
                                Collector<Tuple2<String, Integer>> out) throws Exception {
                String[] fields = line.split(",");
                for (String word : fields) {
                    out.collect(Tuple2.of(word, 1));
                }
            }
        }).setParallelism(3);

        SingleOutputStreamOperator<Tuple2<String, Integer>> result = wordOneStream.keyBy(0).sum(1).setParallelism(3);

        result.map( tuple -> tuple.toString()).setParallelism(3)

                .print().setParallelism(1);

        env.execute("WordCount....");


    }
}
