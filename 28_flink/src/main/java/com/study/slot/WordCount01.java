package com.study.slot;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

/**
 * socket输入流
 * 28_flink/src/main/document/image/DAG图说明.png
 */
public class WordCount01 {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.createLocalEnvironmentWithWebUI(new Configuration());

        /**
         * 针对的是整个全局
         */
        env.setParallelism(2);

//        ParameterTool parameterTool = ParameterTool.fromArgs(args);
//        String hostname = parameterTool.get("hostname");
//        int port = parameterTool.getInt("port");

        DataStreamSource<String> dataStream = env.socketTextStream("localhost", 1234);
        dataStream.flatMap(new FlatMapFunction<String, Tuple2<String,Integer>>() {
            @Override
            public void flatMap(String line,
                                Collector<Tuple2<String, Integer>> out) throws Exception {
                String[] fields = line.split(",");
                for(String word:fields){
                    out.collect(Tuple2.of(word,1));
                }
            }
        }).keyBy(0)
                .sum(1)
                .print().setParallelism(1);

        env.execute("WordCount01");
    }
}
