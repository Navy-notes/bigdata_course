package com.study.slot;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer010;

import java.util.Properties;

/**
 * 从kafka中获取数据流，代码中不设置并行度，默认取local的最大cpu核心数作为系统级的并行度
 */
public class WordCount02 {
    public static void main(String[] args) throws Exception {
         //创建程序入口
        StreamExecutionEnvironment env = StreamExecutionEnvironment.createLocalEnvironmentWithWebUI(new Configuration());
        String topic="test6667";

        Properties consumerProperties = new Properties();
        consumerProperties.setProperty("bootstrap.servers","127.0.0.1:9092");
        consumerProperties.setProperty("group.id","wc02_consumer");



        FlinkKafkaConsumer010<String> myConsumer =
                new FlinkKafkaConsumer010<String>(topic, new SimpleStringSchema(), consumerProperties);

        //获取数据
        DataStreamSource<String> data = env.addSource(myConsumer).setParallelism(1);

        //数据的处理
        SingleOutputStreamOperator<Tuple2<String, Integer>> wordOneStream = data.shuffle()
                .flatMap((FlatMapFunction<String, Tuple2<String, Integer>>) (line, out) -> {
                    String[] fields = line.split(",");
                    for (String word : fields) {
                        out.collect(Tuple2.of(word, 1));
                    }
                })
                .returns(Types.TUPLE(TypeInformation.of(String.class),TypeInformation.of(Integer.class))).setParallelism(3);      // returns不属于一个独立的算子，会归附于它前一个使用了lambda表达式的算子

        SingleOutputStreamOperator<Tuple2<String, Integer>> result = wordOneStream
                .keyBy(0)
                .sum(1).setParallelism(2);

        result.map( tuple -> tuple.toString()).print().setParallelism(1);

        env.execute("WordCount02");

    }
}
