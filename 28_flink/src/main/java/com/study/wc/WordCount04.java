package com.study.wc;

import com.study.pojo.WordAndOne;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/10/21 19:14
 * 运行参数非硬编码（生产一般用这个版本）
 */
public class WordCount04 {
    public static void main(String[] args) throws Exception {
        // 创建流程序环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        ParameterTool argsParma = ParameterTool.fromArgs(args);
        String hostname = argsParma.get("hostname");
        int port = argsParma.getInt("port");

        // 数据流输入
        DataStreamSource<String> stream = env.socketTextStream(hostname, port);

        // 数据流处理
        SingleOutputStreamOperator<WordAndOne> result = stream
                .flatMap(new StringSplitTask())
                .keyBy("word")
                .sum("count");

        // 数据流输出
        result.print();

        // 运行流程序
        env.execute("wc_04");
    }

    static class StringSplitTask implements FlatMapFunction<String, WordAndOne> {
        @Override
        public void flatMap(String line, Collector<WordAndOne> out) throws Exception {
            String[] words = line.split(",");
            for (String word : words) {
                out.collect(new WordAndOne(word, 1));
            }
        }
    }
}
