package com.study.wc;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/10/21 19:14
 * 基础简单版
 */
public class WordCount01 {
    public static void main(String[] args) throws Exception {
        // 创建流程序环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        // 数据流输入
        DataStreamSource<String> stream = env.socketTextStream("127.0.0.1", 1234);

        // 数据流处理
        SingleOutputStreamOperator<Tuple2<String, Integer>> result = stream.flatMap(new FlatMapFunction<String, Tuple2<String, Integer>>() {
            @Override
            public void flatMap(String line, Collector<Tuple2<String, Integer>> out) throws Exception {
                String[] words = line.split(",");
                for (String word : words) {
                    out.collect(new Tuple2<>(word, 1));
//                    out.collect(Tuple2.of(word, 1));
                }
            }
        }).keyBy(0).sum(1);

        // 数据流输出
        result.print();

        // 运行流程序
        env.execute("wc_01");
    }
}
