package com.study.wc;

import com.study.pojo.WordAndOne;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/10/21 19:14
 * 设置全局并行度并配置WebUI
 */
public class WordCount05 {
    public static void main(String[] args) throws Exception {
        // 创建流程序环境（带WebUI）
        StreamExecutionEnvironment env = StreamExecutionEnvironment.createLocalEnvironmentWithWebUI(new Configuration());

        // 并行度不设置的话会根据cpu核数默认为4，当然设置为5也可以
        env.setParallelism(2);

        ParameterTool argsParma = ParameterTool.fromArgs(args);
        String hostname = argsParma.get("hostname");
        int port = argsParma.getInt("port");

        // 数据流输入，由于socket流的特殊性，并行度能且只能为1
        DataStreamSource<String> stream = env.socketTextStream(hostname, port);

        // 数据流处理
        SingleOutputStreamOperator<WordAndOne> result = stream
                .flatMap(new StringSplitTask())
                .keyBy("word")
                .sum("count");

        // 数据流输出
        result.print();

        // 运行流程序
        env.execute("wc_05");
    }

    static class StringSplitTask implements FlatMapFunction<String, WordAndOne> {
        @Override
        public void flatMap(String line, Collector<WordAndOne> out) throws Exception {
            String[] words = line.split(",");
            for (String word : words) {
                out.collect(new WordAndOne(word, 1));
            }
        }
    }
}
