package com.study.wc;

import com.study.pojo.WordAndOne;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/10/21 19:14
 * 算子函数抽离
 */
public class WordCount03 {
    public static void main(String[] args) throws Exception {
        // 创建流程序环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        // 数据流输入
        DataStreamSource<String> stream = env.socketTextStream("127.0.0.1", 1234);

        // 数据流处理
        SingleOutputStreamOperator<WordAndOne> result = stream
                .flatMap(new StringSplitTask())
                .keyBy("word")
                .sum("count");

        // 数据流输出
        result.print();

        // 运行流程序
        env.execute("wc_03");
    }

    static class StringSplitTask implements FlatMapFunction<String, WordAndOne> {
        @Override
        public void flatMap(String line, Collector<WordAndOne> out) throws Exception {
            String[] words = line.split(",");
            for (String word : words) {
                out.collect(new WordAndOne(word, 1));
            }
        }
    }
}
