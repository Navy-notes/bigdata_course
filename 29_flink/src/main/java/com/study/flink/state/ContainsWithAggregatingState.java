package com.study.flink.state;

import org.apache.flink.api.common.functions.AggregateFunction;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.common.state.AggregatingState;
import org.apache.flink.api.common.state.AggregatingStateDescriptor;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/11/1 21:28
 * (1,Contains:3 and 5 and 7)
   (2,Contains:4 and 2 and 5)
 */
public class ContainsWithAggregatingState extends RichMapFunction<Tuple2<Long, Long>, Tuple2<Long, String>> {

    // 辅助字段<IN, OUT>
    private AggregatingState<Long, String> totalStr;


    @Override
    public void open(Configuration parameters) throws Exception {
        AggregatingStateDescriptor<Long, String, String> descriptor =
                // 类似SparkSQL 自定义聚合函数
                new AggregatingStateDescriptor<>("totalStr", new AggregateFunction<Long, String, String>() {
                    // 初始化的操作，只运行一次哦
                    @Override
                    public String createAccumulator() {
                        return "Contains: ";
                    }

                    @Override
                    public String add(Long value, String accumulator) {
                        if ("Contains: ".equals(accumulator)) {
                            return accumulator + value;
                        }
                        return accumulator + " and " + value;
                    }

                    @Override
                    public String getResult(String accumulator) {
                        // contains:1
                        // contains: 1 and 3 and
                        return accumulator;
                    }

                    @Override
                    public String merge(String acc1, String acc2) {
                        return acc1 + " and " + acc2;       // 这里应该是合并两个分区内部各自处理后的结果
                    }
                }, String.class);
        totalStr = getRuntimeContext().getAggregatingState(descriptor);
    }


    @Override
    public Tuple2<Long, String> map(Tuple2<Long, Long> element) throws Exception {
        totalStr.add(element.f1);
        return Tuple2.of(element.f0, totalStr.get());
    }

}
