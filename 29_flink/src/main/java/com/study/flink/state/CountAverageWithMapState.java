package com.study.flink.state;

import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.common.state.MapState;
import org.apache.flink.api.common.state.MapStateDescriptor;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.shaded.guava18.com.google.common.collect.Lists;
import org.apache.flink.util.Collector;

import java.util.List;
import java.util.UUID;


/**
 *  MapState<K, V> ：这个状态为每一个 key 保存一个 Map 集合
 *      put() 将对应的 key 的键值对放到状态中
 *      values() 拿到 MapState 中所有的 value
 *      clear() 清除状态
 */
public class CountAverageWithMapState extends RichFlatMapFunction<Tuple2<Long, Long>, Tuple2<Long, Double>> {
    // managed keyed state
    //1. MapState ：key 是一个唯一的值，value 是接收到的相同的 key 对应的 value 的值

    // 我们开发过程当中声明的mapState其实我们可以理解为就是一个辅助的map。
    // Map的数据类型：key相同 数据就覆盖了，所以这里我们对mapState中的 key 另外的随机生成
    private MapState<String, Long> mapState;


    /**
     * 1，3
     * 1，5
     * 1，7
     *
     */
    @Override
    public void open(Configuration parameters) throws Exception {
        MapStateDescriptor<String, Long> mapStateDescriptor =
                new MapStateDescriptor<>("average", String.class, Long.class);
        mapState = getRuntimeContext().getMapState(mapStateDescriptor);
    }


    /**
     * 1,3
     * 1,5
     * 1,7
     *
     * dfsfsdafdsf，3
     * dfsfxxxfdsf,5
     * xxxx323123,7
     *
     * @param element
     * @param out
     * @throws Exception
     */
    @Override
    public void flatMap(Tuple2<Long, Long> element, Collector<Tuple2<Long, Double>> out) throws Exception {
        mapState.put(UUID.randomUUID().toString(), element.f1);

        List<Long> allValues = Lists.newArrayList(mapState.values());

        // 判断，如果当前的 key (即element.f0) 出现了 3 次，则需要计算平均值，并且输出
        if (allValues.size() == 3) {
            Long sum = 0L;
            for (Long value : allValues) {
                sum += value;
            }
            out.collect(new Tuple2<>(element.f0, sum /3.0));
            // 清除状态
            mapState.clear();
        }
    }
}
