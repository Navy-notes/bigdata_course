package com.study.flink.state;

import org.apache.flink.api.common.state.BroadcastState;
import org.apache.flink.api.common.state.MapStateDescriptor;
import org.apache.flink.api.common.state.ReadOnlyBroadcastState;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.streaming.api.datastream.BroadcastStream;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.co.BroadcastProcessFunction;
import org.apache.flink.util.Collector;

/**
 * 目的：
        为了实现高效的关联字典数据/维表（map side join）并且不需要通过时间窗口来聚合，能够动态维护Broadcast State里面的值。
        缺点是，要想准确关联上字典，那么数据流需要在广播流获取到其对应的字典数据并且广播到所有TaskManager本地之后，再输入数据才能关联上。
       （然而这其中的顺序是无法保证的，不管是字典数据获取的先后还是广播到各个TaskManager的网络传输时间差都无法保证。优化思路见代码注释。）
 * 使用方法：
        创建两个流，其中一个是可广播的流，另外一个是非广播的流
        可广播的流会通过网络广播到（同一Flink作业的）所有的TaskManager的内存中
        非广播的流要connect广播的流，然后就可以实现关联
        支持keyed state 和 nonkey state两种方式
 * 需要广播的字典数据：
       【type,id,activity】
        INSERT,1,新人礼包
        INSERT,2,女神节促销
        INSERT,3,周末活动
        UPDATE,3,周末抽奖
        DELETE,3,周末抽奖
 * 要关联字典的非广播数据：
       【uid,dt,id】
        uid01,2020-03-08 11:11:11,2
        uid01,2020-03-08 11:11:11,1
        uid01,2020-03-08 11:11:11,3
        uid01,2020-03-08 11:11:11,3
 * 参考博客：https://blog.csdn.net/u010271601/article/details/104809489
 */
public class TestBroadcastState {
    // 广播数据的状态描述器
    private static MapStateDescriptor<String, String> mapStateDescriptor = new MapStateDescriptor<>(
            "broadcast-State",
            String.class,
            String.class
    );

    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();


        // 获取数据流
        DataStream<Tuple3<String, String, String>> dataStream = env
                .socketTextStream("localhost", 1234)
                .map(line -> {
                    String[] fields = line.split(",");
                    return Tuple3.of(fields[0], fields[1], fields[2]);
                })
                .returns(Types.TUPLE(Types.STRING, Types.STRING, Types.STRING));
        // 构建广播流
        BroadcastStream<Tuple3<String, String, String>> broadcastStream = env
                .socketTextStream("localhost", 4567)
                .map(line -> {
                    String[] fields = line.split(",");
                    return Tuple3.of(fields[0], fields[1], fields[2]);
                })
                .returns(Types.TUPLE(Types.STRING, Types.STRING, Types.STRING))
                .broadcast(mapStateDescriptor);
        // 双流合并，数据流connect广播流并完成关联
        dataStream.connect(broadcastStream).process(new MyBroadcastProcessFunction()).print();


        env.execute("TestBroadcastState");
    }



    /**
     * IN1  数据流的类型
     * IN2  广播流的类型
     * OUT  合并关联后，输出数据的类型
     */
    static class MyBroadcastProcessFunction extends
            BroadcastProcessFunction<Tuple3<String, String, String>,
                    Tuple3<String, String, String>,
                    Tuple4<String, String, String, String>> {
        // 如果想要尽可能地保证数据流都能关联上字典，可以预设多一个ListState，存储那些关联字段为null的事件（关联字段为null的事件照常向下游数据）。
        // 每当有新事件来的时候则遍历这个ListState，把能够重新关联上的事件往下游输出并移出ListState，下游可以根据幂等写入更新结果，实现最终一致性。


        // 非广播流的数据处理逻辑
        @Override
        public void processElement(Tuple3<String, String, String> dataElement, ReadOnlyContext readOnlyCtx, Collector<Tuple4<String, String, String, String>> out) throws Exception {
            String uid = dataElement.f0;
            String time = dataElement.f1;
            String aid = dataElement.f2;

            // 通过上下文获取广播state（只读）
            ReadOnlyBroadcastState<String, String> broadcastState = readOnlyCtx.getBroadcastState(mapStateDescriptor);

            //通过aid关联broadcastState活动名称
            String name = broadcastState.get(aid);
            out.collect(Tuple4.of(uid, time, aid, name));
        }

        // 广播流的数据处理逻辑
        @Override
        public void processBroadcastElement(Tuple3<String, String, String> broadcastElement, Context ctx, Collector<Tuple4<String, String, String, String>> out) throws Exception {
            String type = broadcastElement.f0;
            String aid = broadcastElement.f1;
            String aname = broadcastElement.f2;

            // broadcastState存储在每个TaskManager的内存中
            BroadcastState<String, String> broadcastState = ctx.getBroadcastState(mapStateDescriptor);

            // 将字典表的数据存储到broadcastState中，并且可以动态维护
            if (type.equals("DELETE")) {
                broadcastState.remove(aid);
            } else {
                broadcastState.put(aid, aname);
            }
        }
    }

}
