package com.study.flink.state;

import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.common.state.ListState;
import org.apache.flink.api.common.state.ListStateDescriptor;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.shaded.guava18.com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Collections;

/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/10/31 21:49
 */
public class CountAverageWithListState extends RichMapFunction<Tuple2<Long, Long>, Tuple2<Long, Double>> {

    /**
     * 1,3
     * 1,7
     * 1,5
     */
    private ListState<Tuple2<Long, Long>> elementsByKey;


    @Override
    public void open(Configuration parameters) throws Exception {
        ListStateDescriptor<Tuple2<Long, Long>> listStateDescriptor =
                new ListStateDescriptor<>("average", Types.TUPLE(Types.LONG, Types.LONG));

        elementsByKey = getRuntimeContext().getListState(listStateDescriptor);
    }


    @Override
    public Tuple2<Long, Double> map(Tuple2<Long, Long> element) throws Exception {
        if (elementsByKey.get() == null) {
            elementsByKey.addAll(Collections.emptyList());
        }

        elementsByKey.add(element);

        ArrayList<Tuple2<Long, Long>> allElements = Lists.newArrayList(elementsByKey.get());

        if (allElements.size() == 3) {
            Long sum = 0L;
            for (Tuple2<Long, Long> e : allElements) {
                sum += e.f1;
            }
            elementsByKey.clear();
            return Tuple2.of(element.f0, sum / 3.0);
        }

        return Tuple2.of(null, null);       // 直接返回null会报错
    }
}
