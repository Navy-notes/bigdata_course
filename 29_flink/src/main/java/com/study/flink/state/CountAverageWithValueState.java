package com.study.flink.state;

import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.util.Collector;



/**
 * @author Yuliang.Lee
 * @version 1.0
 * @date 2021/10/23 20:28
 */
public class CountAverageWithValueState extends RichFlatMapFunction<Tuple2<Long, Long>, Tuple2<Long, Double>> {

    /**
     * 1.valueState属于keyed state
     * 2.valueState里面只能存储一条数据
     * <p>
     * 思路:
     *  long1: 当前key出现的次数
     *  long2: 累加的value值
     *  if(long1=3){
     *      long2/long1 =avg
     *  }
     */
    private ValueState<Tuple2<Long, Long>> countAndSum;

    @Override
    public void open(Configuration parameters) throws Exception {
        ValueStateDescriptor<Tuple2<Long, Long>> valueStateDescriptor =
                new ValueStateDescriptor<>("average", Types.TUPLE(Types.LONG, Types.LONG));

        countAndSum = getRuntimeContext().getState(valueStateDescriptor);
    }

    @Override
    public void flatMap(Tuple2<Long, Long> element, Collector<Tuple2<Long, Double>> out) throws Exception {
        Tuple2<Long, Long> currentState = countAndSum.value();

        if (currentState == null) {
            currentState = Tuple2.of(0L, 0L);
        }

        // 统计key出现的次数
        currentState.f0 += 1;
        // 统计value总值
        currentState.f1 += element.f1;

        // 更新state
        countAndSum.update(currentState);

        if (currentState.f0 == 3) {
            out.collect(Tuple2.of(element.f0, currentState.f1 / 3.0));
            // 清空里面的数据
            countAndSum.clear();
        }

    }


}
