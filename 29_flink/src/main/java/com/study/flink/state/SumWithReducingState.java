package com.study.flink.state;

import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.common.state.ReducingState;
import org.apache.flink.api.common.state.ReducingStateDescriptor;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;

/**
 *  ReducingState<T> ：这个状态为每一个 key 保存一个聚合之后的值
 *      get() 获取状态值
 *      add()  更新状态值，将数据放到状态中
 *      clear() 清除状态
 */
public class SumWithReducingState extends RichMapFunction<Tuple2<Long, Long>, Tuple2<Long,Long>>{

    // sum = 最终累加的结果的数据类型
    private ReducingState<Long> sumState;

    @Override
    public void open(Configuration parameters) throws Exception {
        // 注册状态
        ReducingStateDescriptor<Long> reducingStateDescriptor =
                new ReducingStateDescriptor<>("sum", // 状态的名字
                        new ReduceFunction<Long>() {        // 聚合函数
                    @Override
                    public Long reduce(Long value1, Long value2) throws Exception {
                        return value1 + value2;
                    }
                }, Long.class); // 状态存储的数据类型
        sumState = getRuntimeContext().getReducingState(reducingStateDescriptor);
    }


    /**
     *
     * 3
     * 5
     * 7
     *
     * @param element
     * @throws Exception
     */
    @Override
    public Tuple2<Long, Long> map(Tuple2<Long, Long> element) throws Exception {
        // 将数据放到状态中
        sumState.add(element.f1);
        return Tuple2.of(element.f0, sumState.get());
    }
}
