package com.study.flink.state;



import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 *  需求：当接收到的相同 key 的元素个数等于 3 个
 *  就计算这些元素的 value 的平均值。
 *  计算 keyed stream 中每 3 个元素的 value 的平均值
 *
 *  1，3
 *  1，7
 *
 *  1，5
 *
 *  1，5.0
 *
 *  2，4
 *
 *  2，2
 *  2，5
 *
 *  2，3.666
 *
 *  key,value
 *  1 long,5 doulbe
 *
 */
public class TestKeyedStateMain {
    public static void main(String[] args) throws  Exception{
        //程序入口
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        //数据源
        DataStreamSource<Tuple2<Long, Long>> dataStreamSource =
                env.fromElements(
                        Tuple2.of(1L, 3L),
                        Tuple2.of(1L, 7L),
                        Tuple2.of(2L, 4L),
                        Tuple2.of(1L, 5L),
                        Tuple2.of(2L, 2L),
                        Tuple2.of(2L, 5L),
                        Tuple2.of(1L, 1L)
                );

        // 输出：
        //(1,5.0)
        //(2,3.6666666666666665)
        dataStreamSource
                .keyBy(0)
//                .flatMap(new CountAverageWithValueState())    //flatMap,map + state = 自定义函数的感觉
//                .map(new CountAverageWithListState())
                .flatMap(new CountAverageWithMapState())
//                .map(new SumWithReducingState())                //实现的效果相当于keyBy + sum
//                .map(new ContainsWithAggregatingState())
                .print();

        env.execute("TestStatefulApi");
    }
}
