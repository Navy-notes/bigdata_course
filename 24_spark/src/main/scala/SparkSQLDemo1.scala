import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.types.{IntegerType, StringType, StructField}
import org.apache.spark.sql.{Row, SparkSession, types}


//创建DataFrame时候,使用StructType作为表结构
object SparkSQLDemo1 {
  def main(args: Array[String]): Unit = {
    // 日志设置:
    Logger.getLogger("org").setLevel(Level.OFF)
    System.setProperty("spark.ui.showConsoleProgress","false")

    //创建SparkSession对象
    val spark = SparkSession.builder().master("local").appName("SparkSQLDemo1").getOrCreate()

    //创建schema,通过StructType
    val schema =types.StructType(
      List(
        StructField("id", IntegerType, true), // true表示该字段可以为空
        StructField("name", StringType, true),
        StructField("age", IntegerType, true)
      )
    )

    //从指定文件中读取数据,生成对应的RDD
    val studentRDD = spark.sparkContext.textFile("24_spark/src/main/document/student.txt").map(_.split(" "))

    //将RDD映射到RowRDD行的数据上
    val rowRDD = studentRDD.map(student => Row(student(0).toInt, student(1), student(2).toInt))

    //生成DataFrame
    val studentDF = spark.createDataFrame(rowRDD, schema)

    //将DF注册成表/视图到当前会话
    studentDF.createOrReplaceTempView("student")

    //执行一个sql
    spark.sql("select * from student").show()

    //释放资源
    spark.stop()


  }
}
