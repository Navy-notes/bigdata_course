import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession

/**
  * @author Yuliang.Lee
  * @date 2021/10/15 21:37
  * @version 1.0
  */
object SparkSQLTopN {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.OFF)
    System.setProperty("spark.ui.showConsoleProgress","false")

    val spark = SparkSession.builder().master("local").appName("sparksql_top_n").getOrCreate()
    val scoreDF = spark.read.json("24_spark/src/main/document/score.json")

    scoreDF.createTempView("score")

    // 查询每门课程中成绩最好的前三名
    val query = "select course, name, score " +
                "from (select *, dense_rank() over (partition by course order by score desc) as rn from score) tmp " +
                "where rn <= 3 "
//                "order by course, rn"

    spark.sql(query).show()

    spark.stop()

  }

}
