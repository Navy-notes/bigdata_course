import org.apache.spark.sql.{Row, SparkSession}
import org.apache.spark.sql.expressions.{MutableAggregationBuffer, UserDefinedAggregateFunction}
import org.apache.spark.sql.types.{DataType, DoubleType, IntegerType, StructField, StructType}

object SparkSQLDemo4 extends UserDefinedAggregateFunction {
  /**
   * 定义输入的数据类型
   *
   * @return
   */
  override def inputSchema: StructType = StructType(
    StructField("salary", DoubleType, true) :: Nil
  )

  /**
   * 定义数据输出类型
   *
   * @return
   */
  override def dataType: DataType = DoubleType

  /**
    * 输入类型和输出类型是否一致
    * @return
    */
  override def deterministic: Boolean = true

  /**
   * 最后计算的目标函数
   *
   * @param buffer
   * @return
   */
  override def evaluate(buffer: Row): Any = {
    val total = buffer.getDouble(0)
    val count = buffer.getInt(1)
    //平均工资
    total / count
  }

  /**
    * 定义辅助字段
    * 辅助字段1:total 用来记录总工资
    * 辅助字段2:count 用来记录总人数
    *
    * @return
    */
  override def bufferSchema: StructType = StructType(
    StructField("total", DoubleType, true) ::
      StructField("count", IntegerType, true) :: Nil
  )

  /**
   * 初始化辅助字段
   *
   * @param buffer
   */
  override def initialize(buffer: MutableAggregationBuffer): Unit = {
    buffer.update(0, 0.0)
    buffer.update(1, 0)
  }

  /**
   * 更新辅助字段的值   局部操作   更新自己当前分区的
   *
   * @param buffer
   * @param input
   */
  override def update(buffer: MutableAggregationBuffer, input: Row): Unit = {
    val lastTotal = buffer.getDouble(0)
    val lastCount = buffer.getInt(1)
    val currentSalary = input.getDouble(0)
    buffer.update(0, lastTotal + currentSalary)
    buffer.update(1, lastCount + 1)
  }

  /**
   * 全局操作   合并分区之间的数据
   * @param buffer1
   * @param buffer2
   */
  override def merge(buffer1: MutableAggregationBuffer, buffer2: Row): Unit = {
    val total1 = buffer1.getDouble(0)
    val count1 = buffer1.getInt(1)
    val total2 = buffer2.getDouble(0)
    val count2 = buffer2.getInt(1)
    buffer1.update(0, total1 + total2)
    buffer1.update(1, count1 + count2)
  }

}

object  SalaryAvgtest{
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().master("local").appName("SalaryAvgtest").getOrCreate()

    // 注册UDAF函数
    spark.udf.register("avg_salary", SparkSQLDemo4)

    val df = spark.read.format("json").load("24_spark/src/main/document/emp.json")

    df.createOrReplaceTempView("emp")

    // 查询平均工资
    spark.sql("select avg_salary(sal) from emp").show()

    spark.stop()

  }
}