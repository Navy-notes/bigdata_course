import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession

//创建DataFrame时候,使用case class作为表结构
object SparkSQLDemo2 {
  def main(args: Array[String]): Unit = {
    // 日志设置:
    Logger.getLogger("org").setLevel(Level.OFF)
    System.setProperty("spark.ui.showConsoleProgress","false")

    //创建SparkSession对象
    val spark = SparkSession.builder().master("local").appName("SparkSQLDemo2").getOrCreate()

    //从指定文件中读取数据,生成对应的RDD
    val studentRDD = spark.sparkContext.textFile("24_spark/src/main/document/student.txt").map(_.split(" "))

    //将数据的RDD和caseclass关联起来
    val dataRDD = studentRDD.map(x =>Student(x(0).toInt,x(1),x(2).toInt))

    //生成DataFrame,通过RDD生成DataFrame,导入隐式转换
    import spark.sqlContext.implicits._

    val studentDF = dataRDD.toDF()

    //注册表/视图
    studentDF.createOrReplaceTempView("student")

    //执行sql
    spark.sql("select * from student").show()

    //释放资源
    spark.stop()
  }

}
//定义一个case class 代表表结构
case class Student(stuID:Int, stuName:String, stuAge:Int)