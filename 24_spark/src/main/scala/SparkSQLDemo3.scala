import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.{DataFrame, SparkSession}

//UDF
object SparkSQLDemo3 {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.OFF)
    System.setProperty("spark.ui.showConsoleProgress","false")

    val spark = SparkSession.builder().master("local").appName("Spark SQL basic example").getOrCreate()

    //自定义函数 获取当前字段的长度
    spark.udf.register("strLen", (str: String) => {
      if(str != null) {
        str.length
      } else{
        0
      }
    })


    val df: DataFrame = spark.read.load("24_spark/src/main/document/users.parquet")

    df.createOrReplaceTempView("users")

    spark.sql("select strLen(name) from users").show()

    spark.stop()
  }

}
