package com.nx.hbase;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 1、HBaseConfiguration：封装了hbase集群的配置信息（代码运行所需要的环境）
 *
 * 2、HBaseAdmin： HBase系统管理员的角色，对数据表进行操作或者管理
 *
 * 3、HTable：封装了整个表的信息（表名、列簇的信息），提供了操作该表数据的所有的业务方法
 *
 * 4、HTableDescriptor ： 所有的列簇的信息（一到多个HColumnDescriptor）
 *
 * 5、HColumnDescriptor： 一个列簇的信息
 *
 * 6、Cell：封装了一个column的信息：行键、列簇、列、值、时间戳
 *
 * 7、Put： 插入操作所需要的所有的相关信息
 *
 * 8、Delete： 删除操作所需要的所有的相关信息
 *
 * 9、Get： 封装查询条件
 *
 * 10、Scan： 封装所有的查询信息
 *
 * 11、Result： 封装了一个rowkey所对应的所有的数据信息
 *
 * 12、ResultScanner： 封装了多个Result的结果集
 *
 * @author LIAO
 */
public class HBaseOperate {

    public static void main(String[] args) throws Exception {
        //System.out.println(isTableExist("student"));
        //createTable("student2","info");
        //createTable("student3","base_info","extra_info");
        //dropTable("student3");
        //addData("student2", "001", "info", "name", "zhangsan");//注意增加数据，存在就是修改，不存在就是增加
        //addData("student2", "002", "info", "age", "30");
        //addData("student2", "002", "info", "school", "BUPT");
        //getAllData("student2");
        //getRowData("student2","001");
        //getRowQualifierData("student2", "001", "info", "school");
        deleteRowsData("student2", "002");
        //getAllData("student7");
    }

    //获取Configuration对象
    public static Configuration conf;
    static{
        //使用HBaseConfiguration的单例方法实例化
        //HBaseConfiguration：封装了hbase集群所有的配置信息（最终代码运行所需要的各种环境）
        conf = HBaseConfiguration.create();
        conf.set("hbase.zookeeper.quorum", "hadoop0:2181,hadoop1:2181,hadoop2:2181");
    }

    //1、判断表是否存在
    public static boolean isTableExist(String tableName) throws Exception{
        //在HBase中管理、访问表需要先创建HBaseAdmin对象
        //新方法
        Connection connection = ConnectionFactory.createConnection(conf);
        HBaseAdmin admin = (HBaseAdmin) connection.getAdmin();
        //老方法简单
        //HBaseAdmin：HBase系统管理员的角色，对数据表进行操作或者管理的一些方法。
        //HBaseAdmin admin = new HBaseAdmin(conf);
        return admin.tableExists(tableName);
    }

    /**
     createTable(String tableName, String f1)
     createTable(String tableName, String f1,String f2)
     createTable(String tableName, String f1,String f2,String f3)
     */
    //2、创建表  ，String... columnFamily 是不定长参数，注意理解
    public static void createTable(String tableName, String... columnFamily) throws Exception{
        //HBaseAdmin：HBase系统管理员的角色，对数据表进行操作或者管理的一些方法。
        HBaseAdmin admin = new HBaseAdmin(conf);
        //判断表是否存在
        if(isTableExist(tableName)){
            System.out.println("table： " + tableName + "已存在");
        }else{
            //创建表属性对象,表名需要转字节类型
            //HTableDescriptor：所有列簇的信息（一到多个HColumnDescriptor）
            HTableDescriptor descriptor = new HTableDescriptor(TableName.valueOf(tableName));
            //创建多个列簇
            for(String cf : columnFamily){
                descriptor.addFamily(new HColumnDescriptor(cf));
            }
            //根据对表的配置，创建表
            admin.createTable(descriptor);
            System.out.println("table： " + tableName + "创建成功！");
        }
    }

    //3、删除表 先disable 再drop（delete）
    public static void dropTable(String tableName) throws Exception{
        HBaseAdmin admin = new HBaseAdmin(conf);
        if(isTableExist(tableName)){
            admin.disableTable(tableName);
            admin.deleteTable(tableName);
            System.out.println("table: " + tableName + "删除成功");
        }else{
            System.out.println("table: " + tableName + "不存在");
        }
    }

    //4、向表中插入数据
    public static void addData(String tableName, String rowKey, String columnFamily, String column, String value) throws IOException {
        //创建HTable对象
        HTable hTable = new HTable(conf, tableName);
        //向表中插入数据
        Put put = new Put(Bytes.toBytes(rowKey));
        //向Put对象中组装数据
        put.add(Bytes.toBytes(columnFamily), Bytes.toBytes(column), Bytes.toBytes(value));
        hTable.put(put);
        hTable.close();
        System.out.println("恭喜呀，插入数据成功啦");
    }

    //5、获取所有数据，也就是获取所有行
    public static void getAllData(String tableName) throws IOException{
        //HTable：封装了整个表的所有的信息（表名，列簇的信息），提供了操作该表数据所有的业务方法。
        HTable hTable = new HTable(conf, tableName);
        //得到用于扫描region的对象scan
        //Scan： 封装查询信息，很get有一点不同，Scan可以设置Filter
        Scan scan = new Scan();
        //使用HTable得到resultcanner实现类的对象
        ResultScanner resultScanner = hTable.getScanner(scan);
        for(Result result : resultScanner){
            //Cell：封装了Column的所有的信息：Rowkey、column qualifier、value、时间戳
            Cell[] cells = result.rawCells();
            for(Cell cell : cells){
                System.out.println("行键: " + Bytes.toString(CellUtil.cloneRow(cell)));
                System.out.println("列簇: " + Bytes.toString(CellUtil.cloneFamily(cell)));
                System.out.println("列: " + Bytes.toString(CellUtil.cloneQualifier(cell)));
                System.out.println("值: " + Bytes.toString(CellUtil.cloneValue(cell)));
                System.out.println();
            }
        }
    }

    //6、获取某行数据
    public static void getRowData(String tableName, String rowKey) throws IOException{
        HTable table = new HTable(conf, tableName);
        Get get = new Get(Bytes.toBytes(rowKey));
        Result result = table.get(get);
        //循环获取所有信息
        for(Cell cell : result.rawCells()){
            System.out.println("行键: " + Bytes.toString(result.getRow()));
            System.out.println("列簇: " + Bytes.toString(CellUtil.cloneFamily(cell)));
            System.out.println("列: " + Bytes.toString(CellUtil.cloneQualifier(cell)));
            System.out.println("值: " + Bytes.toString(CellUtil.cloneValue(cell)));
            System.out.println("时间戳: " + cell.getTimestamp());
        }
    }

    //7、获取某行指定的数据，比如指定某个列簇的某个列限定符
    public static void getRowQualifierData(String tableName, String rowKey, String family, String qualifier) throws IOException{
        HTable table = new HTable(conf, tableName);
        Get get = new Get(Bytes.toBytes(rowKey));
        get.addColumn(Bytes.toBytes(family), Bytes.toBytes(qualifier));
        Result result = table.get(get);
        //循环获取所有信息,也可以单独打印自己需要的字段即可，这个一般根据业务需求修改。
        for(Cell cell : result.rawCells()){
            System.out.println("行键:" + Bytes.toString(result.getRow()));
            System.out.println("列族" + Bytes.toString(CellUtil.cloneFamily(cell)));
            System.out.println("列:" + Bytes.toString(CellUtil.cloneQualifier(cell)));
            System.out.println("值:" + Bytes.toString(CellUtil.cloneValue(cell)));
        }
    }

    //8、删除单行或多行数据
    public static void deleteRowsData(String tableName, String... rows) throws IOException{
        HTable hTable = new HTable(conf, tableName);
        List<Delete> deleteList = new ArrayList<Delete>();
        //循环
        for(String row : rows){
            Delete delete = new Delete(Bytes.toBytes(row));
//            delete.addColumn()
            deleteList.add(delete);
        }
        hTable.delete(deleteList);
        hTable.close();
    }

}

