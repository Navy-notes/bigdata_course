课程主题：深入浅出Kafka第三天

讲师：王端阳

内容大纲：
（1）掌握Controller对集群的管理方式

（2）Kafka在Zookeeper上的目录分析

（3）Kafka服务端的两个重要概念LEO和HW

LEO  log end offset 分区里面最新的数据的偏移量+1 
跟offfset偏移量有关系


LEO作用?

更新HW

HW 高水位 

如果follower和leader 的LEO同步了,此时HW就可以更新

HW之前（不包括自己，比如HW=4，则0,1,2,3可见）的数据对消费者可见,消息才属于commit状态
leader partition会维护从副本的LEO，然后主分区的HW取所有副本LEO的最小值，follower partition再把主副本的HW同步过来作为自己的HW


（4）如何保证Kafka消息不重复消费

（
  讲义上的三种方案：
    设置key保存并查询、
    利用数据库主键约束实现幂等、
    加前置条件比如给数据增加一个版本号属性
 ）

重试机制  网络 消息重复  消息乱序  


重复的消息已经发送了,怎么办

在不考虑并发的情况下,执行一个操作:

将李老师的账户余额设置为100万元  ---幂等

将李老师的余额加100万元   ---不是幂等


可以限定,对于每个转账单,每个账户只可以执行一次变更操作.

先在转账流水表中增加一条转账记录,然后再根据表中的转账记录,异步操作更新用户余额即可


加前置条件:
如果李老师的账户当前余额为500万,余额加100万元.

更加通用方法:给数据增加一个版本号属性 



（5）关于Kafka的其他扩展知识介绍


a.
kafka消费者的消费顺序如何保证?

消息的顺序需要保证吗?

发邮件  顺序不重要   

对数据库mysql 的binlog日志分析    两条日志 对同一数据的 update操作 

1.consumer是单线程处理
一个topic多个分区


2.分区和offset

一条消息怎么投递到一个分区里面的?

key   hash
没有设置key  轮询
自定义分区

offset  消息的索引   消息的存储顺序  

维护offset

offset  5    01234    678910

注意:在一个partition分区内部是有序的,partition之间的消息无序的 

如果保证所有数据有序, kafka里面只能有一个分区,一个单线程消费它

保证数据有序的方案：
    首先明白数据乱序是什么原因
    一是因为生产消息多个分区轮询会导致消息错散，这种可以通过只设置一个partition或者将需要保证有序的消息设置相同的key
    二是由于重试机制可能导致的乱序，这个时候需要设置异步发送的参数max.in.flight.requests.per.connection为1


b.数据库的衍生系统

数据仓库 实时数仓   

数据仓库自己不生产不消费数据 只是数据的中转层  他自己也有一定的分层

ods:临时存储层
dwd:核心存储层
dws:轻度汇总层
mid:数据集市层
app:数据应用层

hive  kafka 实时数仓

hdfs/hive.hbase/es/clickhouse/redis/kafka/MQ/kudo 


数仓开发人员做什么?

1.外部摄入数据:flume,爬虫,业务数据导入....
2.层与层之间的流转.ETL(清洗,过滤,转换,转义,统计,粒度转换)
3.开发对外服务接口.

实时采集,全量采集,增量采集

kafka es  

删除,剪切 


数据库的状态与事件

1.数据库只会展现当前状态的数据,不会展现之前数据,即截止到当前这个时刻的他的状态.
2.事件指的就是日志,binlog ,update事件记录下来

事件可以是不变的,由一些事件的发生会导致状态的变更.


可以先得到一个对数据库的操作流,再复现这个操作流程,这样衍生系统就会得到一个和数据源一样的数据.

kafka  先记录不变的事件,通过不变的事件让数据产生状态的变化.

数据库  update/insert 等  

删除  

日志压缩/事件的压缩

key   


数据库的衍生系统想要拿到数据库数据,肯定需要拿到一个全量数据的快照,之后再叠加增量,最终就当做跟源数据一样.

ES--全文搜索
hdfs 数据仓库 大数据批处理


比如: 

MySql -->binlog  变更捕获 <---kafka伪装成mysql从库  -->解析binlog日志-->数据仓库 warehouse
Oracle
再比如:
全文搜索.
add/create index

ES 



数据源实时同步到ES
如何保证同步?
a.实时捕获变化
b.进行多写  全文索引  既要写数据库,又写全文索引
问题:
多写情景下，会导致索引记录顺序跟数据库数据顺序不一样.
比如有两个变化 ：
    A变化请求先到达数据库,在到达索引
    B变化请求先到达索引,在到达数据库
这个时候我觉得，缓存层加入kafka可以把原本是多写变成串联写




mysql oracle --->ES,warehouse
    不用kafka的话，数据链路是m*n
    用了kafka作为消息队列，数据链路是m+n




实时数据仓库关键点:

a.对于同一个组件的数据操作日志记录,要保证他的顺序,如果不按照顺序消费就会产生错误.
b.对于同一个数据表或者数据库的schema的变化日志,要保证全局有序,比如:alter table ,增加1列,减少1列.
c.快. 可以把不同的表放进不同的分区,并发.而且把相同的表的数据放到同一个分区里面,也保证了同一个key的数据,绝对也是有序的.
d.新加一个系统   kafka支持持久化,能够支持消费历史数据




kafka代码背后执行逻辑  0.8版本的源码：
    生产者代码流程
    服务端broker启动流程   kafka-server-start.sh ./conf




spark     scala语言



